<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    // App/Participant
    'LiqPayDescription' => 'For the participation in the conference',

    // index.blade.php
    'Login' => 'Login',
    'Logout' => 'Logout',
    'Register' => 'Register',
    'ResetPas' => 'Password recovery',
    'ActiveEvents' => 'Active events',
    'edit' => 'Edit profile',
    'ref' => 'My referrals',
    'start' => 'Start with Enter or Registration',
    'infoUser' => 'If necessary fill in the personal data',
    'selEvent' => 'Choose the desired event',
    'and' => 'and',
    'actionEvent' => 'Apply for the participation',
    'addWork' => 'Attach the work and make the payment',
    'waitEmail' => 'Expect the news on your email',


    // info
    'info' => 'F.A.Q.',


    // event.blade.php
    'participate' => 'participate',
    'application' => 'Application for participation',
    'select_section' => 'Section selection',
    // 'Выбор' => 'Зробіть вибір' из user/info/create.blade
    'name_work' => 'Title of the work',
    'soavtor' => 'Authors',
    'example' => 'For example: I. Ivanov, P. Petrov etc.',
    'form_part' => 'Participation form',
    'additional_services' => 'Additional services',
    'translation' => 'Translation and publication of the work in the additional collection (+5 €)',
    'NBook' => 'Number of additional(!) collections (+2 €/piece)',
    'send' => 'Apply',
    'generalInfo' => 'General information',
    'description' => 'Description',
    'target' => 'Target',
    'lang' => 'Languages',
    'other' => 'Other',
    'sections' => 'List of sections',
    'designRules' => 'Rules for abstracts',
    'payment' => 'Cost and Payment',
    'orgcom' => 'Organizing Committee',
    'docs' => 'Documents',


    // _sidebar.blade.php
    'AllEvents' => 'ALL EVENTS',
    'MyEvents' => 'MY EVENTS',
    'user' => 'user',
    'admin' => 'admin',
    'general' => 'general',


    // _modal.blade
    'E-Mail Address' => 'E-Mail Address',
    'Password' => 'Password',
    'Remember Me' => 'Remember Me',
    'Confirm Password' => 'Confirm Password',


    // auth/verify.blade
    'Verify Your Email Address' => 'Verify Your Email Address',
    'A fresh verification link has been sent to your email address.' => 'A fresh verification link has been sent to your email address.',
    'Before proceeding, please check your email for a verification link.' => 'Before proceeding, please check your email for a verification link.',
    'If you did not receive the email' => 'If you did not receive the email',
    'click here to request another' => 'click here to request another',

    // vendor\laravel\framework\src\Illuminate\Auth\Notifications\VerifyEmail.php письмо верификации
    'Verify Email Address' => 'Verify Email Address',
    'Please click the button below to verify your email address.' => 'Please click the button below to verify your email address.',
    'If you did not create an account, no further action is required.' => 'If you did not create an account, no further action is required.',

    // vendor\laravel\framework\src\Illuminate\Notifications\resources\views\email.blade.php
    'If you’re having trouble clicking the button, copy and paste the URL below into your web browser:' => 'If you’re having trouble clicking the button, copy and paste the URL below into your web browser: ',

    // vendor\laravel\framework\src\Illuminate\Mail\resources\views\html\header.blade.php
    'Calendar of scientific events' => 'Calendar of scientific events',


    // user/myref
    'refs' => 'Мої реферали',

    // user/info/create.blade.php
    'titleUserInfoCreate' => 'Personal information',
    'titleUserInfoEdit' => 'Editing of personal data',
    'Формат' => 'images in png, jpg or jpeg',
    'Дата рождения' => 'Date of birth',
    'phone' => 'Phone number',
    'country' => 'Country',
    'questionTitle' => 'If not found?',
    'questionBody' => 'Choose ',
    'NoCountry' => '"Country is not found"',
    'NoCity' => '"City is not found"',
    'NoUniversity' => '"University is not found"',
    'site' => 'Website address',
    'Выбор' => 'Make your choice',
    'city' => 'City',
    'university' => 'Educational institution',
    'surname' => 'Surname',
    'name' => 'Name',
    'middle_name' => 'Middle name',
    'degree' => 'degree',
    'position' => 'position',
    'save' => 'save',

    // user/info/create.blade
    'bodyUserInfoEdit' => 'Enter the following data in manual mode',


    // user/events.blade.php
    'Thank' => 'Thank you!',
    'loadFile' => 'Attach your file in .doc or .docx format',
    'datasave' => 'Your profile changes have been saved successfully',
    'titleUserEvents' => 'Listing of scientific events',
    'activeEvents' => 'Active events',
    'uchast' => 'Participation in the conference',
    'dodBook' => 'additional collections', //
    'engBook' => 'collection in English', //
    'all' => 'Total',
    'section' => 'Section',
    // 'name_work' => 'Назва роботи',
    'avtors' => 'Authors',
    // 'form_part' => 'Форма участия' из event.blade.php
    'services' => 'Services',
    'osnBook' => 'obligatory collection',
    'dopBook' => 'additional collection',
    'dopBooks' => 'additional collections',
    'enBook' => 'collection in English',
    'sertificate' => 'certificate',
    'activeEventsError1' => 'You are not currently participating in any events.',
    'activeEventsError2' => 'Select the appropriate event on the page',
    'actualeEvents' => 'Current events',
    'archiveEvents' => 'Archive of Events',
    'archiveEventsError1' => 'There are no completed events with your participation yet',

    // _modal_pay
    'payOk' => 'Paid',
];
