<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    // App/Participant
    'LiqPayDescription' => 'За участие в конференции',


    // index.blade.php
    'Login' => 'Вход',
    'Logout' => 'Выход',
    'Register' => 'Регистрация',
    'ResetPas' => 'Восстановление пароля',
    'ActiveEvents' => 'Активные события',
    'edit' => 'Редактировать профиль',
    'ref' => 'Мои рефералы',
    'start' => 'Начните работу с входа или регистрации',
    'infoUser' => 'При необходимости заполните анкету',
    'selEvent' => 'Выберите событие',
    'and' => 'и',
    'actionEvent' => 'Подайте заявку на участие',
    'addWork' => 'Прикрепите работу и осуществите оплату',
    'waitEmail' => 'Ожидайте результаты и новости на свой емейл',


    // info
    'info' => 'F.A.Q.',


    // event.blade.php
    'participate' => 'принять участие',
    'application' => 'Заявка на участие',
    'select_section' => 'Выбор секции',
    // 'Выбор' => 'Зробіть вибір' из user/info/create.blade
    'name_work' => 'Название работы',
    'soavtor' => 'Авторы работы',
    'example' => 'например: Иванов И.И., Петров П.П. и т.д.',
    'form_part' => 'Форма участия',
    'additional_services' => 'Дополнительные услуги',
    'translation' => 'Перевод работы и публикация в доп.сборнике (+100 грн)',
    'NBook' => 'Количество дополнительных(!) сборников (+50 грн/шт)',
    'send' => 'Подать заявку',
    'generalInfo' => 'Общая информация',
    'description' => 'Описание',
    'target' => 'Цель',
    'lang' => 'Языки',
    'other' => 'Прочее',
    'sections' => 'Перечень секций',
    'designRules' => 'Правила оформления',
    'payment' => 'Стоимость и оплата',
    'orgcom' => 'Оргкомитет',
    'docs' => 'Документы',


    // _sidebar.blade.php
    'AllEvents' => 'ВСЕ СОБЫТИЯ',
    'MyEvents' => 'МОИ СОБЫТИЯ',
    'user' => 'Пользователь',
    'admin' => 'Администратор',
    'general' => 'Генерал',


    // _modal.blade
    'E-Mail Address' => 'E-mail адрес',
    'Password' => 'Пароль',
    'Remember Me' => 'Запомнить',
    'Confirm Password' => 'Пароль ещё раз',


    //auth/verify.blade
    'Verify Your Email Address' => 'Подтверждение электронной почты',
    'A fresh verification link has been sent to your email address.' => 'На Вашу электронную почту отправлено новое письмо.',
    'Before proceeding, please check your email for a verification link.' => 'На Вашу электронную почту отправлено письмо для подтверждения регистрации.',
    'If you did not receive the email' => 'Если Вы не получили письмо нажмите ',
    'click here to request another' => 'здесь',

    // vendor\laravel\framework\src\Illuminate\Auth\Notifications\VerifyEmail.php письмо верификации
    'Verify Email Address' => 'Подтверждение электронного адреса',
    'Please click the button below to verify your email address.' => 'Пожалуйста, нажмите кнопку ниже, чтобы подтвердить свой адрес электронной почты.',
    'If you did not create an account, no further action is required.' => 'Если вы не создали учетную запись, никаких дальнейших действий не требуется.',

    // vendor\laravel\framework\src\Illuminate\Notifications\resources\views\email.blade.php
    'If you’re having trouble clicking the button, copy and paste the URL below into your web browser:' => 'Если у Вас возникли проблемы с нажатием кнопки, скопируйте и вставьте в веб-браузер приведённый ниже URL-адрес: ',

    // vendor\laravel\framework\src\Illuminate\Mail\resources\views\html\header.blade.php
    'Calendar of scientific events' => 'Календарь научных событий',


    // user/myref
    'refs' => 'Мои рефералы',

    // user/info/create.blade.php
    'titleUserInfoCreate' => 'Персональные данные',
    'titleUserInfoEdit' => 'Редактирование персональных данных',
    'Формат' => 'изображение в формате png, jpg або jpeg',
    'Дата рождения' => 'Дата рождения',
    'phone' => 'Телефон',
    'country' => 'Страна',
    'questionTitle' => 'Не нашли?',
    'questionBody' => 'Выберите ',
    'NoCountry' => '"Страна не найдена"',
    'NoCity' => '"Город не найден"',
    'NoUniversity' => '"Университет не найден"',
    'site' => 'Адрес сайта',
    'Выбор' => 'Сделайте выбор',
    'city' => 'Город',
    'university' => 'Навчальній заклад',
    'surname' => 'Фамилия',
    'name' => 'Имя',
    'middle_name' => 'Отчество',
    'degree' => 'Звание',
    'position' => 'Должность',
    'save' => 'Сохранить',

    // user/info/create.blade
    'bodyUserInfoEdit' => 'Введите следующие данные в ручном режиме',


    // user/events.blade.php
    'Thank' => 'Спасибо!',
    'loadFile' => 'Прикрепите файл работы в формате .doc или .docx',
    'datasave' => 'Ваши данные были сохранены',
    'titleUserEvents' => 'Листинг научных событий',
    'activeEvents' => 'Активные события',
    'uchast' => 'Участие в конференции',
    'dodBook' => 'Дополнительніе сборники',
    'engBook' => 'Английский сборник',
    'all' => 'Всего',
    'section' => 'Секция',
    // 'name_work' => 'Назва роботи',
    'avtors' => 'Авторы',
    // 'form_part' => 'Форма участия' из event.blade.php
    'services' => 'Услуги',
    'osnBook' => 'обязательный сборник',
    'dopBook' => 'дополнительный сборник',
    'dopBooks' => 'дополнительных сборника',
    'enBook' => 'английский сборник',
    'sertificate' => 'сертификат',
    'activeEventsError1' => 'В данный момент Вы не принимаете участия в каких-либо событиях.',
    'activeEventsError2' => 'Выберите подходящее событие на странице',
    'actualeEvents' => 'Актуальные события',
    'archiveEvents' => 'Архив событий',
    'archiveEventsError1' => 'Завершённых событий с Вашим участием ещё нет',

    // _modal_pay
    'payOk' => 'Этот счёт оплачен',
];
