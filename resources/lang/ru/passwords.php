<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Языковые Строки Сброса Пароля
    |--------------------------------------------------------------------------
    |
    | Следующие языковые строки являются строками по умолчанию,
    | которые соответствуют причинам, указанным посредником паролей для неудачной попытки обновления пароля,
    | например, для неверного токена или неверного нового пароля
    |
    */

    'password' => 'Пароли должны содержать не менее шести символов и соответствовать подтверждению',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Мы отправили вам ссылку для сброса пароля по электронной почте!',
    'token' => 'Этот токен сброса пароля недействителен',
    'user' => "Мы не можем найти пользователя с таким адресом электронной почты",

];
