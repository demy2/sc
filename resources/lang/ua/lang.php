<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    // App/Participant
    'LiqPayDescription' => 'За участь у конференції',


    // index.blade.php
    'Login' => 'Вхід',
    'Logout' => 'Вихід',
    'Register' => 'Реєстрація',
    'ResetPas' => 'Відновлення паролю',
    'ActiveEvents' => 'Активні події',
    'edit' => 'Редагувати профіль',
    'ref' => 'Мої реферали',
    'start' => 'Почніть роботу з входу або реєстрації',
    'infoUser' => 'За потреби заповніть анкетні дані',
    'selEvent' => 'Оберіть подію',
    'and' => 'та',
    'actionEvent' => 'Подайте заявку на участь',
    'addWork' => 'Додайте роботу та здійсніть оплату',
    'waitEmail' => 'Очікуйте результати та новини на свій емейл',


    // info
    'info' => 'F.A.Q.',


    // event.blade.php
    'participate' => 'узяти участь',
    'application' => 'Заявка на участь',
    'select_section' => 'Вибір секції',
    // 'Выбор' => 'Зробіть вибір' из user/info/create.blade
    'name_work' => 'Назва роботи',
    'soavtor' => 'Автори роботи',
    'example' => 'наприклад: Іванов І.І., Петров П.П. і т.д.',
    'form_part' => 'Форма участі',
    'additional_services' => 'Додаткові послуги',
    'translation' => 'Переклад роботи та публікація в дод.збірнику (+100 грн)',
    'NBook' => 'Кількість додаткових(!) збірників (+50 грн/шт)',
    'send' => 'Подати заявку',
    'generalInfo' => 'Загальна інформація',
    'description' => 'Опис',
    'target' => 'Мета',
    'lang' => 'Мови',
    'other' => 'Інше',
    'sections' => 'Перелік секцій',
    'designRules' => 'Правила оформлення',
    'payment' => 'Вартість та оплата',
    'orgcom' => 'Оргкомітет',
    'docs' => 'Документи',


    // _sidebar.blade.php
    'AllEvents' => 'ВСІ ПОДІЇ',
    'MyEvents' => 'МОЇ ПОДІЇ',
    'user' => 'Користувач',
    'admin' => 'Адміністратор',
    'general' => 'Генерал',


    // _modal.blade
    // auth/login.blade
    // auth/register.blade,
    'E-Mail Address' => 'E-mail адреса',
    'Password' => 'Пароль',
    'Remember Me' => 'Запам\'ятати',
    'Confirm Password' => 'Пароль ще раз',


    // auth/verify.blade
    'Verify Your Email Address' => 'Підтвердження електронної адреси',
    'A fresh verification link has been sent to your email address.' => 'На Вашу електронну адресу надіслано новий лист.',
    'Before proceeding, please check your email for a verification link.' => 'На Вашу електронну адресу надіслано лист для підтвердження реєстрації.',
    'If you did not receive the email' => 'Якщо Ви не отримали електронний лист, натисніть ',
    'click here to request another' => 'тут',

    // vendor\laravel\framework\src\Illuminate\Auth\Notifications\VerifyEmail.php письмо верификации
    'Verify Email Address' => 'Підтвердження електронної адреси',
    'Please click the button below to verify your email address.' => 'Натисніть кнопку нижче, щоб підтвердити свою адресу електронної пошти.',
    'If you did not create an account, no further action is required.' => 'Якщо Ви не створювали обліковий запис, подальших дій не потрібно.',

    // vendor\laravel\framework\src\Illuminate\Notifications\resources\views\email.blade.php
    'If you’re having trouble clicking the button, copy and paste the URL below into your web browser:' => 'Якщо у Вас виникли проблеми з натисканням кнопки, скопіюйте та вставте у веб-браузер наступну URL-адресу: ',

    // vendor\laravel\framework\src\Illuminate\Mail\resources\views\html\header.blade.php
    'Calendar of scientific events' => 'Календар наукових подій',


    // user/myref
    'refs' => 'Мої реферали',

    // user/info/create.blade
    'titleUserInfoCreate' => 'Персональні дані',
    'titleUserInfoEdit' => 'Редагування персональних даних',
    'Формат' => 'зображення у форматі png, jpg або jpeg',
    'Дата рождения' => 'Дата народження',
    'phone' => 'Телефон',
    'country' => 'Країна',
    'questionTitle' => 'Не знайшли?',
    'questionBody' => 'оберіть ',
    'NoCountry' => '"Країну не знайдено"',
    'NoCity' => '"Місто не знайдено"',
    'NoUniversity' => '"Університет не знайдено"',
    'site' => 'Адреса сайту',
    'Выбор' => 'Зробіть вибір',
    'city' => 'Місто',
    'university' => 'Навчальній заклад',
    'surname' => 'Прізвище',
    'name' => 'Ім\'я',
    'middle_name' => 'По батькові',
    'degree' => 'Звання',
    'position' => 'Посада',
    'save' => 'Зберегти',

    // user/info/create.blade
    'bodyUserInfoEdit' => 'Введіть наступні дані в ручному режимі',


    // user/events.blade
    'Thank' => 'Дякуємо!',
    'loadFile' => 'Додайте файл роботи у форматі .doc або .docx',
    'datasave' => 'Ваші дані було збережено',
    'titleUserEvents' => 'Перелік наукових подій',
    'activeEvents' => 'Активні події',
    'uchast' => 'Участь у конференції',
    'dodBook' => 'Додаткові збірники',
    'engBook' => 'Англомовний збірник',
    'all' => 'Всього',
    'section' => 'Секція',
    // 'name_work' => 'Назва роботи' из event.blade.php
    'avtors' => 'Автори',
    // 'form_part' => 'Форма участия' из event.blade.php
    'services' => 'Послуги',
    'osnBook' => 'обов\'язковий збірник',
    'dopBook' => 'додатковий збірник',
    'dopBooks' => 'додаткових збірники',
    'enBook' => 'англомовний збірник',
    'sertificate' => 'сертифікат',
    'activeEventsError1' => 'На даний час Ви не берете участі в подіях.',
    'activeEventsError2' => 'Оберіть подію на сторінці',
    'actualeEvents' => 'Актуальні події',
    'archiveEvents' => 'Архів подій',
    'archiveEventsError1' => 'Завершених подій з Вашою участю ще немає',

    // _modal_pay
    'payOk' => 'Цей рахунок сплачено',

];
