@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary">
        <div class="card-header clearfix deep-purple-text h4">
            <h1 class="text-uppercase float-left">{{ __('lang.info') }}</h1>
        </div>
        <div class="card-body deep-purple-text" style="min-height: 600px">
            <section class="content">
                <div class="container-fluid">

                    <div class="row justify-content-md-center">
                        <div class="col-md-8">
                            <!-- Timeline -->
                                <div class="timeline-main">
                                        <!-- Timeline Wrapper -->
                                        <ul class="stepper stepper-vertical timeline timeline-animated timeline-simple timeline-images pl-0">

                                            <li>
                                                <!--Section Title -->
                                                <a href="#!">
                                                    <span class="circle blue lighten-1 z-depth-1-half">1</span>
                                                </a>

                                                <!-- Section Description -->
                                                <div class="step-content ml-3 p-0 hoverable h5">
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="text-right mr-5" style="font-weight:500">
                                                                {{ __('lang.start') }}
                                                            </td>
                                                            <td width="90px" >
                                                                <img src="/img/login.png" class="m-3" alt="" height="80px">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </li>
                                            <li class="timeline-inverted">
                                                <!--Section Title -->
                                                <a href="#!">
                                                    <span class="circle blue lighten-1">2</span>
                                                </a>

                                                <!-- Section Description -->
                                                <div class="step-content mr-xl-3 p-0 hoverable h5">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="90px">
                                                                <img src="/img/event.png" class="m-3" alt="" height="80px">
                                                            </td>
                                                            <td style="font-weight:500">
                                                                {{ __('lang.infoUser') }}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </li>
                                            <li>
                                                <!--Section Title -->
                                                <a href="#!">
                                                    <span class="circle blue lighten-1">3</span>
                                                </a>

                                                <!-- Section Description -->
                                                <div class="step-content ml-3 p-0 hoverable h5">
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="text-right " style="font-weight:500">
                                                                {{ __('lang.selEvent') }} {{ __('lang.and') }} <span class="text-lowercase">{{ __('lang.actionEvent') }}</span>
                                                            </td>
                                                            <td width="90px" >
                                                                <img src="/img/info.png" class="m-3" alt="" height="80px">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </li>
                                            <li class="timeline-inverted">
                                                <!--Section Title -->
                                                <a href="#!">
                                                    <span class="circle blue lighten-1">4</span>
                                                </a>

                                                <!-- Section Description -->
                                                <div class="step-content mr-xl-3 p-0 hoverable h5">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="90px">
                                                                <img src="/img/work.png" class="m-3" alt="" height="80px">
                                                            </td>
                                                            <td style="font-weight:500">
                                                                {{ __('lang.addWork') }}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </li>
                                            <li>
                                                <!--Section Title -->
                                                <a href="#!">
                                                    <span class="circle blue lighten-1">5</span>
                                                </a>

                                                <!-- Section Description -->
                                                <div class="step-content ml-3 p-0 hoverable h5">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="90%" class="text-right" style="font-weight:500">
                                                                {{ __('lang.waitEmail') }}
                                                            </td>
                                                            <td width="90px">
                                                                <img src="/img/mail.gif" class="m-3" alt="" height="80px">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </li>
                                        </ul>
                                        <!-- Timeline Wrapper -->
                                    </div>
                            <!-- Timeline -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <script type="text/javascript" src="/js/addons-pro/timeline.js"></script>
@endsection
