@extends('emails.header')

@section('content')
    <p>&nbsp;</p>
    <p>Шановний(а) {!!$user->name!!} {!!$user->surname!!}!</p>
    <p>Ви подали заявку на участь у конференції</p>
    <p>"{!! $user->event !!}"</p>
    <p>для роботи</p>
    <p>"{!! $user->work !!}"</p>
    <p>{!! $user->avtor !!}</p>
    <p>&nbsp;</p>
    {!!$user->text!!}
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>З повагою! Адміністрація сайту <a href="http://sciendar.com/">sciendar.com</a></p>
    <p>&nbsp;</p>
    <p>P.S. Будемо дуже вдячні за зауваження та/чи пропозиції!</p>
    <p>&nbsp;</p>
@endsection

