@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3">
        <div class="card-header"><b>Листинг данных</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">

                <section class="content">

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="category-tab" data-toggle="tab" href="#category" role="tab" aria-controls="category" aria-selected="true">Категории</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="position-tab" data-toggle="tab" href="#position" role="tab" aria-controls="position" aria-selected="false">Должности</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="degree-tab" data-toggle="tab" href="#degree" role="tab" aria-controls="degree" aria-selected="false">Звания</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="category" role="tabpanel" aria-labelledby="category-tab"><br>
                            <div class="form-group">
                                <a href="{{ route('general.categories.create') }}" class="btn btn-success">Добавить</a>
                            </div>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Image</th>
                                    <th><img src="/img/flag-ua.png" alt="" width="30 px"> Назва</th>
                                    <th><img src="/img/flag-ru.png" alt="" width="30 px"> Название</th>
                                    <th><img src="/img/flag-en.png" alt="" width="30 px"> Title</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->id }}</td>
                                        <td>{!! $category->image !!}</td>
                                        <td>{{ $category->title_ua }}</td>
                                        <td>{{ $category->title_ru }}</td>
                                        <td>{{ $category->title_en }}</td>
                                        <td>
                                            <div class="d-inline-block">
                                                <a class="btn btn-sm btn-outline-primary waves-effect" href="{{route('general.categories.edit', $category->id)}}"><i class="far fa-edit fa-2x"></i></a>
                                            </div>
                                            <div class="d-inline-block">
                                                {{ Form::open(['route'=>['general.categories.destroy', $category->id], 'method'=>'delete']) }}
                                                <button class="btn btn-sm btn-outline-danger waves-effect" onclick="return confirm('Точно удалить категорию???')" type="submit" class="delete"><i class="fas fa-times fa-2x"></i></button>
                                                {{ Form::close() }}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tfoot>
                            </table>
                        </div>

                        <div class="tab-pane fade" id="position" role="tabpanel" aria-labelledby="position-tab"><br>
                            <div class="form-group">
                                <a href="{{ route('general.positions.create') }}" class="btn btn-success">Добавить</a>
                            </div>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th><img src="/img/flag-ua.png" alt="" width="30 px"> Назва</th>
                                    <th><img src="/img/flag-ru.png" alt="" width="30 px"> Название</th>
                                    <th><img src="/img/flag-en.png" alt="" width="30 px"> Title</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($positions as $position)
                                    <tr>
                                        <td>{{ $position->id }}</td>
                                        <td>{{ $position->title_ua }}</td>
                                        <td>{{ $position->title_ru }}</td>
                                        <td>{{ $position->title_en }}</td>
                                        <td>
                                            <div class="d-inline-block">
                                                <a class="btn btn-sm btn-outline-primary waves-effect" href="{{route('general.positions.edit', $position->id)}}"><i class="far fa-edit fa-2x"></i></a>
                                            </div>
                                            <div class="d-inline-block">
                                                {{ Form::open(['route'=>['general.positions.destroy', $position->id], 'method'=>'delete']) }}
                                                <button class="btn btn-sm btn-outline-danger waves-effect" onclick="return confirm('Точно удалить должность???')" type="submit" class="delete"><i class="fas fa-times fa-2x"></i></button>
                                                {{ Form::close() }}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tfoot>
                            </table>
                        </div>

                        <div class="tab-pane fade" id="degree" role="tabpanel" aria-labelledby="degree-tab"><br>
                            <div class="form-group">
                                <a href="{{ route('general.degrees.create') }}" class="btn btn-success">Добавить</a>
                            </div>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th><img src="/img/flag-ua.png" alt="" width="30 px"> Назва</th>
                                    <th><img src="/img/flag-ru.png" alt="" width="30 px"> Название</th>
                                    <th><img src="/img/flag-en.png" alt="" width="30 px"> Title</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($degrees as $degree)
                                    <tr>
                                        <td>{{ $degree->id }}</td>
                                        <td>{{ $degree->title_ua }}</td>
                                        <td>{{ $degree->title_ru }}</td>
                                        <td>{{ $degree->title_en }}</td>
                                        <td>
                                            <div class="d-inline-block">
                                                <a class="btn btn-sm btn-outline-primary waves-effect" href="{{route('general.degrees.edit', $degree->id)}}"><i class="far fa-edit fa-2x"></i></a>
                                            </div>
                                            <div class="d-inline-block">
                                                {{ Form::open(['route'=>['general.degrees.destroy', $degree->id], 'method'=>'delete']) }}
                                                <button class="delete btn btn-sm btn-outline-danger waves-effect" onclick="return confirm('Точно удалить звание???')" type="submit" class=""><i class="fas fa-times fa-2x"></i></button>
                                                {{ Form::close() }}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tfoot>
                            </table>
                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>
@endsection

