@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3 deep-purple-text">
        <div class="card-header"><b>Список вероятных участников</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">
                <section class="content">




                    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" style="color: #4c00be">
                        <thead>
                        <tr class="text-center">
                            <th>№</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th>Фамилия</th>
                            <th>Имя</th>
                            <th>Отчество</th>
                            <th>Пол</th>
                            <th>ДР</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                    <tr class="text-center">
                                        <td class="align-middle">@isset($user->id){{ $user->id }}@endisset</td>
                                        <td class="align-middle">@isset($user->email){{ $user->email }}@endisset</td>
                                        <td class="align-middle text-left">@isset($user->phone){{ $user->phone }}@endisset</td>
                                        <td class="align-middle text-left">@isset($user->fname){{ $user->fname }}@endisset</td>
                                        <td class="align-middle text-left">@isset($user->name){{ $user->name }}@endisset</td>
                                        <td class="align-middle">@isset($user->mname){{ $user->mname }}@endisset</td>
                                        <td class="align-middle">@isset($user->sex){{ $user->sex }}@endisset</td>
                                        <td class="align-middle text-left">@isset($user->bdate){{ $user->bdate }}@endisset</td>
                                    </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="text-center">
                            <th></th>
                            <th></th>
                            <th class="align-middle"></th>
                            <th class="align-middle">
                                <input type="checkbox" class="form-check-input" id="checkall">
                                <label for="checkall">выбрать усех<input type="checkbox" id="checkall" ></label>
                            </th>
                            <th  class="align-middle" colspan="2">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal" id="sm" style="display: none;">
                                    SEND <i class="far fa-paper-plane"></i>
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Отправка писем выбранным участникам</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <textarea name="design_rules_ru" id="" cols="30" rows="10" class="form-control" >{{ old('design_rules_ru') }}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" class="btn btn-primary" name="sent-mail-submit" value="send mail">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </th>

                            <th class="align-middle">5 | 3 | 2 | 0</th>
                            <th class="align-middle">4 | 6 | 1 | 0 | 2</th>

                            <!--th class="align-middle"></th-->
                        </tr>

                        <!--tr>
                            <td>11111</td>
                            <td><img src="http://sciendar/assets/dist/img/user2-160x160.jpg" width="50px" alt=""></td>
                            <td>Фамилия Имя Отчество</td>
                            <td>mail@gmail.com</td>
                            <td>067 000 00 00</td>
                            <td>3</td>
                            <td>Название работы</td>
                            <td>Очная</td>
                            <td>2</td>
                            <td>150</td>
                            <td>26.11.18</td>
                            <td></td>
                        </tr-->
                        </tfoot>
                    </table>
                    <div class="testing"></div>
                </section>
            </div>
        </div>
    </div>

@endsection
