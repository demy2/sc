@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3">
        <div class="card-header"><b>Редактирование звания</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">

                <section class="content">

                    {!! Form::open(['route'=>['general.degrees.update', $degrees->id], 'method'=>'put']) !!}
                    @include('_errors')
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Назва</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{!! $degrees->title_ua !!}" name="title_ua">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Название</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{!! $degrees->title_ru !!}" name="title_ru">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Title</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{!! $degrees->title_en !!}" name="title_en">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ route('general.info') }}" class="btn btn-default">Назад</a>
                        <button class="btn btn-success pull-right">Изменить</button>
                    </div>
                    <!-- /.box-footer-->
                    {!! Form::close() !!}

                </section>

            </div>
        </div>
    </div>
@endsection
