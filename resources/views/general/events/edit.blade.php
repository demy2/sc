@extends('index')

@section('content')
    <br><br><br>
    @include('_errors')
    <div class="card border-primary deep-purple-text">
        {{Form::open([
              'route'	=> ['general.events.update', $event->id],
              'method'	=>	'put'
          ])}}

        <div class="card-header"><b>Перелік наукових подій</b></div>
        <div class="card-body">
            <div class="container-fluid">
                <section class="content">
                    <input type="hidden" name="status" value="1">
                    <input type="hidden" name="country" value="1">

                    <div class="row">
                        <div class="col-md-4">
                            <label for="title_ua"><b>Назва події</b></label>
                            <input type="text" class="form-control" id="title_ua" placeholder="" name="title_ua" value="{{ $event->title_ua }}">
                        </div>
                        <div class="col-md-4">
                            <label for="univer"><b>ВНЗ</b></label>
                            <input type="text" class="form-control" id="univer" placeholder="" name="univer" value="{{ $event->univer }}">
                        </div>
                        <div class="col-md-4">
                            <label><b>Дата начала и конца проведения события:</b></label>
                            <div class="input-group">
                                {!! Form::date('date_open', $event->date_open, ['class' => 'form-control', 'id'    =>  'datepicker3']) !!}
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt" style="color: #4c00be"></i></span>
                                </div>
                                {!! Form::date('date_close', $event->date_close, ['class' => 'form-control', 'id'    =>  'datepicker4']) !!}
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="phone"><b>Категории</b></label><br>
                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isChemistry" name="isChemistry" value="1" @if($event->isChemistry == 1)checked @endif>
                                <label class="form-check-label" for="isChemistry"><i class="fas fa-flask"></i></label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isPhysics" name="isPhysics" value="1" @if($event->isPhysics == 1)checked @endif>
                                <label class="form-check-label" for="isPhysics"><i class="fab fa-react"></i></label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isBiology" name="isBiology" value="1" @if($event->isBiology == 1)checked @endif>
                                <label class="form-check-label" for="isBiology"><i class="fas fa-dna"></i></label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isMathematics" name="isMathematics" value="1" @if($event->isMathematics == 1)checked @endif>
                                <label class="form-check-label" for="isMathematics"><i class="fas fa-square-root-alt"></i></label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isEconomics" name="isEconomics" value="1" @if($event->isEconomics == 1)checked @endif>
                                <label class="form-check-label" for="isEconomics"><i class="fas fa-dollar-sign"></i></label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isHumanities" name="isHumanities" value="1" @if($event->isHumanities == 1)checked @endif>
                                <label class="form-check-label" for="isHumanities"><i class="fas fa-street-view"></i></label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="FIO"><b>Відповідальний</b></label>
                            <input type="text" class="form-control" id="FIO" placeholder="" name="FIO" value="{{ $event->FIO }}">
                        </div>
                        <div class="col-md-4">
                            <label for="phone"><b>Телефон</b></label>
                            <input type="text" class="form-control" id="phone" placeholder="" name="phone" value="{{ $event->phone }}">
                        </div>
                        <div class="col-md-4">
                            <label for="email"><b>Емейл</b></label>
                            <input type="text" class="form-control" id="email" placeholder="" name="email" value="{{ $event->email }}">
                        </div>
                    </div>
                    <br>

                </section>
            </div>
        </div>
        <div class="card-footer">
            <div class="clearfix">
                <a href="/general/events" class="btn btn-default float-left">Назад</a>
                <button class="btn btn-success float-right">Изменить</button>
            </div>
        </div>

        {{Form::close()}}
    </div>


    <script>
        // Material Select Initialization
        $(document).ready(function() {
            $('.mdb-select').materialSelect();
        });

    </script>
@endsection
