@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary deep-purple-text">
        <div class="card-header"><b>Перелік наукових подій</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">
                <section class="content">
                    <div class="form-group">
                        <a href="{{ route('general.events.create') }}" class="btn btn-success">Добавить</a>
                    </div>

                    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" style="color: #4c00be">
                        <thead>
                        <tr class="text-center">
                            <th>№</th>
                            <th>Дата проведення</th>
                            <th>Назва конференції</th>
                            <th>Заклад вищої освіти (установа)</th>
                            <th>Контакти</th>
                            <th>Категории</th>
                            <th>Дії</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($events as $event)
                            <tr class="text-center">
                                <td class="align-middle">{{$loop->iteration}}</td>
                                <td class="align-middle">{{ $event->dateOnOff($event) }}</td>
                                <td class="align-middle text-left">{{ $event->title_ua }}</td>
                                <td class="align-middle text-left">{{ $event->univer }}</td>
                                <td class="align-middle">
                                    @isset($event->FIO){{ $event->FIO }}<br>@endisset
                                    @isset($event->phone){{ $event->phone }}<br>@endisset
                                    @isset($event->email){{ $event->email }}@endisset
                                </td>
                                <td class="align-middle">
                                    @if($event->isChemistry == 1)<i class="fas fa-flask"></i> @endif
                                    @if($event->isPhysics == 1)<i class="fab fa-react"></i> @endif
                                    @if($event->isBiology == 1)<i class="fas fa-dna"></i> @endif
                                    @if($event->isMathematics == 1)<i class="fas fa-square-root-alt"></i> @endif
                                    @if($event->isEconomics == 1)<i class="fas fa-dollar-sign"></i> @endif
                                    @if($event->isHumanities == 1)<i class="fas fa-street-view"></i> @endif
                                </td>
                                <td class="align-middle">
                                    <div class="d-inline-block">
                                        <a href="{{route('general.events.edit', $event->id)}}" class="btn btn-sm btn-outline-primary waves-effect"><i class="far fa-edit fa-2x"></i></a>
                                    </div>
                                    <div class="d-inline-block">
                                    {{ Form::open(['route'=>['general.events.destroy', $event->id], 'method'=>'delete']) }}
                                    <!-- ???? --><button class="delete btn btn-sm btn-outline-danger waves-effect" onclick="return confirm('Точно удалить событие???')" type="submit"><i class="fas fa-times fa-2x"></i></button>
                                        {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="text-center">
                            <th>№</th>
                            <th>Дата проведення</th>
                            <th>Назва конференції</th>
                            <th>Заклад вищої освіти (установа)</th>
                            <th>Контакти</th>
                            <th>Категории</th>
                            <th>Дії</th>
                        </tr>
                        </tfoot>
                    </table>
                </section>
            </div>
        </div>
    </div>
@endsection
