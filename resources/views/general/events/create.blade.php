@extends('index')

@section('content')
    <br><br><br>
    @include('_errors')
    <div class="card border-primary deep-purple-text">
        {{Form::open([
              'route' => 'general.events.store'
          ])}}

        <div class="card-header"><b>Перелік наукових подій</b></div>
        <div class="card-body">
            <div class="container-fluid">
                <section class="content">
                    <input type="hidden" name="status" value="1">
                    <input type="hidden" name="country" value="1">

                    <div class="row">
                        <div class="col-md-4">
                            <label for="title_ua"><b>Назва події</b></label>
                            <input type="text" class="form-control" id="title_ua" placeholder="" name="title_ua" value="{{ old('title_ua') }}">
                        </div>
                        <div class="col-md-4">
                            <label for="univer"><b>ВНЗ</b></label>
                            <input type="text" class="form-control" id="univer" placeholder="" name="univer" value="{{ old('univer') }}">
                        </div>
                        <div class="col-md-4">
                            <label><b>Дата начала и конца проведения события:</b></label>
                            <div class="input-group">
                                {!! Form::date('date_open', \Carbon\Carbon::now(), ['class' => 'form-control', 'id'    =>  'datepicker3']) !!}
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt" style="color: #4c00be"></i></span>
                                </div>
                                {!! Form::date('date_close', \Carbon\Carbon::now(), ['class' => 'form-control', 'id'    =>  'datepicker4']) !!}
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="phone"><b>Категории</b></label><br>
                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isChemistry" name="isChemistry" value="1">
                                <label class="form-check-label" for="isChemistry"><i class="fas fa-flask"></i></label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isPhysics" name="isPhysics" value="1">
                                <label class="form-check-label" for="isPhysics"><i class="fab fa-react"></i></label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isBiology" name="isBiology" value="1">
                                <label class="form-check-label" for="isBiology"><i class="fas fa-dna"></i></label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isMathematics" name="isMathematics" value="1">
                                <label class="form-check-label" for="isMathematics"><i class="fas fa-square-root-alt"></i></label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isEconomics" name="isEconomics" value="1">
                                <label class="form-check-label" for="isEconomics"><i class="fas fa-dollar-sign"></i></label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="isHumanities" name="isHumanities" value="1">
                                <label class="form-check-label" for="isHumanities"><i class="fas fa-street-view"></i></label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="FIO"><b>Відповідальний</b></label>
                            <input type="text" class="form-control" id="FIO" placeholder="" name="FIO" value="{{ old('FIO') }}">
                        </div>
                        <div class="col-md-4">
                            <label for="phone"><b>Телефон</b></label>
                            <input type="text" class="form-control" id="phone" placeholder="" name="phone" value="{{ old('phone') }}">
                        </div>
                        <div class="col-md-4">
                            <label for="email"><b>Емейл</b></label>
                            <input type="text" class="form-control" id="email" placeholder="" name="email" value="{{ old('email') }}">
                        </div>
                    </div>
                    <br>

                </section>
            </div>
        </div>
        <div class="card-footer">
            <div class="clearfix">
                <a href="/general/events" class="btn btn-default float-left">Назад</a>
                <button class="btn btn-success float-right">Добавить</button>
            </div>
        </div>

        {{Form::close()}}
    </div>



@endsection
