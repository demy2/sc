@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3">
        <div class="card-header"><b>{{ $countries->title_ru }} - добавление города</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">

                <section class="content">
                    @include('_errors')
                        {!! Form::open(['route'=>['general.univers.store', $countries->id, $cities->id]]) !!}

                        <div class="row">
                            <input type="hidden" value="{{ $cities->id }}" name="cities_id">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Назва</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('title_ua') }}" name="title_ua">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Название</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('title_ru') }}" name="title_ru">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Title</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('title_en') }}" name="title_en">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Коротка назва</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('short_title_ua') }}" name="short_title_ua">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Краткое название</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('short_title_ru') }}" name="short_title_ru">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Short title</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('short_title_en') }}" name="short_title_en">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">URL</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('url') }}" name="url">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-mail</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('email') }}" name="email">
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <a href="/general/country/{{ $countries->id }}/city/{{ $cities->id }}/univers" class="btn btn-default">Назад</a>
                            <button class="btn btn-success pull-right">Добавить</button>
                        </div>
                        {!! Form::close() !!}

                </section>

            </div>
        </div>
    </div>
@endsection
