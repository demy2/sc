@extends('index')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Изменить страну
                <small>приятные слова..</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                {!! Form::open(['route'=>['general.country.update', $country->id], 'method'=>'put']) !!}
                <div class="box-header with-border">
                    <h3 class="box-title">Изменяем страну</h3>
                    @include('_errors')
                </div>
                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Назва</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ $country->title_ua }}" name="title_ua">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Название</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ $country->title_ru }}" name="title_ru">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Title</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ $country->title_en }}" name="title_en">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="/general/country" class="btn btn-default">Назад</a>
                    <button class="btn btn-success pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
                {!! Form::close() !!}
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
@endsection

