@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3">
        <div class="card-header"><b>{{ $countries->title_ru }}, {{ $cities->title_ru }} - Листинг университетов</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">

                <section class="content">

                    <div class="form-group">
                        <a href="{{ route('general.univers.create', [$countries->id, $cities->id]) }}" class="btn btn-success">Добавить</a>
                    </div>

                    <table id="dtBasicExample" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th><img src="/img/flag-ua.png" alt="" width="30 px"> Універсітет</th>
                            <th><img src="/img/flag-ru.png" alt="" width="30 px"> Университет</th>
                            <th><img src="/img/flag-en.png" alt="" width="30 px"> University</th>
                            <th><img src="/img/flag-en.png" alt="" width="30 px"> Е-мейл</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($universities as $university)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td><a href="{{ $university->url }}">{{ $university->short_title_ua }}<br>{{ $university->title_ua }}</a></td>
                                <td>{{ $university->short_title_ru }}<br>{{ $university->title_ru }}</td>
                                <td>{{ $university->short_title_en }}<br>{{ $university->title_en }}</td>
                                <td>{{ $university->email }}</td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group mr-2" role="group" aria-label="Second group">
                                            <a href="{{route('general.univers.edit', ['country' => $countries->id, 'city' => $cities->id, $university->id])}}" class="btn btn-sm btn-outline-primary waves-effect"><i class="far fa-edit fa-2x"></i></a>

                                            {{ Form::open(['route'=>['general.univers.destroy', 'country' => $countries->id, 'city' => $cities->id, $university->id], 'method'=>'delete']) }}
                                            <button onclick="return confirm('Точно удалить город???')" type="submit" class="delete btn btn-sm btn-outline-danger waves-effect"><i class="fas fa-times fa-2x"></i></button>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>

                    <div class="box-footer">
                        <a href="/general/country/{{ $countries->id }}/city" class="btn btn-default">Назад</a>
                    </div>

                </section>

            </div>
        </div>
    </div>
@endsection
