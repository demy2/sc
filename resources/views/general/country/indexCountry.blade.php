@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3">
        <div class="card-header"><b>Листинг стран</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">

                <section class="content">

                    <div class="form-group">
                        <a href="{{ route('general.country.create') }}" class="btn btn-success">Добавить</a>
                    </div>
                    <table id="dtBasicExample" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th><img src="/img/flag-ua.png" alt="" width="30 px"> Країна</th>
                            <th><img src="/img/flag-ru.png" alt="" width="30 px"> Страна</th>
                            <th><img src="/img/flag-en.png" alt="" width="30 px"> Country</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($countries as $country)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td><a href="country/{{ $country->id }}/city">{{ $country->title_ua }}</a></td>
                                <td>{{ $country->title_ru }}</td>
                                <td>{{ $country->title_en }}</td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group mr-2" role="group" aria-label="Second group">
                                            <a href="{{route('general.country.edit', $country->id)}}" class="btn btn-sm btn-outline-primary waves-effect"><i class="far fa-edit fa-2x"></i></a>

                                            {{ Form::open(['route'=>['general.country.destroy', $country->id], 'method'=>'delete']) }}
                                            <button onclick="return confirm('Точно удалить страну???')" type="submit" class="delete btn btn-sm btn-outline-danger waves-effect"><i class="fas fa-times fa-2x"></i></button>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>

                </section>

            </div>
        </div>
    </div>
@endsection

