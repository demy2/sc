@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3">
        <div class="card-header"><b>Добавление страны</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">

                <section class="content">
                    @include('_errors')
                        {!! Form::open(['route'=>'general.country.store']) !!}

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Назва</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('title_ua') }}" name="title_ua">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Название</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('title_ru') }}" name="title_ru">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Title</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('title_en') }}" name="title_en">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="/general/country" class="btn btn-default">Назад</a>
                            <button class="btn btn-success pull-right">Добавить</button>
                        </div>
                        <!-- /.box-footer-->
                        {!! Form::close() !!}
                </section>

            </div>
        </div>
    </div>
@endsection
