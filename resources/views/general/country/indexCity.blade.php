@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3">
        <div class="card-header"><b>{{ $countries->title_ru }} - Листинг городов</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">

                <section class="content">

                    <div class="form-group">
                        <a href="{{ route('general.city.create', $countries->id) }}" class="btn btn-success">Добавить</a>
                    </div>

                    <table id="dtBasicExample" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th><img src="/img/flag-ua.png" alt="" width="30 px"> Місто</th>
                            <th><img src="/img/flag-ru.png" alt="" width="30 px"> Город</th>
                            <th><img src="/img/flag-en.png" alt="" width="30 px"> City</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cities as $city)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td><a href="city/{{ $city->id }}/univers">{{ $city->title_ua }}</a></td>
                                <td>{{ $city->title_ru }}</td>
                                <td>{{ $city->title_en }}</td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group mr-2" role="group" aria-label="Second group">
                                            <a href="{{route('general.city.edit', ['country' => $countries->id, 'city' => $city->id])}}" class="btn btn-sm btn-outline-primary waves-effect"><i class="far fa-edit fa-2x"></i></a>

                                            {{ Form::open(['route'=>['general.city.destroy', 'country' => $countries->id, 'city' => $city->id], 'method'=>'delete']) }}
                                            <button onclick="return confirm('Точно удалить город???')" type="submit" class="delete btn btn-sm btn-outline-danger waves-effect"><i class="fas fa-times fa-2x"></i></button>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>

                    <div class="box-footer">
                        <a href="/general/country" class="btn btn-default">Назад</a>
                    </div>

                </section>

            </div>
        </div>
    </div>
@endsection
