@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3">
        <div class="card-header"><b>{{ $countries->title_ru }} - добавление города</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">

                <section class="content">
                    @include('_errors')
                    {!! Form::open(['route'=>['general.city.store', $countries->id]]) !!}

                    <div class="row">
                        <input type="hidden" value="{{ $countries->id }}" name="country_id">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Назва</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('title_ua') }}" name="title_ua">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Название</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('title_ru') }}" name="title_ru">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Title</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('title_en') }}" name="title_en">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="/general/country/{{ $countries->id }}/city" class="btn btn-default">Назад</a>
                        <button class="btn btn-success pull-right">Добавить</button>
                    </div>
                    <!-- /.box-footer-->
                    {!! Form::close() !!}
                </section>

            </div>
        </div>
    </div>
@endsection



@extends('index')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Добавить город
                <small>приятные слова..</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">

            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
@endsection

