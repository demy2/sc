@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary deep-purple-text">
        <div class="card-header"><b>Список участников "{{ $eventTitle }}"</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">
                <section class="content">

                    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" style="color: #4c00be">
                        <thead>
                        <tr class="text-center">
                            <th>№</th>
                            <th>ФИО</th>
                            <th>Страна | Город | ВУЗ</th>
                            <th>Секция</th>
                            <th>Участие</th>
                            <th>Работа</th>
                            <th>Сборник</th>
                            <th>Доставка</th>
                            <!--th>Действия</th-->
                        </tr>
                        </thead>
                        <tbody><?php $i = 0; $s1 = 0; $s2 = 0; $s3 = 0; $s4 = 0; $s5 = 0; $form1 = 0; $form2 = 0; $form3 = 0; $form4 = 0; $NBook = 0; $sumUAH = 0; $sumEUR = 0; $sumDop = 0 ?>
                        @if(isset($participants))
                            @foreach($participants as $participant)

                                <tr class="text-center">
                                    <td class="align-middle"><?php $i++;  echo "$i"?></td>
                                    <td class="align-middle text-left">{{ $participant->user->info->surname }} <br>{{ $participant->user->info->name }} {{ $participant->user->info->middle_name }}</td>
                                    <td class="align-middle text-left">
                                        @if(isset($participant->user->info->country->title_ua)){{ $participant->user->info->country->title_ua }}@endif<br>
                                        @if(isset($participant->user->info->city->title_ua)){{ $participant->user->info->city->title_ua }}@endif<br>
                                        @if(isset($participant->user->info->university->title_ua)){{ str_limit($participant->user->info->university->title_ua, 35) }}@endif
                                    </td>
                                    <td class="align-middle">{{ $participant->section_id }}</td>
                                    <td class="align-middle">
                                        @if($participant->form_id == 1) <?php $form1++ ?>
                                        @elseif($participant->form_id == 2) <?php $form2++ ?>
                                        @elseif($participant->form_id == 3) <?php $form3++ ?>
                                        @elseif($participant->form_id == 4) <?php $form4++ ?>
                                        @endif
                                        {{ $participant->form->$title }}
                                    </td>

                                    <td class="align-middle text-left">
                                        @if($participant->status == 1) <?php $color = 'lightgray'; $check ; $s1++?>
                                        @elseif($participant->status == 2) <?php $color = 'blue'; $s2++ ?>
                                        @elseif($participant->status == 3) <?php $color = 'limegreen'; $s3++ ?>
                                        @elseif($participant->status == 4) <?php $color = 'orange'; $s4++ ?>
                                        @elseif($participant->status == 5) <?php $color = 'red'; $s5++ ?>
                                        @endif

                                            <a href="#" style="color: {{ $color }}" title="{{ $participant->name }}">{{ str_limit($participant->name, 35) }}</a><br>
                                            <a href="#" style="color: {{ $color }}" title="{{ $participant->soavtor }}">{{ str_limit($participant->soavtor, 35) }}</a>
                                    </td>
                                    <td class="align-middle">
                                        <?php $NBook = $NBook + $participant->Nbook; $NB = $participant->Nbook + 1 ?>{{ $NB }}
                                            @if($participant->dopolnitelno == 'on') + А <?php $sumDop++ ?>@endif
                                    </td>
                                    <td class="align-middle">
                                        @if(isset($participant->user->info->surnameNP)){{ $participant->user->info->surnameNP }}@endif
                                        @if(isset($participant->user->info->nameNP)){{ $participant->user->info->nameNP }}@endif
                                        @if(isset($participant->user->info->middle_nameNP)){{ $participant->user->info->middle_nameNP }}@endif<br>
                                        @if(isset($participant->user->info->phoneNP)){{ $participant->user->info->phoneNP }}@endif
                                        @if(isset($participant->user->info->city_idNP)), {{ $participant->user->info->city_idNP }}@endif
                                        @if(isset($participant->user->info->oNP)), {{ $participant->user->info->oNP }}@endif
                                    </td>
                                    <!--td class="align-middle">
                                        @if($participant->pay == 1) <?php $color = 'red'; $titlePay = 'Не оплачено' ?>
                                        @elseif($participant->pay == 2) <?php $color = 'limegreen'; $titlePay = 'Оплачено';
                                        if($participant->currency == 'UAH'){$sumUAH = $sumUAH + $participant->amount;}
                                        else($participant->currency == 'EUR'){$sumEUR = $sumEUR + $participant->amount} ?>
                                        @elseif($participant->pay == 3) <?php $color = 'lightgray'; $titlePay = 'Льгота' ?>
                                        @endif
                                        <a href="#" style="color: {{ $color }}" title="{{ $titlePay }}">{{ $participant->infoPay($participant)['sumTxt'] }}</a><title>{{ $titlePay }}</title>
                                    </td-->
                                </tr>

                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <div class="testing"></div>
                </section>
            </div>
        </div>
    </div>
@endsection
