<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" style="color: #4c00be">
    <thead>
    <tr class="text-center">
        <th>№</th>
        <th>
            Дата проведения<br>
            Категории
        </th>
        <th>Логотип</th>
        <th>Назва | Название | Title</th>
        <th>Участники</th>
        <th>Действия</th>
    </tr>
    </thead>
    <tbody>
    @foreach($events->where('date_close','<',now()) as $event)
        <tr class="text-center">
            <td class="align-middle">{{ $loop->iteration }}</td>
            <td class="align-middle">
                <p><b>{{ $event->dateOnOff($event) }}</b></p>
                <p>
                    @foreach($event->categories as $category)
                        <a href="#" title="{{ $category->title_ua }}" style="color: rgb(76, 0, 190)">{!! $category->image !!}</a>
                    @endforeach
                </p>
            </td>
            <td><img src="{{ $event->getImage() }}" style="max-height: 100px" alt=""></td>
            <td class="align-middle text-left">
                <a class="text-primary" href="/event/{{ $event->id }}"><img src="/img/flag-ua.png" alt="" width="30 px"> {{ str_limit($event->title_ua, 50) }}</a><br>
                <a class="text-primary" href="/ru/event/{{ $event->id }}"><img src="/img/flag-ru.png" alt="" width="30 px"> {{ $event->title_ru }}</a><br>
                <a class="text-primary" href="/en/event/{{ $event->id }}"><img src="/img/flag-en.png" alt="" width="30 px"> {{ $event->title_en }}</a><br>
            </td>
            <td class="align-middle">
                <!-- Button trigger modal -->
                <a href="" class="h3 text-primary" data-toggle="modal" data-target="#sectionInfo{{$event->id}}"><b>{{ $event->countUserEvent($event) }}</b></a>

                <!-- Modal -->
                @include('admin.events._modal_statSectuin')
            </td>

            <td class="align-middle">
                <div class="d-inline-block">
                    <a href="{{route('admin.events.edit', $event->id)}}" class="btn btn-sm btn-outline-primary waves-effect"><i class="far fa-edit fa-2x"></i></a>
                </div>
                <div class="d-inline-block">
                {{ Form::open(['route'=>['admin.events.destroy', $event->id], 'method'=>'delete']) }}
                <!-- ???? --><button class="delete btn btn-sm btn-outline-danger waves-effect" onclick="return confirm('Точно удалить событие???')" type="submit"><i class="fas fa-times fa-2x"></i></button>
                    {{ Form::close() }}
                </div>
            </td>
        </tr>
    @endforeach
    </tfoot>
</table>
