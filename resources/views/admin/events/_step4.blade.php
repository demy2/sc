<div class="card border-primary deep-purple-text">
    <div class="card-header h5"><b>{{ __('lang.docs') }}</b></div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Правила оформлення наукових робіт</label>
                    <textarea name="docs_ua" id="" cols="30" rows="10" class="validate form-control" required>{{ old('docs_ua') }}</textarea>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Правила оформления научных работ</label>
                    <textarea name="docs_ru" id="" cols="30" rows="10" class="validate form-control" required>{{ old('docs_ru') }}</textarea>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Описание</label>
                    <textarea name="docs_en" id="" cols="30" rows="10" class="validate form-control" required>{{ old('docs_en') }}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="clearfix">
            <button class="btn btn-indigo waves-effect previous-step float-left">НАЗАД</button>
            <button class="btn btn-primary waves-effect float-right">СОХРАНИТЬ</button>
        </div>
    </div>
</div>
