<div class="card border-primary deep-purple-text">
    <div class="card-header h5"><b>{{ __('lang.section') }}</b></div>
    <div class="card-body">
        <table class="table table-hover" id="dynamic" width="100%">
            <thead>
            <tr>
                <th class="th-sm"></th>
                <th class="th-sm"><img src="/img/flag-ua.png" alt="" width="30 px"> Назва секції</th>
                <th class="th-sm"><img src="/img/flag-ru.png" alt="" width="30 px"> Название секции</th>
                <th class="th-sm"><img src="/img/flag-en.png" alt="" width="30 px"> Назва секції</th>
                <th class="th-sm"><a class="btn btn-sm btn-success" name="add" id="add"><i class="fas fa-plus"></i></a></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-right"><b>1. </b></td>
                <td><input type="text" class="validate form-control" id="exampleInputEmail1" placeholder="Назва секції" name="section_ua[]" required></td>
                <td><input type="text" class="validate form-control" id="exampleInputEmail1" placeholder="Название секции" name="section_ru[]" required></td>
                <td><input type="text" class="validate form-control" id="exampleInputEmail1" placeholder="placeholder" name="section_en[]" required></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <div class="clearfix">
            <button class="btn btn-indigo waves-effect previous-step float-left">НАЗАД</button>
            <button class="btn btn-primary waves-effect next-step float-right">ДАЛЕЕ</button>
        </div>
    </div>
</div>
