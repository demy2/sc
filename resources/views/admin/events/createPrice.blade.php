@extends('index')

@section('content')
    <br> <br>
    <div class="container">

        {{Form::open([
                         'route' => 'admin.price.store',
                         'files' =>  true
                     ])}}

        <div data-step-label="{{ __('lang.payment') }}" class="step-title waves-effect">{{$eventTitle}}</div>
        <input type="hidden" name="event_id" value="{{ $id }}">
        <div class="step-new-content">
            <div class="card border-primary deep-purple-text">
                <div class="card-header h5"><b>{{ __('lang.payment') }}</b></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label><b>Страна</b></label>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" id="country" name="country" value="country" checked>
                                <label class="form-check-label" for="country">Любая</label>
                            </div>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" id="country2" name="country" value="two">
                                <label class="form-check-label" for="country2">
                                    Разделить на:
                                    <ul>
                                        <li>Украина</li>
                                        <li>Не Украина</li>
                                    </ul>
                                </label>
                            </div>

                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="countryVuz" name="myVuz">
                                <label class="form-check-label" for="countryVuz"><b>Отдельная цена для родного ВНЗ</b></label>
                            </div>
                            <br><br>
                            <label><b>Форма участия</b></label>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" id="formAny" name="form" value="formAny" checked>
                                <label class="form-check-label" for="formAny">Любая</label>
                            </div>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" id="formTwo" name="form" value="formTwo">
                                <label class="form-check-label" for="formTwo">
                                    Разделить на:
                                    <ul>
                                        <li>очно/стенд</li>
                                        <li>заочно/онлай</li>
                                    </ul>
                                </label>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-4"><strong>Страна</strong></div>
                                <div class="col-md-4"><strong>Форма участия</strong></div>
                                <div class="col-md-4"><strong>Стоимость участия</strong></div>
                            </div>
                            <div id="trAny" class="row border">

                                <div class="col-md-4">любая</div>
                                <div class="col-md-8">

                                    <div class="row" id="anyForm1">
                                        <div class="col-md-6">любая</div><div class="col-md-6">
                                            <div class="input-group">
                                                <input type="text" aria-label="First name" class="form-control" name="price">
                                                <select class="browser-default custom-select" name="currency">
                                                    <option value="1" selected>грн</option>
                                                    <option value="2">euro</option>
                                                    <option value="3">dollar</option>
                                                </select>
                                            </div></div></div>
                                    <div id="notAnyForm1">
                                        <div class="row" id="ochno1">
                                            <div class="col-md-6">очно/стенд</div><div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="text" aria-label="First name" class="form-control" name="priceAO">
                                                    <select class="browser-default custom-select" name="currency">
                                                        <option value="1" selected>грн</option>
                                                        <option value="2">euro</option>
                                                        <option value="3">dollar</option>
                                                    </select>
                                                </div></div></div>

                                        <div class="row" id="zaochno1">
                                            <div class="col-md-6">заочно/онлайн</div><div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="text" aria-label="First name" class="form-control" name="priceAZ">
                                                    <select class="browser-default custom-select" name="currency">
                                                        <option value="1" selected>грн</option>
                                                        <option value="2">euro</option>
                                                        <option value="3">dollar</option>
                                                    </select>
                                                </div></div></div></div>
                                </div>
                            </div>
                            <div id="trRodnoy" class="row border">

                                <div class="col-md-4">Родной ВУЗ</div>
                                <div class="col-md-8">

                                    <div class="row" id="anyForm4">
                                        <div class="col-md-6">любая</div><div class="col-md-6">
                                            <div class="input-group">
                                                <input type="text" aria-label="First name" class="form-control" name="priceVA">
                                                <select class="browser-default custom-select" name="currency">
                                                    <option value="1" selected>грн</option>
                                                    <option value="2">euro</option>
                                                    <option value="3">dollar</option>
                                                </select>
                                            </div></div></div>
                                    <div id="notAnyForm4">
                                        <div class="row" id="ochno1">
                                            <div class="col-md-6">очно/стенд</div><div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="text" aria-label="First name" class="form-control" name="priceVO">
                                                    <select class="browser-default custom-select" name="currency">
                                                        <option value="1" selected>грн</option>
                                                        <option value="2">euro</option>
                                                        <option value="3">dollar</option>
                                                    </select>
                                                </div></div></div>

                                        <div class="row">
                                            <div class="col-md-6">заочно/онлайн</div><div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="text" aria-label="First name" class="form-control" name="priceVZ">
                                                    <select class="browser-default custom-select" name="currency">
                                                        <option value="1" selected>грн</option>
                                                        <option value="2">euro</option>
                                                        <option value="3">dollar</option>
                                                    </select>
                                                </div></div></div></div>
                                </div>
                            </div>
                            <div id="trUkr" class="row border">

                                <div class="col-md-4">Украина</div>
                                <div class="col-md-8">
                                    <div class="row" id="anyForm2">
                                        <div class="col-md-6">любая</div><div class="col-md-6">
                                            <div class="input-group">
                                                <input type="text" aria-label="First name" class="form-control" name="priceCA">
                                                <select class="browser-default custom-select" name="currency">
                                                    <option value="1" selected>грн</option>
                                                    <option value="2">euro</option>
                                                    <option value="3">dollar</option>
                                                </select>
                                            </div></div></div>
                                    <div id="notAnyForm2">
                                        <div class="row" id="ochno1">
                                            <div class="col-md-6">очно/стенд</div><div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="text" aria-label="First name" class="form-control" name="priceCO">
                                                    <select class="browser-default custom-select" name="currency">
                                                        <option value="1" selected>грн</option>
                                                        <option value="2">euro</option>
                                                        <option value="3">dollar</option>
                                                    </select>
                                                </div></div></div>

                                        <div class="row">
                                            <div class="col-md-6">заочно/онлайн</div><div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="text" aria-label="First name" class="form-control" name="priceCZ">
                                                    <select class="browser-default custom-select" name="currency">
                                                        <option value="1" selected>грн</option>
                                                        <option value="2">euro</option>
                                                        <option value="3">dollar</option>
                                                    </select>
                                                </div></div></div></div>
                                </div>
                            </div>
                            <div id="trNotUkr" class="row border">

                                <div class="col-md-4">Не Украина</div>
                                <div class="col-md-8">
                                    <div class="row" id="anyForm3">
                                        <div class="col-md-6">любая</div><div class="col-md-6">
                                            <div class="input-group">
                                                <input type="text" aria-label="First name" class="form-control" name="priceNA">
                                                <select class="browser-default custom-select" name="currency">
                                                    <option value="1" selected>грн</option>
                                                    <option value="2">euro</option>
                                                    <option value="3">dollar</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="notAnyForm3">
                                        <div class="row" id="ochno1">
                                            <div class="col-md-6">очно/стенд</div><div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="text" aria-label="First name" class="form-control" name="priceNO">
                                                    <select class="browser-default custom-select" name="currency">
                                                        <option value="1" selected>грн</option>
                                                        <option value="2">euro</option>
                                                        <option value="3">dollar</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">заочно/онлайн</div><div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="text" aria-label="First name" class="form-control" name="priceNZ">
                                                    <select class="browser-default custom-select" name="currency">
                                                        <option value="1" selected>грн</option>
                                                        <option value="2">euro</option>
                                                        <option value="3">dollar</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1"></div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-12">
                                    <label for="exampleForm2"><b>Стоимость отдельного сборника</b></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text" style="height: 38px;">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox1">
                                                <label class="form-check-label" for="defaultCheckbox1">
                                            </div>
                                        </div>
                                        <input type="text" aria-label="First name" class="form-control w-50" name="price_book" readonly="readonly">
                                        <select class="browser-default custom-select" name="currency_book_en">
                                            <option value="1" selected>грн</option>
                                            <option value="2">euro</option>
                                            <option value="3">dollar</option>
                                        </select>
                                    </div><br>
                                </div>

                                <div class="col-12">
                                    <label for="exampleForm2"><b>Сертификат</b></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text" style="height: 38px;">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox2">
                                                <label class="form-check-label" for="defaultCheckbox2">
                                            </div>
                                        </div>
                                        <input type="text" aria-label="First name" class="form-control w-50" name="price_certificate" readonly="readonly">
                                        <select class="browser-default custom-select" name="currency_certificate">
                                            <option value="1" selected>грн</option>
                                            <option value="2">euro</option>
                                            <option value="3">dollar</option>
                                        </select>
                                    </div><br>
                                </div>

                                <div class="col-12">
                                    <label for="exampleForm2"><b>Перевод работы</b></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text" style="height: 38px;">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox3">
                                                <label class="form-check-label" for="defaultCheckbox3">
                                            </div>
                                        </div>
                                        <input type="text" aria-label="First name" class="form-control w-50" name="price_translate" readonly="readonly">
                                        <select class="browser-default custom-select" name="currency_translate">
                                            <option value="1" selected>грн</option>
                                            <option value="2">euro</option>
                                            <option value="3">dollar</option>
                                        </select>
                                    </div><br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label for="exampleForm2"><b>Публикация в английском сборнике</b></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text" style="height: 38px;">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox4">
                                                <label class="form-check-label" for="defaultCheckbox4">
                                            </div>
                                        </div>
                                        <input type="text" aria-label="First name" class="form-control w-50" name="price_en" readonly="readonly">
                                        <select class="browser-default custom-select" name="currency_en">
                                            <option value="1" selected>грн</option>
                                            <option value="2">euro</option>
                                            <option value="3">dollar</option>
                                        </select>
                                    </div><br>
                                </div>

                                <div class="col-md-12">
                                    <label for="exampleForm2"><b>Стоимость отдельного сборника на английском</b></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text" style="height: 38px;">
                                                <input class="form-check-input" type="checkbox" id="defaultCheckbox5">
                                                <label class="form-check-label" for="defaultCheckbox5">
                                            </div>
                                        </div>
                                        <input type="text" aria-label="First name" class="form-control w-50" name="price_book_en" readonly="readonly">
                                        <select class="browser-default custom-select" name="currency_book_en">
                                            <option value="1" selected>грн</option>
                                            <option value="2">euro</option>
                                            <option value="3">dollar</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="clearfix">

                        <button class="btn btn-primary waves-effect float-right">СОХРАНИТЬ ЦЕНУ</button>
                    </div>
                </div>

            </div>
        </div>







        {{Form::close()}}



    </div>

    <script src="/js/rscripts.js"></script>
@endsection
