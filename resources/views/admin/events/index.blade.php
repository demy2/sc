@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary deep-purple-text">
        <div class="card-header"><b>Листинг активных событий</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">

                <section class="content">

                    <!--div class="row inline text-center">
                        <div class="col-12 align-middle">
                            <a class="btn-floating success-color" type="button" role="button"><i class="fas fa-check"><b class="h2">1</b></i></a>
                            <i class="fas fa-angle-double-right"></i>
                            <a class="btn-floating danger-color" type="button" role="button"><i class="fab fa-slack-hash"><b class="h2">2</b></i></a>
                            <i class="fas fa-angle-double-right"></i>
                            <a class="btn-floating danger-color" type="button" role="button"><i class="fab fa-slack-hash"><b class="h2">3</b></i></a>
                            <i class="fas fa-angle-double-right"></i>
                            <a class="btn-floating danger-color" type="button" role="button"><i class="fab fa-slack-hash"><b class="h2">4</b></i></a>
                            <i class="fas fa-angle-double-right"></i>
                            <a class="btn-floating danger-color" type="button" role="button"><i class="fab fa-slack-hash"><b class="h2">5</b></i></a>
                        </div>
                    </div-->

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active text-uppercase" id="activeEvents-tab" data-toggle="tab" href="#activeEvents" role="tab" aria-controls="activeEvents"
                               aria-selected="true">Активні події</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" id="archiveEvents-tab" data-toggle="tab" href="#archiveEvents" role="tab" aria-controls="archiveEvents"
                               aria-selected="false">Архів подій</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="activeEvents" role="tabpanel" aria-labelledby="activeEvents-tab">
                            @include('admin.events._activeEvents')
                        </div>
                        <div class="tab-pane fade" id="archiveEvents" role="tabpanel" aria-labelledby="archiveEvents-tab">
                            @include('admin.events._archiveEvents')
                        </div>
                    </div>

                </section>


            </div>
        </div>
    </div>
@endsection
