<div class="modal fade" id="sectionInfo{{$event->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Статестичні дані</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" style="color: #4c00be">
                    <thead>
                    <tr class="text-center">
                        <th><b>№</b></th>
                        <th><b>Назва секції</b></th>
                        <th><b>Відповідальний</b></th>
                        <th><b>Учасники</b></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td class="align-middle text-left"><a href="/admin/events/{{$event->id}}" class="text-primary">Всі учасники</a></td>
                        <td></td>
                        <td><a href="/admin/events/{{$event->id}}" class="text-primary"><b>{{ $event->countUserEvent($event) }}</b></a></td>
                    </tr>
                    @foreach($event->sections($event->id) as $section)
                        <tr>
                            <td>{{ $section->local_id }}</td>
                            <td class="align-middle text-left"><a href="{{ route('admin.section', ['events' => $event->id, 'section' => $section->local_id]) }}" class="text-primary" title="{{ $section->$title }}">{{ str_limit($section->$title, 40) }}</a></td>
                            <td class="align-middle text-left">{{ $section->admin['surname'] }} {{ $section->admin['name'] }} {{ $section->admin['middle_name'] }}</td>
                            <td><a href="{{ route('admin.section', ['events' => $event->id, 'section' => $section->local_id]) }}" class="text-primary"><b>{{ $section->userCount($section->id) }}</b></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
