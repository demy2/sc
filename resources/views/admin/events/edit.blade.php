@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3">
        <div class="card-header"><b>Редактирование события "{{ $events->title_ua }}"</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">
                <section class="content">

                    <section class="content">
                        @include('_errors')

                    {{Form::open([
                        'route'	=> ['admin.events.update', $events->id],
                        'files'	=>	true,
                        'method'	=>	'put'
                    ])}}

                        <div class="accordion md-accordion accordion-blocks" id="accordionEx" role="tablist" aria-multiselectable="true">
                            <!-- Общая информация -->
                            <div class="card">
                                <!-- Card header -->
                                <div class="card-header grey lighten-4" role="tab" id="heading1">
                                    <a data-toggle="collapse" data-parent="#accordionEx" href="#collapse1" aria-expanded="true"
                                       aria-controls="collapse1">
                                        <h5 class="mb-0">
                                            <i class="fas fa-pencil-alt"></i> {{ __('lang.generalInfo') }} <i class="fas fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>


                                <!-- Card body -->
                                <div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="heading1" data-parent="#accordionEx">
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="{{ $events->getImage() }}" alt="" width="500px"><br>
                                                <label>Фоновое изображение</label><i class="fas fa-upload ml-3" aria-hidden="true"></i>
                                                <input type="file" id="exampleInputFile" name="image">
                                                <p class="help-block">Какое-нибудь уведомление о форматах..</p>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Дата начала и конца регистрации участников:</label>
                                                <div class="input-group">
                                                    {!! Form::date('date_start', $events->date_start, ['class' => 'form-control', 'id'    =>  'datepicker1']) !!}
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="far fa-calendar-alt" style="color: #4c00be"></i></span>
                                                    </div>

                                                    {!! Form::date('date_stop', $events->date_stop, ['class' => 'form-control', 'id'    =>  'datepicker2']) !!}
                                                </div>
                                                <br>
                                                <label>Дата начала и конца проведения события:</label>
                                                <div class="input-group">
                                                    {!! Form::date('date_open', $events->date_open, ['class' => 'form-control', 'id'    =>  'datepicker3']) !!}
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="far fa-calendar-alt" style="color: #4c00be"></i></span>
                                                    </div>

                                                    {!! Form::date('date_close', $events->date_close, ['class' => 'form-control', 'id'    =>  'datepicker4']) !!}
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Перечень категорий {{ $events->getCategory() }}</label>
                                                <div class="form-group">
                                                    {{Form::select('categories[]',
                                                        $categories,
                                                        123,
                                                        ['class' => 'form-control select2 browser-default', 'multiple'  =>  'multiple'])
                                                    }}
                                                </div>
                                            </div>


                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <img src="/img/flag-ua.png" alt="" width="30 px"><br>
                                                    <label for="exampleInputEmail1">Назва події</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="title_ua" value="@if(old('title_ua') != null){{ old('title_ua') }}@else{{ $events->title_ua }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Коротка назва (абревіатура)</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="short_title_ua" value="@if(old('short_title_ua') != null){{ old('short_title_ua') }}@else{{ $events->short_title_ua }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Опис події</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Наприклад: ІІІ Міжнародна конференція студентів та молодих..." name="description_ua" value="@if(old('description_ua') != null){{ old('description_ua') }}@else{{ $events->description_ua }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Мета проведення</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Наприклад: Розвиток наукового потенціалу студентів, аспірантів та ..." name="target_ua" value="@if(old('target_ua') != null){{ old('target_ua') }}@else{{ $events->target_ua }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Інша важлиа інформація</label>
                                                    <input type="text" name="other_ua" class="form-control" id="exampleInputEmail1" placeholder="Наприклад: Присвячується..." value="@if(old('other_ua') != null){{ old('other_ua') }}@else{{ $events->other_ua }}@endif">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <img src="/img/flag-ru.png" alt="" width="30 px"><br>
                                                    <label for="exampleInputEmail1">Название события</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="title_ru" value="@if(old('title_ru') != null){{ old('title_ru') }}@else{{ $events->title_ru }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Короткое название (абревиатура)</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="short_title_ru" value="@if(old('short_title_ru') != null){{ old('short_title_ru') }}@else{{ $events->short_title_ru }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Описание события</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Например: IIIМеждународная конференция студентов и молодых..." name="description_ru" value="@if(old('description_ru') != null){{ old('description_ru') }}@else{{ $events->description_ru }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Цель проведения</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Например: Развитие научного потенциала студентов, аспирантов и ..." name="target_ru" value="@if(old('target_ru') != null){{ old('target_ru') }}@else{{ $events->target_ru }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Прочая важная информация</label>
                                                    <input type="text" name="other_ru" class="form-control" id="exampleInputEmail1" placeholder="Наприклад: Посвящается..." value="@if(old('other_ru') != null){{ old('other_ru') }}@else{{ $events->other_ru }}@endif">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <img src="/img/flag-en.png" alt="" width="30 px"><br>
                                                    <label for="exampleInputEmail1">Title event</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="title_en" value="@if(old('title_en') != null){{ old('title_en') }}@else{{ $events->title_en }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Short title event</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="short_title_en" value="@if(old('short_title_en') != null){{ old('short_title_en') }}@else{{ $events->short_title_en }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Описание события</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="description_en" value="@if(old('description_en') != null){{ old('description_en') }}@else{{ $events->description_en }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Цель проведения</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="target_en" value="@if(old('target_en') != null){{ old('target_en') }}@else{{ $events->target_en }}@endif">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Інша важлиа інформація</label>
                                                    <input ype="text" name="other_en" class="form-control" id="exampleInputEmail1" placeholder="Наприклад: Посвящается..." value="@if(old('other_en') != null){{ old('other_en') }}@else{{ $events->other_en }}@endif">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ./Общая информация -->

                            <!-- Секции -->
                            <div class="card">
                                <!-- Card header -->
                                <div class="card-header grey lighten-4" role="tab" id="heading3">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapse3"
                                       aria-expanded="false" aria-controls="collapse3">
                                        <h5 class="mb-0">
                                            <i class="fas fa-pencil-alt"></i> {{ __('lang.section') }} <i class="fas fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>

                                <!-- Card body -->
                                <div id="collapse3" class="collapse" role="tabpanel" aria-labelledby="heading3" data-parent="#accordionEx">
                                    <div class="card-body">
                                        <table class="table table-hover" id="dynamic" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="th-sm"></th>
                                                <th class="th-sm"><img src="/img/flag-ua.png" alt="" width="30 px"> Назва секції</th>
                                                <th class="th-sm"><img src="/img/flag-ru.png" alt="" width="30 px"> Название секции</th>
                                                <th class="th-sm"><img src="/img/flag-en.png" alt="" width="30 px"> Назва секції</th>
                                                <th class="th-sm"><a class="btn btn-success" name="add" id="add"><i class="fas fa-plus"></i></a></th>
                                            </tr>
                                            </thead>
                                            <tbody><?php $i =0 ?>
                                            @foreach($sections as $section)
                                                <tr>
                                                    <td class="text-right"><b>{{ ++$i }} </b></td>
                                                    <td><input type="text" class="form-control" id="exampleInputEmail1" placeholder="Назва секції" name="section_ua[]" value="{{ $section->section_ua }}"></td>
                                                    <td><input type="text" class="form-control" id="exampleInputEmail1" placeholder="Название секции" name="section_ru[]" value="{{ $section->section_ru }}"></td>
                                                    <td><input type="text" class="form-control" id="exampleInputEmail1" placeholder="placeholder" name="section_en[]" value="{{ $section->section_en }}"></td>
                                                    <td></td>
                                                </tr>
                                            @endforeach

                                            <!--tr>
                                                <td class="text-right"><b>1. </b></td>
                                                <td><input type="text" class="form-control" id="exampleInputEmail1" placeholder="Назва секції" name="section_ua[]" ></td>
                                                <td><input type="text" class="form-control" id="exampleInputEmail1" placeholder="Название секции" name="section_ru[]" ></td>
                                                <td><input type="text" class="form-control" id="exampleInputEmail1" placeholder="placeholder" name="section_en[]" ></td>
                                                <td></td>
                                            </tr-->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <!-- ./Секции -->

                            <!-- Правила оформления -->
                            <div class="card">

                                <!-- Card header -->
                                <div class="card-header grey lighten-4" role="tab" id="heading2">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapse2"
                                       aria-expanded="false" aria-controls="collapse">
                                        <h5 class="mb-0">
                                            <i class="fas fa-pencil-alt"></i> {{ __('lang.designRules') }} <i class="fas fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>

                                <!-- Card body -->
                                <div id="collapse2" class="collapse" role="tabpanel" aria-labelledby="heading2" data-parent="#accordionEx">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Правила оформлення наукових робіт</label>
                                                    <textarea name="design_rules_ua" id="" cols="30" rows="10" class="form-control" >@if(old('design_rules_ua') != null){{ old('design_rules_ua') }}@else{{ $events->design_rules_ua }}@endif</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Правила оформления научных работ</label>
                                                    <textarea name="design_rules_ru" id="" cols="30" rows="10" class="form-control" >@if(old('design_rules_ru') != null){{ old('design_rules_ru') }}@else{{ $events->design_rules_ru }}@endif</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Описание</label>
                                                    <textarea name="design_rules_en" id="" cols="30" rows="10" class="form-control" >@if(old('design_rules_en') != null){{ old('design_rules_en') }}@else{{ $events->design_rules_en }}@endif</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- ./Правила оформления -->

                            <!-- Стоимость и оплата -->
                            <div class="card">

                                <!-- Card header -->
                                <div class="card-header grey lighten-4" role="tab" id="heading4">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapse4"
                                       aria-expanded="false" aria-controls="collapse4">
                                        <h5 class="mb-0">
                                            <i class="fas fa-pencil-alt"></i> {{ __('lang.payment') }} <i class="fas fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>

                                <!-- Card body -->
                                <div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="heading4" data-parent="#accordionEx">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Вартість та оплата</label>
                                                    <textarea name="pay_ua" id="" cols="30" rows="10" class="form-control" >@if(old('pay_ua') != null){{ old('pay_ua') }}@else{{ $events->pay_ua }}@endif</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Стоимость и оплата</label>
                                                    <textarea name="pay_ru" id="" cols="30" rows="10" class="form-control" >@if(old('pay_ru') != null){{ old('pay_ru') }}@else{{ $events->pay_ru }}@endif</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Стоимость и оплата</label>
                                                    <textarea name="pay_en" id="" cols="30" rows="10" class="form-control" >@if(old('pay_en') != null){{ old('pay_en') }}@else{{ $events->pay_en }}@endif</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- ./Стоимость и оплата -->

                            <!-- Оргкоммитет -->
                            <div class="card">

                                <!-- Card header -->
                                <div class="card-header grey lighten-4" role="tab" id="heading5">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapse5"
                                       aria-expanded="false" aria-controls="collapse5">
                                        <h5 class="mb-0">
                                            <i class="fas fa-pencil-alt"></i> {{ __('lang.orgcom') }} <i class="fas fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>

                                <!-- Card body -->
                                <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="heading5" data-parent="#accordionEx">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Організаційний комітет</label>
                                                    <textarea name="orgcom_ua" id="" cols="30" rows="10" class="form-control" >@if(old('orgcom_ua') != null){{ old('orgcom_ua') }}@else{{ $events->orgcom_ua }}@endif</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Организационный комитет</label>
                                                    <textarea name="orgcom_ru" id="" cols="30" rows="10" class="form-control" >@if(old('orgcom_ru') != null){{ old('orgcom_ru') }}@else{{ $events->orgcom_ru }}@endif</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Описание</label>
                                                    <textarea name="orgcom_en" id="" cols="30" rows="10" class="form-control" >@if(old('orgcom_en') != null){{ old('orgcom_en') }}@else{{ $events->orgcom_en }}@endif</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- ./Оргкоммитет -->

                            <!-- Документация -->
                            <div class="card">

                                <!-- Card header -->
                                <div class="card-header grey lighten-4" role="tab" id="heading6">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapse6"
                                       aria-expanded="false" aria-controls="collapse6">
                                        <h5 class="mb-0">
                                            <i class="fas fa-pencil-alt"></i> {{ __('lang.docs') }} <i class="fas fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>

                                <!-- Card body -->
                                <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="heading6" data-parent="#accordionEx">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Правила оформлення наукових робіт</label>
                                                    <textarea name="docs_ua" id="" cols="30" rows="10" class="form-control" >@if(old('docs_ua') != null){{ old('docs_ua') }}@else{{ $events->docs_ua }}@endif</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Правила оформления научных работ</label>
                                                    <textarea name="docs_ru" id="" cols="30" rows="10" class="form-control" >@if(old('docs_ru') != null){{ old('docs_ru') }}@else{{ $events->docs_ru }}@endif</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Описание</label>
                                                    <textarea name="docs_en" id="" cols="30" rows="10" class="form-control" >@if(old('docs_en') != null){{ old('docs_en') }}@else{{ $events->docs_en }}@endif</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- ./Документация -->

                            <div class="box-footer">
                                <button class="btn btn-default">Назад</button>
                                <button class="btn btn-success pull-right">Изменить</button>
                            </div>
                        </div>
                        <!-- /.box -->
                        {{Form::close()}}
                    </section>

                </section>
            </div>
        </div>
    </div>
@endsection
