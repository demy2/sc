<div class="modal fade" id="workStatus{{ $i }}" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Состояние работы</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if($participant->file != null)
                {{Form::open([
                     'route' => 'admin.statusset',
                ])}}
                <div class="modal-body text-center">
                    <div><b>{{ $participant->name }}</b></div>
                    <div>{{ $participant->soavtor}}</div>
                    <br>
                    <div>
                        <a href="{{ route('admin.download', $participant->id) }}"><i class="far fa-file-alt fa-3x blue-text"></i></a>
                    </div>
                    <hr>

                    <div><b>Статус работы</b></div>


                    <input type="hidden" name="part" value="{{ $participant->id }}">
                    <select class="mdb-select md-form colorful-select dropdown-primary" name="status" id="status" data-part="{{ $participant->id }}">
                        <option value="1" @if($participant->status == 1) selected @endif>Работа не прислана</option>
                        <option value="2" @if($participant->status == 2) selected @endif>Работа прислана</option>
                        <option value="3" @if($participant->status == 3) selected @endif>Работа одобрена</option>
                        <option value="4" @if($participant->status == 4) selected @endif>Работа содержит замечания</option>
                        <option value="5" @if($participant->status == 5) selected @endif>Работа не одобрена</option>
                    </select>

                </div>

                <div class="modal-footer">
                    <input type="submit" class=" btn btn-primary" id="statsend" value="Изменить статус">
                </div>
                {{Form::close()}}

            @else
                <div class="modal-body text-center">
                    <div><b>{{ $participant->name }}</b></div>
                    <div>{{ $participant->soavtor }}</div>
                    <br>
                    <div style="color: lightgray">
                        <i class="far fa-file-alt fa-2x"></i><br>Файл не был отправлен<br>
                    </div>
                    <div>Вы можете отправить пользователю письмо с напоминанием об отправке работы</div>
                </div>
            @endif
        </div>
    </div>
</div>
