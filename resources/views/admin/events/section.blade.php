@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary deep-purple-text">
        <div class="card-header"><b>Список участников "{{ $eventTitle }}". Секция {{ $section->local_id }}: "{{ $section->$title }}"</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">
                <section class="content">
                    <div class="row">
                        <div class="col-6">
                            <dl class="row">
                                <dt class="col-sm-6">Число работ</dt>
                                <dd class="col-sm-6">{{ $N_work }}</dd>

                                <dt class="col-sm-6">Число участников</dt>
                                <dd class="col-sm-6">{{ $N_user }}</dd>

                                <dt class="col-sm-6 text-truncate">Число доп.сборников</dt>
                                <dd class="col-sm-6">{{ $N_book }}</dd>
                            </dl>
                        </div>

                        <div class="col-6">



                        </div>

                    </div>

                    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" style="color: #4c00be">
                        <thead>
                        <tr class="text-center">
                            <th>№</th>
                            <th>ФИО | Контакти</th>
                            <th>Страна | Город<br>ВУЗ</th>
                            <th>Участие</th>
                            <th>Перевод</th>
                            <th>Работа</th>
                            <th>Доп.<br>сборников</th>
                            <th>Сумма</th>
                            <th>Доставка</th>
                        </tr>
                        </thead>
                        <tbody><?php $i = 0; $s1 = 0; $s2 = 0; $s3 = 0; $s4 = 0; $s5 = 0; $form1 = 0; $form2 = 0; $form3 = 0; $form4 = 0; $NBook = 0; $sumUAH = 0; $sumEUR = 0; $sumDop = 0 ?>
                        @if(isset($participants))
                            @foreach($participants as $participant)

                                <tr class="text-center">
                                    <td class="align-middle">
                                        <div class="form-check-label blue-text">
                                            <input type="checkbox" class="form-check-input usersMail" id="mail{{ $i }}" value="{{ $participant->id }}">
                                            <label class="form-check-label" for="mail{{ $i }}">
                                                <?php $i++;  echo "$i"?>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="align-middle text-left">
                                        {{ $participant->user->info->surname }} {{ $participant->user->info->name }} {{ $participant->user->info->middle_name }}
                                        <br>
                                        {{ $participant->user->email }}
                                        <br>
                                        {{ $participant->user->info->phone }}
                                    </td>
                                    <td class="align-middle text-left">
                                        @if(isset($participant->user->info->country->$title)){{ $participant->user->info->country->$title }}, @endif
                                        @if(isset($participant->user->info->city->$title)){{ $participant->user->info->city->$title }}@endif<br>
                                        @if(isset($participant->user->info->university->$shortTitle)){{ $participant->user->info->university->$shortTitle }}@endif
                                    </td>
                                    <td class="align-middle">
                                        @if($participant->form_id == 1) <?php $form1++ ?>
                                        @elseif($participant->form_id == 2) <?php $form2++ ?>
                                        @elseif($participant->form_id == 3) <?php $form3++ ?>
                                        @elseif($participant->form_id == 4) <?php $form4++ ?>
                                        @endif
                                        {{ $participant->form->$title }}
                                    </td>
                                    <td class="align-middle">
                                        @if($participant->dopolnitelno == 'on'){{ $participant->dopolnitelno }} <?php $sumDop++ ?>@endif
                                    </td>
                                    <td class="align-middle text-left">
                                        @if($participant->status == 1) <?php $color = 'lightgray'; $check ; $s1++?>
                                        @elseif($participant->status == 2) <?php $color = 'blue'; $s2++ ?>
                                        @elseif($participant->status == 3) <?php $color = 'limegreen'; $s3++ ?>
                                        @elseif($participant->status == 4) <?php $color = 'orange'; $s4++ ?>
                                        @elseif($participant->status == 5) <?php $color = 'red'; $s5++ ?>
                                        @endif

                                        <a style="color: {{ $color }}" title="{{ $participant->stat->$status }}. Нажмите для просмотра деталей" data-toggle="modal" data-target="#workStatus{{ $i }}">
                                            <i class="far fa-arrow-alt-circle-down"></i> {{ str_limit($participant->name, 50) }}<br>
                                            {{ str_limit($participant->soavtor, 50) }}
                                        </a><title>{{ $participant->stat->$status }}</title>
                                        <!-- Modal workStatus-->
                                    @include('admin.events._modal_workStatus')
                                    <!-- /Modal workStatus-->

                                    </td>
                                    <td class="align-middle"><?php $NBook = $NBook + $participant->Nbook ?>{{ $participant->Nbook }}</td>
                                    <td class="align-middle">
                                        @if($participant->pay == 1) <?php $color = 'red'; $titlePay = 'Не оплачено' ?>
                                        @elseif($participant->pay == 2) <?php $color = 'limegreen'; $titlePay = 'Оплачено';
                                        if($participant->currency == 'UAH'){$sumUAH = $sumUAH + $participant->amount;}
                                        else($participant->currency == 'EUR'){$sumEUR = $sumEUR + $participant->amount} ?>
                                        @elseif($participant->pay == 3) <?php $color = 'lightgray'; $titlePay = 'Льгота' ?>
                                        @endif
                                        <a href="#" style="color: {{ $color }}" title="{{ $titlePay }}">{{ $participant->infoPay($participant)['sumTxt'] }}</a><title>{{ $titlePay }}</title>
                                    </td>
                                    <td class="align-middle">
                                        @isset($participant->user->info->oNP)
                                            <a type="button" data-toggle="modal" data-target="#postModal">
                                                <i class="fas fa-dice-d6"></i>
                                            </a>

                                            <div class="modal fade" id="postModal" tabindex="-1" role="dialog" aria-labelledby="postModal"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="postModal">Адреса доставки</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @isset($participant->user->info->surnameNP){{ $participant->user->info->surnameNP }}@endisset
                                                            @isset($participant->user->info->nameNP){{ $participant->user->info->nameNP }}@endisset
                                                            @isset($participant->user->info->middle_nameNP){{ $participant->user->info->middle_nameNP }}@endisset<br>
                                                            @isset($participant->user->info->phoneNP){{ $participant->user->info->phoneNP }}@endisset
                                                            @isset($participant->user->info->city_idNP), {{ $participant->user->info->city_idNP }}@endisset
                                                            @isset($participant->user->info->oNP), {{ $participant->user->info->oNP }}@endisset
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endisset
                                    </td>
                                </tr>

                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                            <tr class="text-center">
                                <th class="align-middle">
                                    <input type="checkbox" class="form-check-input" id="checkall">
                                    <label for="checkall">все<input type="checkbox" id="checkall" ></label>
                                </th>
                                <th class="align-middle">
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal" id="sm" style="display: none;">
                                        SEND <i class="far fa-paper-plane"></i>
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                         aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Отправка писем выбранным участникам</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <textarea style="height: 180px;" name="text_telo" id="text_telo" class="form-control"></textarea>
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="submit" class="btn btn-primary" name="sent-mail-submit" value="send mail" id="smwt">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </th>
                                <th></th>

                                <th class="align-middle">
                                    <a title="Очная"><i class="fas fa-chalkboard-teacher"></i> {{ $form1 }}</a><br>
                                    <a title="Онлайн"><i class="fas fa-desktop"></i> {{ $form2 }}</a><br>
                                    <a title="Стенд"><i class="far fa-list-alt"></i> {{ $form3 }}</a><br>
                                    <a title="Заочная"><i class="fas fa-home"></i> {{ $form4 }}</a>
                                </th>
                                <th class="align-middle">{{ $sumDop }}</th>
                                <th class="align-middle">
                                    <a title="Работа не прислана" style="color: lightgray">{{ $s1 }}</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                    <a title="Работа прислана" style="color: blue">{{ $s2 }}</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                    <a title="Работа одобрена" style="color: limegreen">{{ $s3 }}</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                    <a title="Работа содержит замечания" style="color: orange">{{ $s4 }}</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                    <a title="Работа не одобрена" style="color: red">{{ $s5 }}</a>
                                </th>
                                <th class="align-middle">{{ $NBook }}</th>
                                <th class="align-middle">{{ $sumUAH }} UAH <br> {{ $sumEUR }} EUR</th>
                                <th class="align-middle"></th>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="testing"></div>
                </section>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.mdb-select').materialSelect();
        });

        $('#checkall').change(function(){

            $('.usersMail').prop("checked", $(this).prop("checked"))
        });

        $('#checkall').change(function(){
            $('.usersMail').prop("checked", $(this).prop("checked"));
        });
        $('.form-check-input').change(function(){
            var result = $('.usersMail:checked');
            if (result.length > 0) {

                var resultString = result.length + "email(s) checked";

                $('.testing').html(resultString);
                $('#sm').show(500);
            }
            else{
                $('.testing').html('');
                $('#sm').hide(500);
            }
        });


        $('#smwt').click(function () {
            var result = $('.usersMail:checked');
            var email = [];
            var event = "{{ $eventTitle }}";

            var text = CKEDITOR.instances.text_telo.getData();

            result.each(function () {
                email.push($(this).val());
            });

            if(email.length > 0){
                $.ajax({
                    url:'{{ route('admin.napochtu') }}',
                    method:'post',
                    data:{email:email, _token: '{{csrf_token()}}', event:event, text:text},

                    success:function(data) {
                        $('.testing').html(data);
                    }
                });
            }
        });
    </script>
@endsection
