@extends('index')

@section('content')
    <br>
    <style>

        ul.stepper.horizontal .step-actions, ul.stepper.horizontal .step-new-content {
            padding-left: 0rem;
            padding-right: 0rem;
        }
        .card {
            min-height: 500px;
        }
        ul.stepper .step-title {
            padding-right: 90px;
            width: 350px;
        }

        body {
            line-height: 1.1rem;
        }
        ul.stepper li a {
            padding: 0.5rem;
            text-align: center;
        }

        label {
            margin-bottom: 0.1rem;
        }



        /* Загрузка логотипа */
        .logoContainer{
            width:300px;
            height:225px;
            margin: 0px auto 0 auto;
            /*background: url(http://img1.wikia.nocookie.net/__cb20130901213905/battlebears/images/9/98/Team-icon-placeholder.png) no-repeat 0 0;*/
            padding: 0px 0px 0px 0px;
            text-align: center;
            line-height: 20px;
        }
        .logoContainer img{
            max-width:100%;
        }
        .fileContainer{
            background:#ccc;/* you can give it background img as well*/
            width: 202px;
            height: 31px;
            overflow:hidden;
            position:relative;
            font-size:16px;
            line-height: 31px;
            color:#434343;
            padding: 0px 30px 0 30px;
            margin: 0 auto 60px auto;
            cursor: pointer !important;
        }
        .fileContainer span{
            overflow:hidden;
            display:block;
            white-space:nowrap;
            text-overflow:ellipsis;
            cursor: pointer;
        }
        .fileContainer input[type="file"]{
            opacity:0;
            margin: 0;
            padding: 0;
            width:100%;
            height:100%;
            left: 0;
            top: 0;
            position: absolute;
            cursor: pointer;
        }

    </style>
    @include('_errors')

    <div class="container-fluid">
    {{Form::open([
              'route' => 'admin.events.store',
              'files' =>  true
          ])}}


    <!--input type="hidden" name="admin_id" value="??????"-->

        <ul class="stepper parallel horizontal horizontal-fix" id="custom-validation">
            <li class="step active">
                <div data-step-label="{{ __('lang.generalInfo') }}" class="step-title waves-effect">ШАГ 1</div>
                <div class="step-new-content">
                    @include('admin.events._step1')
                </div>
            </li>



            <li class="step">
                <div data-step-label="{{ __('lang.section') }}" class="step-title waves-effect waves-dark">ШАГ 2</div>
                <div class="step-new-content">
                    @include('admin.events._step2')
                </div>
            </li>

            <li class="step">
                <div data-step-label="Оформлення та оргкомітет" class="step-title waves-effect waves-dark">ШАГ 3</div>
                <div class="step-new-content">
                    @include('admin.events._step3')
                </div>
            </li>

            <li class="step">
                <div data-step-label="{{ __('lang.docs') }}" class="step-title waves-effect waves-dark">ШАГ 4</div>
                <div class="step-new-content">
                    @include('admin.events._step4')
                </div>
            </li>

        </ul>

        {{Form::close()}}
    </div>








    <!-- Stepper JavaScript -->
    <script type="text/javascript" src="js/addons-pro/stepper.js"></script>
    <!-- Stepper JavaScript - minified -->
    <script type="text/javascript" src="js/addons-pro/stepper.min.js"></script>

    <script type="text/javascript">
        function validationFunction() {
            setTimeout(function () {
                $('#custom-validation').nextStep();
            }, 1600);
        }
        function someTrueFunction() {
            return true;
        }

        $(document).ready(function () {
            $('.stepper').mdbStepper();
        })



        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });

        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });


        /// Add section
        $(document).ready(function () {
            var i=1;
            $('#add').click(function () {
                i++;
                /// add section
                $('#dynamic').append(''+
                    '<tr id="row'+i+'" class="dynamic-add">'+
                    '<td class="text-right"><b>'+i+'. </b></td>'+
                    '<td><input type="text" class="validate form-control" id="exampleInputEmail1" placeholder="placeholder" name="section[]" value="" required></td>'+
                    '<td><input type="text" class="validate form-control" id="exampleInputEmail1" placeholder="placeholder" name="section[]" value="" required></td>'+
                    '<td><input type="text" class="validate form-control" id="exampleInputEmail1" placeholder="placeholder" name="section[]" value="" required></td>'+
                    '<td><a class="btn btn-sm btn-danger btn_remove" name="del" id="'+i+'"><i class="fas fa-minus"></i></a></td>'+
                    '</tr>');
            });
            /// remove section
            $(document).on('click', '.btn_remove', function () {
                var botton_id = $(this).attr("id");
                $('#row'+botton_id+'').remove();
            });
        });




        $("input:file").change(function (){
            var fileName = $(this).val();
            if(fileName.length >0){
                $(this).parent().children('span').html(fileName);
            }
            else{
                $(this).parent().children('span').html("Choose file");

            }
        });
        //file input preview
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.logoContainer img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("input:file").change(function(){
            readURL(this);
        });


    </script>

@endsection

