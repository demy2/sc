<div class="card border-primary deep-purple-text">
    <div class="card-header h5"><b>{{ __('lang.generalInfo') }}</b></div>


    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <label><b>{{ __('lang.Формат') }}</b></label>
                <div class="logoContainer">
                    <img src="/img/no-logo.png">
                </div>
                <div class="fileContainer sprite">
                    <span>выберите файл</span>
                    <input type="file" class="validate form-control" value="Choose File" name="image" accept=".png, .jpg, .jpeg">
                </div>
            </div>

            <div class="col-md-3">
                <label><b>Дата начала и конца регистрации участников:</b></label>
                <div class="input-group">
                    {!! Form::date('date_start', \Carbon\Carbon::now(), ['class' => 'validate form-control', 'id'    =>  'datepicker1', 'required']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt" style="color: #4c00be"></i></span>
                    </div>

                    {!! Form::date('date_stop', \Carbon\Carbon::now(), ['class' => 'validate form-control', 'id'    =>  'datepicker2', 'required']) !!}
                </div>
                <br>
                <label><b>Дата начала и конца проведения события:</b></label>
                <div class="input-group">
                    {!! Form::date('date_open', \Carbon\Carbon::now(), ['class' => 'validate form-control', 'id'    =>  'datepicker3', 'required']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt" style="color: #4c00be"></i></span>
                    </div>

                    {!! Form::date('date_close', \Carbon\Carbon::now(), ['class' => 'validate form-control', 'id'    =>  'datepicker4', 'required']) !!}
                </div>
                <br><br><br>
                <label><b>Наукові категории</b></label><br>

                <div class="input-group">
                    <!--div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-book-reader" style="color: #4c00be"></i></span>
                    </div-->
                    {{Form::select('categories[]',
                        $categories,
                        null,
                        ['class' => 'js-example-basic-multiple validate form-control browser-default', 'multiple'  =>  'multiple', 'data-placeholder'  =>  'Выберите перечень категорий'])
                    }}
                </div>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">

                    </div>

                    <div class="col-md-4">
                        <label><b>Участники</b></label><br>
                        @foreach($positions as $position)
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="position{!! $position->id !!}" name="position{!! $position->id !!}" checked required>
                                <label class="form-check-label" for="position{!! $position->id !!}">{{$position->title_ua}}</label>
                            </div>
                        @endforeach
                    </div>


                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <img src="/img/flag-ua.png" alt="" width="30 px">
                    <label for="title_ua">Назва події</label>
                    <input type="text" class="validate form-control" id="title_ua" placeholder="" name="title_ua" value="{{ old('title_ua') }}" required>
                </div>
                <div class="form-group">
                    <label for="short_title_ua">Коротка назва (абревіатура)</label>
                    <input type="text" class="validate form-control" id="short_title_ua" placeholder="" name="short_title_ua" value="{{ old('short_title_ua') }}" required>
                </div>
                <div class="form-group">
                    <label for="description_ua">Опис події</label>
                    <input type="text" class="validate form-control" id="description_ua" placeholder="Наприклад: ІІІ Міжнародна конференція студентів та молодих..." name="description_ua" value="{{ old('description_ua') }}" required>
                </div>
                <div class="form-group">
                    <label for="target_ua">Мета проведення</label>
                    <input type="text" class="validate form-control" id="target_ua" placeholder="Наприклад: Розвиток наукового потенціалу студентів, аспірантів та ..." name="target_ua" value="{{ old('target_ua') }}" required>
                </div>
                <div class="form-group">
                    <label for="other_ua">Інша важлиа інформація</label>
                    <input name="other_ua" class="validate form-control" id="other_ua" placeholder="Наприклад: Присвячується..." value="{{ old('other_ua') }}" required>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <img src="/img/flag-ru.png" alt="" width="30 px">
                    <label for="title_ru">Название события</label>
                    <input type="text" class="validate form-control" id="title_ru" placeholder="" name="title_ru" value="{{ old('title_ru') }}" required>
                </div>
                <div class="form-group">
                    <label for="short_title_ru">Короткое название (абревиатура)</label>
                    <input type="text" class="validate form-control" id="short_title_ru" placeholder="" name="short_title_ru" value="{{ old('short_title_ru') }}" required>
                </div>
                <div class="form-group">
                    <label for="description_ru">Описание события</label>
                    <input type="text" class="validate form-control" id="description_ru" placeholder="Например: IIIМеждународная конференция студентов и молодых..." name="description_ru" value="{{ old('description_ru') }}" required>
                </div>
                <div class="form-group">
                    <label for="target_ru">Цель проведения</label>
                    <input type="text" class="validate form-control" id="target_ru" placeholder="Например: Развитие научного потенциала студентов, аспирантов и ..." name="target_ru" value="{{ old('target_ru') }}" required>
                </div>
                <div class="form-group">
                    <label for="other_ru">Прочая важная информация</label>
                    <input name="other_ru" class="validate form-control" id="other_ru" placeholder="Наприклад: Посвящается..." value="{{ old('other_ru') }}" required>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <img src="/img/flag-en.png" alt="" width="30 px">
                    <label for="title_en">Title event</label>
                    <input type="text" class="validate form-control" id="title_en" placeholder="" name="title_en" value="{{ old('title_en') }}" required>
                </div>
                <div class="form-group">
                    <label for="short_title_en">Short title event</label>
                    <input type="text" class="validate form-control" id="short_title_en" placeholder="" name="short_title_en" value="{{ old('short_title_en') }}" required>
                </div>
                <div class="form-group">
                    <label for="description_en">Описание события</label>
                    <input type="text" class="validate form-control" id="description_en" placeholder="" name="description_en" value="{{ old('description_en') }}" required>
                </div>
                <div class="form-group">
                    <label for="target_en">Цель проведения</label>
                    <input type="text" class="validate form-control" id="target_en" placeholder="" name="target_en" value="{{ old('target_en') }}" required>
                </div>
                <div class="form-group">
                    <label for="other_en">Інша важлиа інформація</label>
                    <input name="other_en" class="validate form-control" id="other_en" placeholder="Наприклад: Посвящается..." value="{{ old('other_en') }}" required>
                </div>
            </div>
        </div>

    </div>
    <div class="card-footer">
        <div class="clearfix">
            <button class="btn btn-primary waves-effect next-step float-right" data-feedback="validationFunction">ДАЛЕЕ</button>
        </div>
    </div>

</div>
