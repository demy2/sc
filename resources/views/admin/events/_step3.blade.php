<div class="card border-primary deep-purple-text">
    <div class="card-header h5"><b>{{ __('lang.designRules') }} та {{ __('lang.orgcom') }}</b></div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <label><img src="/img/flag-ua.png" alt="" width="30 px"> Правила оформлення наукових робіт</label>
                <textarea name="design_rules_ua" id="" cols="30" rows="10" class="validate form-control" required>{{ old('design_rules_ua') }}</textarea>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label><img src="/img/flag-ru.png" alt="" width="30 px"> Правила оформления научных работ</label>
                    <textarea name="design_rules_ru" id="" cols="30" rows="10" class="validate form-control" required>{{ old('design_rules_ru') }}</textarea>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label><img src="/img/flag-en.png" alt="" width="30 px"> Описание</label>
                    <textarea name="design_rules_en" id="" cols="30" rows="10" class="validate form-control" required>{{ old('design_rules_en') }}</textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1"><img src="/img/flag-ua.png" alt="" width="30 px"> Організаційний комітет</label>
                    <textarea name="orgcom_ua" id="" cols="30" rows="10" class="validate form-control" required>{{ old('orgcom_ua') }}</textarea>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1"><img src="/img/flag-ru.png" alt="" width="30 px"> Организационный комитет</label>
                    <textarea name="orgcom_ru" id="" cols="30" rows="10" class="validate form-control" required>{{ old('orgcom_ru') }}</textarea>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1"><img src="/img/flag-en.png" alt="" width="30 px"> Описание</label>
                    <textarea name="orgcom_en" id="" cols="30" rows="10" class="validate form-control" required>{{ old('orgcom_en') }}</textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="card-footer">
        <div class="clearfix">
            <button class="btn btn-indigo waves-effect previous-step float-left">НАЗАД</button>
            <button class="btn btn-primary waves-effect next-step float-right">ДАЛЕЕ</button>
        </div>
    </div>
</div>
