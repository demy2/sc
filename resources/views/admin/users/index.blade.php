@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary deep-purple-text">
        <div class="card-header"><b>Список участников</b></div>
        <div class="card-body">
            <div class="container-fluid">
                <section class="content">


                    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Фото</th>
                            <th>ФИО</th>
                            <th>email</th>
                            <th>Телефон</th>
                            <th>Страна</th>
                            <th>Город</th>
                            <th>ВУЗ</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody><?php $i = 0 ?>
                        @if(isset($users))
                            @foreach($users as $user)
                                @if(isset($user->info))
                                <tr>
                                    <td><?php $i++;  echo "$i"?></td>
                                    <td><img src="{{ $user->info->getAvatar() }}" alt="" width="50px"></td>
                                    <td>{{ $user->info->surname }} {{ $user->info->name }} {{ $user->info->middle_name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->info->phone }}</td>
                                    <td>@if(isset($user->info->country->title_ua)){{ $user->info->country->title_ua }}@endif</td>
                                    <td>@if(isset($user->info->city->title_ua)){{ $user->info->city->title_ua }}@endif</td>
                                    <td>@if(isset($user->info->university->title_ua)){{ $user->info->university->title_ua }}@endif</td>
                                    <td></td>
                                </tr>
                                @endif
                            @endforeach
                        @endif
                        </tfoot>
                    </table>

                </section>
            </div>
        </div>
    </div>
@endsection
