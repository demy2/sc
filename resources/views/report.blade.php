@extends('index')

@section('content')
    <br><br>

    <div class="card border-primary deep-purple-text">
        <div class="card-header clearfix" style="max-height: 60px">
            <h1 class="text-uppercase float-left">{{ $event->$title }}</h1>
            <h5 class="float-right">
                {{ $event->dateOnOff($event) }}, {{ $univer->$short_title }}, {{ $city->$title }}, {{ $country->$title }}
            </h5>

        </div>
        <div class="card-body text-justify">

            <section class="content">
                <div class="container-fluid">
                    <div class="row justify-content-md-center">
                        <div class="col-md-4 text-center">
                            <img class="center" src="{{ $event->getImage() }}" alt="" style="width: 100%">
                        </div>
                    </div>
                </div>

                <br>
                <div class="container-fluid">
                    <style>
                        p {
                            text-indent: 20px;
                        }
                    </style>

                    <p>24-26 квітня 2019 року в Українському державному хіміко-технологічному університеті відбулася ІХ Міжнародна науково-технічна конференція студентів, аспірантів та молодих вчених «Хімія і сучасні технології». Співорганізаторами конференції виступили Дніпровський національний університет ім. О. Гончара, ЗО «Білоруський державний технологічний університет», Норвезький університет природничих наук, Гірничо-металургійний інститут Таджикистану, Киргизький національний університет ім. Ж. Баласагина, Таджицький технічний університет ім. академіка М. С. Осімі.</p>

                    <p>На пленарному засіданні перший проректор університету, професор Голеус В. І. привітав учасників конференції й побажав успіху, натхнення, творчих ідей і їх втілення, збагачення досвідом та подальшої співпраці. З доповідями виступили начальник відділу міжнародного співробітництва, доцент Коток В. А., к. т. н., асистент кафедри МІМ Анісімов В. В., Kuрiec K. R. та Malyshew V. V. (The Institute of Physical Chemistry of the Polish Academy of Sciences).</p>

                    <p>До оргкомітету конференції було надіслано понад 300 тез доповідей з різних навчальних закладів України, а також Польщі, Росії, Молдови, Франції. Відповідно до програми, робота конференції тривала у 13 секціях за тематичними напрямами. Кращі наукові доповіді було відзначено грамотами.</p>

                    <p>Також, не порушуючи традиції, з нагоди проведення конференції на внутрішньому подвір’ї університету було висаджено декоративну сливу. Сприятливі погодні умови дали змогу нашим гостям прогулятися містом пішою екскурсією. Вона виявилася дуже цікавою та пізнавальною.</p>

                    <p>Дякуємо усім, хто сприяв організації й успішному проведенню конференції! Нашим гостям-учасникам конференції кажемо: «До зустрічі у 2021 році!»</p>


                    {{-- $event->report --}}

                    <div class="row">
                        <div class="col-md-4">
                            <img src="/img/chem-2019/chem-2019-1.jpg" class="img-fluid">
                        </div>

                        <div class="col-md-4">
                            <img src="/img/chem-2019/chem-2019-2.jpg" class="img-fluid">
                        </div>

                        <div class="col-md-4">
                            <img src="/img/chem-2019/chem-2019-5.jpg" class="img-fluid">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="/img/chem-2019/chem-2019-6.jpg" class="img-fluid">
                        </div>

                        <div class="col-md-4">
                            <img src="/img/chem-2019/chem-2019-7.jpg" class="img-fluid">
                        </div>

                        <div class="col-md-4">
                            <img src="/img/chem-2019/chem-2019-9.jpg" class="img-fluid">
                        </div>
                    </div>
                    <br>
                    <hr>
                    <p class="text-center text-uppercase">збірники тез</p>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="{{ route('downloadArc','T1(1-3).pdf') }}"><img src="/img/chem-2019/InChemTeC 2019_1.jpg" width="200px" alt=""></a>
                            <a href="{{ route('downloadArc','T2(4-8).pdf') }}"><img src="/img/chem-2019/InChemTeC 2019_2.jpg" width="200px" alt=""></a>
                            <a href="{{ route('downloadArc','T3(9).pdf') }}"><img src="/img/chem-2019/InChemTeC 2019_3.jpg" width="200px" alt=""></a>
                            <a href="{{ route('downloadArc', 'T4(10-13).pdf') }}"><img src="/img/chem-2019/InChemTeC 2019_4.jpg" width="200px" alt=""></a>
                            <a href="{{ route('downloadArc', 'T5(En).pdf') }}"><img src="/img/chem-2019/InChemTeC 2019_En.jpg" width="200px" alt=""></a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

@endsection
