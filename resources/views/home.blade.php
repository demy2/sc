@extends('index')

@section('content')
    <br><br><br><br>
    <div class="card border-primary">
        <div class="card-header">Dashboard</div>
        <div class="card-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        You are logged in!
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection


