@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3">
        <div class="card-header clearfix">
            <h1 class="text-uppercase float-left">{{ __('lang.refs') }}</h1>
        </div>
        <div class="card-body text-primary">

            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col">
                            @if($refs->count('id') == 0)
                                <p>В данний час у Вас ще не має рефералів</p>
                            @else
                                <ol>
                                @foreach($refs as $ref)
                                    <li>{{ $ref->email }}</li>
                                @endforeach
                                </ol>
                            @endif
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
