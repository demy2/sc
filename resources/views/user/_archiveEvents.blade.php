@if($oldParticipants != null)

    <ol>
    @foreach($oldParticipants as $oldParticipant)
        <li>
            <a href="/event/{{ $oldParticipant->event->id }}" class="text-primary">
                {{ $oldParticipant->event->dateOnOff($oldParticipant->event) }}, {{ $oldParticipant->event->description_ua }} &laquo;<span class="text-uppercase">{{ $oldParticipant->event->title_ua }}</span>&raquo;<br>
                {{ $oldParticipant->name }}<br>
                <i>{{ $oldParticipant->soavtor }}</i>
            </a>
        </li>
    @endforeach
    </ol>
@else
    <div class="alert alert-danger" role="alert">
        {{ __('lang.archiveEventsError1') }}
    </div>
@endif
