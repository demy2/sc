@extends('index')

@section('content')
    <br><br>
    <div class="alert alert-primary alert-dismissible fade show deep-purple-text h4 text-center" role="alert">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-6">
                    <img src="/img/work.png" alt="" height="80px"><br>
                    {{ __('lang.addWork') }}<br>
                    <a class="btn btn-floating waves-effect white" style="width: 20px; height: 20px" title="Приклад кнопки">
                            <i class="far fa-file-alt fa-spin" style="color: grey; line-height: 20px;"></i>
                    </a>
                    <a class="btn btn-floating waves-effect white" style="width: 20px; height: 20px" title="Приклад кнопки">
                        <i class="fas fa-dollar-sign fa-spin" style="color: red; line-height: 20px;"></i>
                    </a>
                </div>
            </div>
        </div>

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ __('lang.Thank') }}</strong> {{ __('lang.datasave') }}.

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
    @if(Session::has('file'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>файл добавлен</strong>.

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
    <div class="card border-primary deep-purple-text">
        <div class="card-header text-uppercase"><b>{{ __('lang.titleUserEvents') }}</b></div>

        <div class="card-body text-primary">
            <section class="content">

                <!--Accordion wrapper-->
                <div class="accordion md-accordion accordion-blocks" id="accordionEx" role="tablist" aria-multiselectable="true">

                    <!-- Accordion card -->
                    <div class="card">

                        <!-- Card header -->
                        <div class="card-header grey lighten-4" role="tab" id="heading1">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                <h5 class="mb-0">
                                    {{ __('lang.activeEvents') }} <i class="fas fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapse1" role="tabpanel" aria-labelledby="heading1" data-parent="#accordionEx">
                            <div class="card-body">

                                <div class="row">
                                    @include('user._activeEvents')
                                </div>

                            </div>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card">

                        <!-- Card header -->
                        <div class="card-header grey lighten-4" role="tab" id="headingTwo2">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                               aria-expanded="false" aria-controls="collapseTwo2">
                                <h5 class="mb-0">
                                    {{ __('lang.actualeEvents') }} <span class="text-danger"><b>{{ $actualeEventsCount }}</b></span> <i class="fas fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionEx">
                            <div class="card-body">
                                @include('user._actualeEvents')
                            </div>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card">

                        <!-- Card header -->
                        <div class="card-header grey lighten-4" role="tab" id="headingThree3">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
                               aria-expanded="false" aria-controls="collapseThree3">
                                <h5 class="mb-0">
                                    {{ __('lang.archiveEvents') }} <i class="fas fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3" data-parent="#accordionEx">
                            <div class="card-body">
                                @include('user._archiveEvents')
                            </div>
                        </div>

                    </div>
                    <!-- Accordion card -->

                </div>
                <!-- Accordion wrapper -->

            </section>
        </div>
    </div>
@endsection


