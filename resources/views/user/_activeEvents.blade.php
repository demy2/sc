@if($participants != null)
    @foreach($participants->sortBy('section_id') as $participant)
        <div class="col-md-12">
            <div class="card border-{{ $participant->statPart($participant)['border'] }} mb-3">
                <div class="card-header grey lighten-4 text-uppercase text-primary h5-responsive">
                    <a href="\event\{{ $participant->event->id }}" title="Перехід на сторінку події">{{ $participant->event->$title }}</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-2 col-md-3 text-center">
                            <a type="button" class="btn btn-floating waves-effect" data-toggle="modal" data-target="#file{{ $participant->id }}" title="{{ $participant->statPart($participant)['t'] }}">
                                <i class="far fa-file-alt fa-2x {{ $participant->statPart($participant)['s'] }}" style="color: {{ $participant->statPart($participant)['color'] }}"></i>
                            </a>

                            <!-- Modal file -->
                            @include('user._modal_file')
                            <!-- //Modal file -->

                            <a type="button" class="btn btn-floating waves-effect z-depth-1a" data-toggle="modal" data-target="#pay{{ $participant->id }}" title="{{ $participant->statPart($participant)['p'] }}">
                                <i class="fas fa-dollar-sign fa-2x {{ $participant->statPart($participant)['sPay'] }}" style="color: {{ $participant->statPart($participant)['colorPay'] }}"></i>
                            </a>

                            <!-- Modal pay -->
                            @include('user._modal_pay')
                            <!-- //Modal pay -->
                        </div>

                        <div class="col-lg-10 col-md-9">
                            <dl class="row deep-purple-text text-justify h6-responsive">
                                <dt class="col-lg-2 col-md-2 col-sm-3 text-truncate">{{ __('lang.section') }} {{-- $participant->section_id --}}</dt>
                                <dd class="col-lg-10 col-md-10 col-sm-9">{{ $participant->section->$title }}</dd>

                                <dt class="col-md-2 text-truncate">{{ __('lang.name_work') }}</dt>
                                <dd class="col-md-10">{{ $participant->name }}</dd>

                                <dt class="col-md-2 text-truncate">{{ __('lang.avtors') }}</dt>
                                <dd class="col-md-10">{{ $participant->soavtor }}</dd>

                                <dt class="col-md-2 text-truncate">{{ __('lang.form_part') }}</dt>
                                <dd class="col-md-10">{{ $participant->form->$title }}</dd>

                                <dt class="col-md-2 text-truncate">{{ __('lang.services') }}</dt>
                                <dd class="col-md-10">1 {{ __('lang.osnBook') }}
                                    @if($participant->Nbook != 0)
                                        + {{ $participant->Nbook }}
                                        @if($participant->Nbook == 1)
                                            {{ __('lang.dopBook') }}
                                        @else
                                            {{ __('lang.dopBooks') }}
                                        @endif
                                    @endif
                                    @if($participant->dopolnitelno != null)
                                        + 1 {{ __('lang.enBook') }}
                                    @endif
                                    + 1 {{ __('lang.sertificate') }}
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@else
    <div class="container-fluid">
        <div class="col alert alert-danger alert-dismissible fade show" role="alert">
            {{ __('lang.activeEventsError1') }}<br>
            {{ __('lang.activeEventsError2') }} <a href="/" class="alert-link">http:\\sciendar.com</a>
        </div>
    </div>
@endif
