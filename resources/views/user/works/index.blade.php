@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3">
        <div class="card-header"><b>{{ 123 }} - Листинг городов</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">

                <section class="content">



                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal">
                        Добавить
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form class="was-validated">
                                <div class="modal-body">
                                    <div class="form-row">
                                        <div class="col-12">
                                            <label for="input1"><b>Вид роботи</b></label>
                                            <select class="custom-select browser-default" id="input1" required>
                                                <option value="" disabled selected>Зробіть вибір</option>
                                                <option value="1">Стаття</option>
                                                <option value="2">Тези</option>
                                                <option value="3">Книга</option>
                                                <!--option value="4">Патент</option-->
                                                <option value="5">Автореферат</option>
                                                <option value="6">Дисертація</option>
                                            </select>
                                            <br>
                                            <label for="input2"><b>Назва роботи</b></label>
                                            <input placeholder="Назва роботи" type="text" id="input2" class="form-control" required>
                                            <small id="passwordHelpBlock" class="form-text text-muted">
                                                Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces,
                                                special characters,
                                                or emoji.
                                            </small>
                                            <br>
                                            <label for="input3"><b>Автори</b> <a href="#" data-toggle="tooltip" data-html="true"
                                                                                 title="Зверніть увагу">
                                                    <i class="far fa-question-circle"></i>
                                                </a></label>
                                            <input placeholder="Наприклад: Іванов Іван Іванович, Петров Петро Петрович" type="text" id="input3" class="form-control" required>
                                            <br>
                                            <label for="input4"><b>Дата публікації</b></label>
                                            <input placeholder="Наприклад: 2019, 2019/12 или 2019/12/31" length="10" type="text" id="input4" class="form-control" required>
                                            <br>
                                            <div>
                                                <label for="input5"><b>Журнал</b></label>
                                                <input placeholder="Название журнала" type="text" id="input5" class="form-control" required>
                                            </div>
                                            <br>
                                            <label for="inputPlaceholderEx"><b>Том / Номер / Стр.</b></label>
                                            <div class="input-group">
                                                <input placeholder="Том" length="5" type="text" aria-label="First name" class="form-control" required>
                                                <input placeholder="Номер" length="5" type="text" aria-label="First name" class="form-control" required disabled>
                                                <input placeholder="Стр." length="10" type="text" aria-label="Last name" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary">Добавить</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>






                    <div class="box-footer">
                        <a href="/general/country" class="btn btn-default">Назад</a>
                    </div>

                </section>

            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.mdb-select').materialSelect();
        });


        $(function () {
            $('.material-tooltip-main').tooltip({
                template: '<div class="tooltip md-tooltip"><div class="tooltip-arrow md-arrow"></div><div class="tooltip-inner md-inner"></div></div>'
            });
        });
    </script>
@endsection
