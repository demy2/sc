<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" style="color: #4c00be">
    <thead>
    <tr class="text-center">
        <th>№</th>
        <th>Дата проведения</th>
        <th>Категории</th>
        <th>Назва</th>
        <th>Заклад</th>
    </tr>
    </thead>
    <tbody>
        @foreach($actualeEvents as $actualeEvent)
            <tr class="text-center">
                <td>{{ $loop->iteration }}</td>
                <td>{{ $actualeEvent->dateOnOff($actualeEvent) }}</td>
                <td>
                    @foreach($actualeEvent->categories as $category)
                        <a href="#" title="{{ $category->$title }}" style="color: rgb(76, 0, 190); font-size: 14 px"><b>{!! $category->image !!}</b></a>
                    @endforeach
                </td>
                <td class="text-left"><a href="/event/{{ $actualeEvent->id }}" class="text-primary">{{ $actualeEvent->$title }}</a></td>
                <td class="text-left">{{ $actualeEvent->univer->$title }}, {{ $actualeEvent->city->$title }}, {{ $actualeEvent->country->$title }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
