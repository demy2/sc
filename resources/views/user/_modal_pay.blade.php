<div class="modal fade" id="pay{{ $participant->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content deep-purple-text">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Стан оплати</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <dl class="row text-justify">
                    <dt class="col-9">{{ __('lang.uchast') }}</dt>
                    <dd class="col-3 text-right">{{ $participant->infoPay($participant)['uchastTxt'] }}</dd>

                    <dt class="col-9">{{ __('lang.dodBook') }}</dt>
                    <dd class="col-3 text-right">{{ $participant->infoPay($participant)['bookTxt'] }}</dd>

                    @if($participant->event->id != 2)
                        <dt class="col-9">{{ __('lang.engBook') }}</dt>
                        <dd class="col-3 text-right">{{ $participant->infoPay($participant)['dopTxt'] }}</dd>
                    @endif

                    <dt class="col-9"><hr></dt>
                    <dd class="col-3 text-right"><hr></dd>

                    <dt class="col-9">{{ __('lang.all') }}</dt>
                    <dd class="col-3 text-right">{{ $participant->infoPay($participant)['sumTxt'] }}</dd>
                </dl>
            </div>

                @csrf
@if($participant->pay == 2 or $participant->pay == 3)
            <div class="modal-footer">
                <div class="h2 text-uppercase text-success">
                    {{ __('lang.payOk') }} <i class="far fa-thumbs-up fa-2x"></i>
                </div>
            </div>
@else
            <div class="modal-footer">
{!! $participant->infoPay($participant)['html'] !!}
            </div>
@endif


        </div>
    </div>
</div>
