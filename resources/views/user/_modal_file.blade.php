<div class="modal fade" id="file{{ $participant->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title deep-purple-text" id="exampleModalLabel">Стан роботи</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if($participant->status == 1)
                <div class="container">
                    <form class="was-validated" action="{{ route('user.events.store') }}" enctype="multipart/form-data" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{ $participant->id }}">
                        <input type="hidden" name="status" value="2">
                        <div class="modal-body deep-purple-text">
                            <br>
                            {{ __('lang.loadFile') }}
                            <img src="\img\docx_upload2.gif" alt="" width="50%">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="File" name="work" accept=".doc, .docx" required>
                                <label class="custom-file-label" for="File">Choose file...</label>
                                <div class="invalid-feedback">Example invalid custom file feedback</div>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <br>
                        </div>

                        <div class="modal-footer">
                            <input type="submit" class="btn btn-primary" name="go_work">
                        </div>
                    </form>
                </div>
            @elseif($participant->status == 2)
                <div class="modal-body text-center deep-purple-text">
                    <br>
                    Мы получили Вашу работу и уже проверяем её<br>
                    <img src="\img\icon.gif" alt="" width="50%"><br>
                    Ожидайте результата проверки<br>
                    <br>
                </div>
            @endif
        </div>
    </div>
</div>
