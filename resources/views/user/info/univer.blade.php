@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary deep-purple-text">
        <div class="card-header h5"><b>{{ __('lang.titleUserInfoEdit') }}</b></div>
        <div class="card-body">
            <div class="container-fluid">
                <section class="content">
                    @include('_errors')
                    {!! Form::open([
                            'route'	=> 'user.univer.store',
                            'files'	=>	true
                        ])!!}
                    @csrf
                    <div class="row">
                        <div class="col-md-4"></div>

                        <div class="col-md-4">
                            <input type="hidden" name="user_id" value="{{ Auth::id() }}">

                            {{ __('lang.bodyUserInfoEdit') }}<br><br>
                            <label for="country">{{ __('lang.country') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-globe-africa" style="color: #4c00be"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="{{ __('lang.country') }}" name="country" id="country">
                            </div>
                            <br>
                            <label for="city">{{ __('lang.city') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-map" style="color: #4c00be"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="{{ __('lang.city') }}" name="city" id="city">
                            </div>
                            <br>
                            <label for="univer">{{ __('lang.university') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-landmark" style="color: #4c00be"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="{{ __('lang.university') }}" name="univer" id="univer">
                            </div>
                            <br>
                            <label for="website">{{ __('lang.site') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-laptop-code" style="color: #4c00be"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="{{ __('lang.site') }}" name="website" id="website" value="http://">
                            </div>
                        </div>

                        <div class="col-md-4"></div>
                    </div>



                    <br><br><br><br>
                    <!--<button type="button" class="btn btn-outline-info btn-block waves-effect">Сохранить</button>-->
                    <button class="btn aqua-gradient btn-lg btn-block waves-effect">{{ __('lang.save') }}</button>

                    {!! Form::close() !!}
                </section>

            </div>
        </div>
    </div>
@endsection
