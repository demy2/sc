@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary mb-3 deep-purple-text" >
        <div class="card-header h5"><b>{{ __('lang.titleUserInfoEdit') }}</b></div>
        <div class="card-body">
            <div class="container-fluid">
                <section class="content">
                    @include('_errors')
                    {!! Form::open([
                            'route'	=> ['user.info.update', Auth::id()],
                            'files'	=>	true,
                            'method'	=>	'put'
                        ])!!}
                    @csrf
                    <div class="row">
                        <div class="col-md-3 text-center">
                            <input type="hidden" name="user_id" value="{{ Auth::id() }}">

                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' id="imageUpload" name="avatar" accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload" class="light-blue-text"><i class="fas fa-camera fa-3x light-blue-text"></i></label>
                                </div>
                                <div class="avatar-preview">
                                    @if(!isset($userInfo->avatar) or $userInfo->avatar == null)
                                        <div id="imagePreview" style="background-image: url(/img/not-user.png);"></div>
                                    @else
                                        <div id="imagePreview" style="background-image: url(/uploads/{{ $userInfo->avatar }});"></div>
                                    @endif
                                </div>
                            </div>

                            <p class="help-block">{{ __('lang.Формат') }}</p>
                            <br><br><br>
                            <div class="custom-control custom-radio custom-control-inline">
                                @if($userInfo->gender == 1)
                                    <input type="radio" class="custom-control-input" id="male" name="gender" value="1" checked>
                                @else <input type="radio" class="custom-control-input" id="male" name="gender" value="1">
                                @endif
                                <label class="custom-control-label" for="male"><i class="fas fa-male fa-3x" style="color: blue"></i></label>
                            </div>
                            &thinsp;&thinsp;&thinsp;
                            <div class="custom-control custom-radio custom-control-inline">
                                @if($userInfo->gender == 2)
                                    <input type="radio" class="custom-control-input" id="female" name="gender" value="2" checked>
                                @else <input type="radio" class="custom-control-input" id="female" name="gender" value="2">
                                @endif
                                <label class="custom-control-label" for="female"><i class="fas fa-female fa-3x" style="color: red"></i></label>
                            </div>
                        </div>

                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="surname">{{ __('lang.surname') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-user" style="color: #4c00be"></i>&thinsp;</span>
                                        </div>
                                        <input type="text" class="form-control" id="surname" placeholder="{{ __('lang.surname') }}" name="surname" value="{{ $userInfo->surname }}">
                                    </div>
                                    <br>
                                </div>

                                <div class="col-md-4">
                                    <label for="name">{{ __('lang.name') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-user" style="color: #4c00be"></i>&thinsp;</span>
                                        </div>
                                        <input type="text" class="form-control" id="name" placeholder="{{ __('lang.name') }}" name="name" value="{{ $userInfo->name }}">
                                    </div>
                                    <br>
                                </div>

                                <div class="col-md-4">
                                    <label for="middle_name">{{ __('lang.middle_name') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-user" style="color: #4c00be"></i>&thinsp;</span>
                                        </div>
                                        <input type="text" class="form-control" id="middle_name" placeholder="{{ __('lang.middle_name') }}" name="middle_name" value="{{ $userInfo->middle_name }}">
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="date_of_birth">{{ __('lang.Дата рождения') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt" style="color: #4c00be"></i>&thinsp;</span>
                                        </div>
                                        {!! Form::date('date_of_birth', $userInfo->date_of_birth,
                                            ['class' => 'form-control', 'id'    =>  'datepicker3'])
                                        !!}
                                    </div>
                                    <br>
                                </div>

                                <div class="col-md-4">
                                    <label for="phone">{{ __('lang.phone') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">&thinsp;<i class="fas fa-mobile-alt" style="color: #4c00be"></i>&thinsp;</span>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="phone" placeholder="{{ __('lang.phone') }}" name="phone" value="{{ $userInfo->phone }}">
                                    </div>
                                    <br>
                                </div>

                                <div class="col-md-4">
                                    <label for="city">Наукові інтереси</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-book-reader" style="color: #4c00be"></i></span>
                                        </div>
                                        {{Form::select('categories[]',
                                            $categories,
                                            $userCat,
                                            ['class' => 'js-example-basic-multiple form-control browser-default', 'multiple'  =>  'multiple', 'data-placeholder'  =>  'Выберите перечень категорий'])
                                        }}
                                    </div>

                                    <br>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="country">{{ __('lang.country') }}</label>
                                    <a style="color: red" data-toggle="modal" data-target="#ModalCountry" title="{{ __('lang.questionTitle') }}">
                                        <i class="far fa-question-circle"></i>
                                    </a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="ModalCountry" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                         aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('lang.questionTitle') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    {{ __('lang.questionBody') }}<br>!!! {{ __('lang.NoCountry') }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-globe-africa" style="color: #4c00be"></i></span>
                                        </div>
                                        <select class="js-example-placeholder-single js-states form-control browser-default custom-select" name="country_id" id="countries">
                                            @foreach($countries as $key => $country)
                                                <option value="{{ $key }}" @if($country == $countryUser->$title)selected="selected" @endif>{{ $country }}</option>
                                            @endforeach
                                            <option value="999999"> !!! {{ __('lang.NoCountry') }}</option>
                                        </select>
                                    </div>
                                    <br>
                                </div>

                                <div class="col-md-4">
                                    <label for="city">{{ __('lang.city') }}</label>
                                    <a style="color: red" data-toggle="modal" data-target="#ModalCity" title="{{ __('lang.questionTitle') }}">
                                        <i class="far fa-question-circle"></i>
                                    </a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="ModalCity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                         aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('lang.questionTitle') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    {{ __('lang.questionBody') }}<br>!!! {{ __('lang.NoCity') }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-map" style="color: #4c00be"></i></span>
                                        </div>
                                        <select class="js-example-placeholder-single js-states form-control browser-default custom-select" name="city_id" id="cities">
                                            <option disabled="disabled" selected="selected">{{ $cityUser }}</option>
                                        </select>
                                    </div>
                                    <br>
                                </div>

                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">{{ __('lang.university') }}</label>
                                    <a style="color: red" data-toggle="modal" data-target="#ModalUniver" title="{{ __('lang.questionTitle') }}">
                                        <i class="far fa-question-circle"></i>
                                    </a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="ModalUniver" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                         aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('lang.questionTitle') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    {{ __('lang.questionBody') }}<br>!!! {{ __('lang.NoUniversity') }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-landmark" style="color: #4c00be"></i></span>
                                        </div>
                                        <select class="js-example-placeholder-single js-states form-control browser-default custom-select" name="univer_id" id="univery">
                                            <option disabled="disabled" selected="selected">{{ $univerUser->$title }}</option>
                                        </select>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <hr>

                            <div class="row ifpochta" @if($userInfo->country->id != 1) style="display: none;" @endif >
                                <div class="col-12">

                                    <div class="spinner-grow text-danger" role="status" style="width: 20px; height: 20px"></div>
                                    <b class="text-danger">Зверніть увагу!</b><br>
                                    Якщо місто проведення події не співпадає з містом Вашого постійного перебування, матеріали можуть бути надіслані Новою Поштою
                                </div>
                                <div class="col-md-6">
                                    <table class="table table-borderless">
                                        <tbody>
                                        <tr>
                                            <td class="align-middle">
                                                <a data-toggle="modal" data-target="#ModalNP" title="Змінити данні отримання">
                                                    <img src="\public\img\nova-poshta.png" alt="" height="50px">
                                                </a>
                                            </td>
                                            <td class="align-middle deep-purple-text">
                                                <a data-toggle="modal" data-target="#ModalNP" title="Змінити данні отримання">
                                                    <b>Отримувач:</b>
                                                    @if($userInfo->surnameNP !== null){{ $userInfo->surnameNP }}
                                                    @else <span class="text-danger"> !!не вказано прізвище!!</span>
                                                    @endif

                                                    @if($userInfo->nameNP !== null){{ $userInfo->nameNP }}
                                                    @else <span class="text-danger"> !!не вказано ім'я!!</span>
                                                    @endif

                                                    @if($userInfo->middle_nameNP !== null){{ $userInfo->middle_nameNP }},
                                                    @else <span class="text-danger"> !!не вказано ім'я по батькові!!</span>
                                                    @endif

                                                    @if($userInfo->phoneNP !== null){{ $userInfo->phoneNP }}
                                                    @else <span class="text-danger"> !!не вказано номер телефону!!</span>
                                                    @endif
                                                    <br>
                                                    <b>Відділення:</b> №
                                                    @if($userInfo->oNP !== null){{ $userInfo->oNP }},
                                                    @else <span class="text-danger"> !!не вказано номер відділення!!</span>
                                                    @endif

                                                    @if($userInfo->city_idNP !== null){{ $npCity }}
                                                    @else <span class="text-danger"> !!не вказано місто!!</span>
                                                    @endif
                                                </a>
                                            </td>
                                            <td class="align-middle">
                                                <a class="reddit-ic" role="button" style="color: red" data-toggle="modal" data-target="#ModalNP" title="Змінити данні отримання">
                                                    <i class="far fa-edit fa-2x"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <hr>
                            <div class="row">
                                <div class="col-md-2">

                                </div>

                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">{{ __('lang.position') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-user-tie" style="color: #4c00be"></i></span>
                                        </div>
                                        <select class="js-example-placeholder-single js-states form-control browser-default custom-select" name="position_id">
                                            <option disabled="disabled" selected="selected">{{ $position->$title }}</option>

                                            @foreach($positions as $key => $position)
                                                <option value="{{ $key }}">{{ $position }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <br>
                                </div>

                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">{{ __('lang.degree') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-user-graduate" style="color: #4c00be"></i></span>
                                        </div>
                                        <select class="js-example-placeholder-single js-states form-control browser-default custom-select" name="degree_id">
                                            <option disabled="disabled" selected="selected">{{ $degree->$title }}</option>

                                            @foreach($degrees as $key => $degree)
                                                <option value="{{ $key }}">{{ $degree }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <br>
                                </div>

                                <div class="col-md-2">

                                </div>
                            </div>

                        </div>
                    </div>



                    <br><br><br><br>
                    <!--<button type="button" class="btn btn-outline-info btn-block waves-effect">Сохранить</button>-->
                    <button class="btn aqua-gradient btn-lg btn-block waves-effect" name="subUI">Зберігти зміни</button>

                    {!! Form::close() !!}
                </section>
                <!-- Modal -->
                <div class="modal fade" id="ModalNP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    @include('_errors')
                    {!! Form::open([
                            'route' => ['user.info.update', Auth::id()],
                            'files' =>  true,
                            'method'    =>  'put'
                        ])!!}
                    @csrf
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Данні для доставки Новою Поштою</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form class="was-validated">
                                    <label for="surname">{{ __('lang.surname') }}  {{ __('lang.name') }}  {{ __('lang.middle_name') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-user" style="color: #4c00be"></i>&thinsp;</span>
                                        </div>
                                        <input type="text" class="form-control" id="surnameNP" placeholder="{{ __('lang.surname') }}" name="surnameNP" value="{{ isset($userInfo->surnameNP) ? $userInfo->surnameNP : $userInfo->surname }}" required>
                                        <input type="text" class="form-control" id="nameNP" placeholder="{{ __('lang.name') }}" name="nameNP" value="{{ isset($userInfo->nameNP) ? $userInfo->nameNP : $userInfo->name }}" required>
                                        <input type="text" class="form-control" id="middle_nameNP" placeholder="{{ __('lang.middle_name') }}" name="middle_nameNP" value="{{ isset($userInfo->middle_nameNP) ? $userInfo->middle_nameNP : $userInfo->middle_name }}" required>
                                    </div>
                                    <br>

                                    <label for="phone">{{ __('lang.phone') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">&thinsp;<i class="fas fa-mobile-alt" style="color: #4c00be"></i>&thinsp;</span>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="phoneNP" placeholder="{{ __('lang.phone') }}" name="phoneNP" value="{{ isset($userInfo->phoneNP) ? $userInfo->phoneNP : $userInfo->phone }}" required>
                                    </div>
                                    <br>
                                    <label for="city_idNP">{{ __('lang.city') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-map" style="color: #4c00be"></i></span>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="city_idNP"  name="city_idNP" value="{{ isset($npCity) ? $npCity : $cityUser }}" required>
                                    </div>
                                    <br>

                                    <label for="oNP">Номер відділення</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">&thinsp;<i class="fas fa-arrows-alt" style="color: #4c00be"></i>&thinsp;</span>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="oNP"  name="oNP" value="{{ $userInfo->oNP !== null ? $userInfo->oNP : '' }}" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><a type="btn" href="https://novaposhta.ua/office" target="_blank">знайти відділення</a></span>
                                        </div>
                                    </div>
                                </form>

                                <br>
                                <br>

                                <button class="btn aqua-gradient btn-lg btn-block waves-effect" name="subNP">Зберігти зміни</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });

        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });



        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
        });

        $('#countries').on('change', function(e){
            var country_id = e.target.value;
            $.get('/json-cities?country_id=' + country_id,function(data) {
                //  console.log(data);
                $('#cities').empty();
                $('#cities').append('<option disabled="disabled" selected="selected"> {!! __('lang.Выбор') !!} </option>');

                $.each(data, function(index, citiesObj){
                    $('#cities').append('<option value="'+ citiesObj.id +'">'+ citiesObj.title_{{ app()->getLocale() }} +'</option>');
                });
                $('#cities').append('<option value="999999"> !!! {{ __('lang.NoCity') }} </option>');
            });
        });

        $('#cities').on('change', function(e){
            var cities_id = e.target.value;
            $.get('/json-university?cities_id=' + cities_id,function(data) {

                $('#univery').empty();
                $('#univery').append('<option disabled="disabled" selected="selected"> {!! __('lang.Выбор') !!} </option>');

                $.each(data, function(index, universitiesObj){
                    $('#univery').append('<option value="'+ universitiesObj.id +'">'+ universitiesObj.title_{{ app()->getLocale() }} +'</option>');
                });
                $('#univery').append('<option value="999999"> !!! {{ __('lang.NoUniversity') }} </option>');
            });
        });
    </script>
@endsection
