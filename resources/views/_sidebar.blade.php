@if(Auth::user()->role != 1)
    <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown">
            @if(Auth::user()->role == 8)
                @if( Request::segment(1) == 'general' or Request::segment(2) == 'general')
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false"><i class="fas fa-user-astronaut"></i> {{ __('lang.general') }}</a>
                <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/user/events"><i class="fas fa-user"></i> {{ __('lang.user') }}</a>
                    <a class="dropdown-item" href="/admin"><i class="fas fa-user-graduate"></i> {{ __('lang.admin') }}</a>
                </div>
                @elseif(Request::segment(1) == 'admin' or Request::segment(2) == 'admin')
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"><i class="fas fa-user-graduate"></i> {{ __('lang.admin') }}</a>
                    <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/user/events"><i class="fas fa-user"></i> {{ __('lang.user') }}</a>
                        @if(Auth::user()->role == 8)
                            <a class="dropdown-item" href="/general"><i class="fas fa-user-astronaut"></i> {{ __('lang.general') }}</a>
                        @endif
                    </div>
                @elseif(Request::segment(1) == 'user' or Request::segment(2) == 'user')
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"><i class="fas fa-user"></i> {{ __('lang.user') }}</a>
                    <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/admin"><i class="fas fa-user-graduate"></i> {{ __('lang.admin') }}</a>
                        @if(Auth::user()->role == 8)
                            <a class="dropdown-item" href="/general"><i class="fas fa-user-astronaut"></i> {{ __('lang.general') }}</a>
                        @endif
                    </div>
                @else
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"><i class="fas fa-user-secret"></i> role</a>
                    <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/user/events"><i class="fas fa-user"></i> {{ __('lang.user') }}</a>
                        <a class="dropdown-item" href="/admin"><i class="fas fa-user-graduate"></i> {{ __('lang.admin') }}</a>
                        @if(Auth::user()->role == 8)
                            <a class="dropdown-item" href="/general"><i class="fas fa-user-astronaut"></i> {{ __('lang.general') }}</a>
                        @endif
                    </div>
                @endif
            @endif
        </li>
    </ul>
@endif


@if( Request::segment(1) == 'admin' or Request::segment(2) == 'admin')
    <ul class="navbar-nav mr-auto">
        <li class="nav-item"><a class="nav-link"href="{{ route('admin.events.index') }}"><i class="far fa-copy"></i> Созданные события</a></li>
    </ul>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item"><a class="nav-link"href="{{ route('admin.users.index') }}"><i class="fas fa-users"></i><span>Учасники</span></a></li>
    </ul>
@elseif( Request::segment(1) == 'general' or Request::segment(2) == 'general')
    <ul class="navbar-nav mr-auto">
        <li class="nav-item"><a class="nav-link" href="{{ route('general.events.index') }}"><i class="far fa-copy"></i> События</a></li>
    </ul>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item"><a class="nav-link" href="{{ route('general.country.index') }}"><i class="fas fa-landmark"></i> Университеты</a></li>
    </ul>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item"><a class="nav-link" href="/general/info"><i class="fas fa-indent"></i> Информация</a></li>
    </ul>
@else
    <ul class="navbar-nav mr-auto">

    </ul>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item"><a class="nav-link" href="{{ route('index') }}"><i class="far fa-copy"></i><span> {{ __('lang.AllEvents') }}</span></a></li>
    </ul>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item"><a class="nav-link" href="{{ route('user.userEvents') }}"><i class="far fa-file-alt"></i><span> {{ __('lang.MyEvents') }}</span></a></li>
    </ul>
@endif
