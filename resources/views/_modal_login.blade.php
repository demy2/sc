<div class="modal fade" id="Modal_Login" tabindex="-1" role="dialog" aria-labelledby="Modal_Login" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <!-- Навигация -->
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true">{{ __('lang.Login') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="reg-tab" data-toggle="tab" href="#reg" role="tab" aria-controls="reg" aria-selected="false">{{ __('lang.Register') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="forgot-tab" data-toggle="tab" href="#forgot" role="tab" aria-controls="forgot" aria-selected="false">Восстановление пароля</a>
                    </li>
                </ul>
                <!-- /Навигация -->

                <!-- Содержимое вкладок -->
                <div class="tab-content" id="myTabContent">
                    <!-- Вход -->
                    <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                        <br>@include('auth._login')
                    </div>
                    <!-- /Вход -->

                    <!-- Регистрация -->
                    <div class="tab-pane fade" id="reg" role="tabpanel" aria-labelledby="reg-tab">
                        <br>@include('auth._register')
                    </div>
                    <!-- /Регистрация -->

                    <!-- Восстановление пароля -->
                    <div class="tab-pane fade" id="forgot" role="tabpanel" aria-labelledby="forgot-tab">
                        <br>@include('auth.passwords.email')
                    </div>
                    <!-- /Восстановление пароля -->
                </div>
                <!-- /Содержимое вкладок -->
            </div>
        </div>
    </div>
</div>
