<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>



    <title>Sciendar - {{ __('lang.Calendar of scientific events') }}</title>
    <meta name="author" content="Sciendar.com">
    <meta name="description" content="{{ __('lang.Calendar of scientific events') }}">
    <meta property="og:image" content="\img\sciendar.jpg">
    <meta property="og:description" content="{{ __('lang.Calendar of scientific events') }}">
    <meta property="og:title" content="Sciendar - {{ __('lang.Calendar of scientific events') }}">
    <link rel="shortcut icon" href="\img\logoSc.png">
    <meta name="keywords" content="конференції, конференции">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!-- MDBootstrap -->
    <link href="/css/mdb.css" rel="stylesheet">
    <!-- MDBootstrap animations-extended -->
    <link href="/css/modules/animations-extended.css" rel="stylesheet" />
    <!-- MDBootstrap Cards Extended Pro
    <link href="/css/addons-pro/cards-extended.css" rel="stylesheet">-->

    <!-- Select2 from -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <!-- Datatables  -->
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Карточки событий -->
    <link rel="stylesheet" href="/css/card-events.css">

    <!-- My Styles -->
    <link href="/css/style.css" rel="stylesheet">


    <!-- Timeline CSS -->
    <link href="/css/addons-pro/timeline.css" rel="stylesheet">
    <!-- Timeline CSS - minified-->
    <link href="/css/addons-pro/timeline.min.css" rel="stylesheet">


    <!-- Stepper CSS -->
    <link href="/css/addons-pro/steppers.css" rel="stylesheet">
    <!-- Stepper CSS - minified-->
    <link href="/css/addons-pro/steppers.min.css" rel="stylesheet">


    <!-- JQuery -->
    <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/js/modules/cards.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="grey lighten-3">

<!--Main Navigation-->
<header>
    <!--Navbar -->
    <nav class="mb-1 navbar fixed-top navbar-expand-lg navbar-dark indigo scrolling-navbar">
        <a class="navbar-brand" href="/"><img src="\img\Sciendar.png" height="50px" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar">

            @auth
                @include('_sidebar')
            @endauth

                <div class="navbar-nav ml-auto">
                &#8194;&#8194;&#8194;
                <!-- Инфо панель -->
                <li class="nav-item avatar dropdown">
                    <a class="nav-link" href="/info"><i class="fas fa-info-circle"></i></a>
                </li>
                <!-- /Инфо панель -->
                &#8194;&#8194;&#8194;
                <!-- Языковая панель -->
                <li class="nav-item avatar dropdown">
                    <a class="nav-link dropdown-toggle" id="Lang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(App::isLocale('ua'))<img src="/img/flag-ua.png" alt="" height="35">&#8194;Українська @endif
                        @if(App::isLocale('ru'))<img src="/img/flag-ru.png" alt="" height="35">&#8194;Русский @endif
                        @if(App::isLocale('en'))<img src="/img/flag-en.png" alt="" height="35">&#8194;English @endif
                        <i class="fas fa-caret-down"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Lang">
                        @if(!App::isLocale('ua'))<a class="dropdown-item" href="<?= route('setlocale', ['lang' => 'ua']) ?>"><img src="/img/flag-ua.png" alt="" width="20 px">&#8194;Українська</a>@endif
                        @if(!App::isLocale('ru'))<a class="dropdown-item" href="<?= route('setlocale', ['lang' => 'ru']) ?>"><img src="/img/flag-ru.png" alt="" width="20 px">&#8194;Русский</a>@endif
                        @if(!App::isLocale('en'))<a class="dropdown-item" href="<?= route('setlocale', ['lang' => 'en']) ?>"><img src="/img/flag-en.png" alt="" width="20 px">&#8194;English</a>@endif
                    </div>
                </li>
                <!-- /Языковая панель -->
                &#8194;&#8194;&#8194;
                <!-- Вход/выход -->
                @if (Route::has('login'))
                    @auth
                        <li class="nav-item avatar dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownAuth" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(Auth::user()->info != null)
                                    <img src="{{ Auth::user()->info->getAvatar() }}" class="rounded-circle z-depth-0" alt="avatar image" height="35">&#8194;{{ Auth::user()->email }} <i class="fas fa-caret-down"></i>
                                @else
                                    <img src="/img/not-user.png" class="rounded-circle z-depth-0" alt="avatar image" height="35">&#8194;{{ Auth::user()->email }} <i class="fas fa-caret-down"></i>
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownAuth">
                                <a class="dropdown-item" href="{{ route('user.info.index') }}"><i class="fas fa-user-cog"></i> {{ __('lang.edit') }}</a>
                                <a class="dropdown-item" href="{{ route('user.myref') }}"><i class="fas fa-users"></i> {{ __('lang.ref') }}</a>
                                <hr>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt"></i> {{ __('lang.Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @else
                        <li class="nav-item avatar dropdown">
                            <a class="nav-link dropdown-toggle" id="MenuAuth" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                <div class="spinner-grow text-primary fast" role="status"
                                     style="position: absolute; left: 2px; width: 2rem; height: 2rem;">
                                    <i class="fas fa-power-off fa-2x" style="color: lime"></i></div>
                                <i class="fas fa-power-off fa-2x" style="color: lime"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="MenuAuth">
                                <a class="dropdown-item" href="{{ route('login') }}">{{ __('lang.Login') }}</a>
                                <a class="dropdown-item" href="{{ route('register') }}">{{ __('lang.Register') }}</a>
                                <a class="dropdown-item" href="{{ route('password.request') }}">{{ __('lang.ResetPas') }}</a>
                            </div>
                        </li>
                    @endauth
                @endif



            <!-- /Вход/выход -->
            </div>
        </div>
    </nav>
    <!--/.Navbar -->
</header>
<!--Main Navigation-->




<!-- Page Content  -->
<br><br>



<div class="container-fluid" id="content">
    @yield('content')
</div>
<br><br><br>
<!-- /Page Content  -->

<!-- Footer -->
<footer class="page-footer font-small indigo fixed-bottom">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2019
        <a href="http://sciendar.com"> Sciendar.com</a><br>
        <div class="text-center" style="font-size: 14px">В данний час сайт працює в тестовому режимі. З усіх питань звертайтесь на електрону адресу <span class="text-primary">demy2@ukr.net</span></div>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->




<!-- SCRIPTS -->

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="/js/popper.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="/js/bootstrap.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="/js/mdb.js"></script>
<!-- Datatables  -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

<!-- из-за них ошибка нажатия меню
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
-->

<!-- card-sorting -->
<script src="/js/card-sorting.js"></script>



<!-- Select2 from -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<!-- MyScripts -->
<script type="text/javascript" src="/js/scripts.js"></script>



<!-- Stepper JavaScript -->
<script type="text/javascript" src="/js/addons-pro/stepper.js"></script>
<!-- Stepper JavaScript - minified -->
<script type="text/javascript" src="/js/addons-pro/stepper.min.js"></script>


<!-- ckeditor -->
<script src="/plugins/ckeditor/ckeditor.js"></script>
<script src="/plugins/ckfinder/ckfinder.js"></script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135514382-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-135514382-1');
</script>
<!-- /Global site tag (gtag.js) - Google Analytics -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(52631191, "init", {
        id:52631191,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/52631191" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>

</html>
