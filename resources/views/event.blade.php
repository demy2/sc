@extends('index')

@section('content')
    <br><br>
    <div class="alert alert-primary alert-dismissible fade show deep-purple-text h4 text-center" role="alert">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-6">
                    @auth
                        <img src="/img/action.png" alt="" height="80px"><br>
                        {{ __('lang.actionEvent') }}<br>
                    @else
                        <img src="/img/login.png" alt="" height="80px"><br>
                        {{ __('lang.start') }}<br>
                    @endauth
                </div>
            </div>
        </div>


        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="card border-primary deep-purple-text">
        <div class="card-header clearfix" style="max-height: 60px">
            <h1 class="text-uppercase float-left">{{ $event->$title }}</h1>
            <h5 class="float-right">
                @if((Auth::id()) != 0)
                    <a onclick="copyToClipboard('#p1')" href="#" data-toggle="popover-hover" data-content="ok">
                        <title id="p1" >http://sciendar.com/event/{{ $event->id }}/{{ Auth::id() }}</title><i class="fas fa-link cyan-text fa-spin"></i>
                    </a>
                @endif
            </h5>
        </div>
        <div class="card-body text-primary">

            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 text-right">
                            <img class="align-middle" src="{{ $event->getImage() }}" alt="" style="height: 250px">
                        </div>
                        <div class="col-md-6" style="font-size: 20px; color: #4c00be" >
                            <div class="row">
                                <p><i class="far fa-calendar-alt" style="width: 50px"></i>{{ $event->dateOnOff($event) }}</p>
                            </div>

                            <div class="row">
                                <p><i class="fas fa-map-marked-alt" style="width: 50px"></i>{{ $univer->$short_title }}, {{ $city->$title }}, {{ $country->$title }}</p>
                            </div>

                            <div class="row">
                                <p><i class="fas fa-user-tie" style="width: 50px"></i>@if(App::isLocale('ru'))Бондарь Дмитрий Викторович@elseif(App::isLocale('en'))Bondar Dmitrij Victorovich @else{{ $user->info->surname }} {{ $user->info->name }} {{ $user->info->middle_name }}@endif</p>
                            </div>

                            <div class="row">
                                <p><i class="fas fa-envelope" style="width: 50px"></i>{{ $user->email }}</p>
                            </div>

                            <div class="row">
                                <p><i class="fas fa-mobile-alt" style="width: 50px"></i>{{ $user->info->phone }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                @include('_errors')
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            @if($event->status == 1 and $event->date_stop != null)
                                @guest
                                    <a href="/login">
                                        <button type="button" class="btn btn-primary btn-block btn-lg text-uppercase">
                                            <b style="font-size: 24px">{{ __('lang.participate') }}</b>

                                            <div class="text-lowercase">
                                                @if($event->dateOnOff($event) == "nonstop")

                                                @else
                                                    @if(!App::isLocale('en')) <span id="days"></span> дн.  <span id="hours"></span> : <span id="minutes"></span> : <span id="seconds">
                                                    @else <span id="days"></span> days  <span id="hours"></span> hours  <span id="minutes"></span> min   <span id="seconds"></span> sec
                                                    @endif
                                                @endif
                                            </div>
                                        </button>
                                    </a>
                                @else
                                <!-- Кнопка пуска модальное окно -->
                                    <button type="button" class="btn btn-primary btn-block btn-lg text-uppercase" data-toggle="modal" data-target="#myModal">
                                        <b style="font-size: 24px">{{ __('lang.participate') }}</b>

                                        <div class="text-lowercase">
                                            @if($event->dateOnOff($event) == "nonstop")

                                            @else
                                                @if(!App::isLocale('en')) <span id="days"></span> дн.  <span id="hours"></span> : <span id="minutes"></span> : <span id="seconds">
                                                @else <span id="days"></span> days  <span id="hours"></span> hours  <span id="minutes"></span> min   <span id="seconds"></span> sec
                                                @endif
                                            @endif
                                        </div>
                                    </button>
                                    <!-- Модальное окно -->
                                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <form  class="was-validated" method="post" action="{{route ('eventPart', array('event'=>$event['id'])) }}">
                                                    @csrf
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{ __('lang.application') }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    @include('_errors')
                                                    <div class="modal-body grey-text">

                                                        <!-- Default select -->
                                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                                        <input type="hidden" name="event_id" value="{{$event->id}}">

                                                        <label for="exampleForm2"><b>{{ __('lang.select_section') }}</b></label><br>
                                                        <select class="browser-default custom-select" name="section_id" required>
                                                            <option value="" disabled selected>{{ __('lang.Выбор') }}</option>
                                                            @foreach($sections as $section)
                                                                <option class="grey-text" value="{{ $section->id }}">{{ $loop->iteration }}. {{ str_limit($section->$title, 48) }}</option>
                                                            @endforeach
                                                        </select>
                                                        <br>
                                                        <!-- Default input -->
                                                        <label for="exampleForm2"><b>{{ __('lang.name_work') }}</b></label>
                                                        <input type="text" id="exampleForm2" class="form-control is-valid" name="name" required>
                                                        <br>
                                                        <!-- Default input -->
                                                        <label for="exampleForm2"><b>{{ __('lang.soavtor') }}</b></label>
                                                        <input type="text" id="exampleForm2" placeholder="{{ __('lang.example') }}" class="form-control is-valid" name="soavtor" required>
                                                        <br>
                                                        @if($event->id == 2)
                                                            <input type="hidden" name="form_id" value="4">
                                                        @else
                                                            <label><b>{{ __('lang.form_part') }}</b></label><br>
                                                            @foreach ($forms as $form)
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" class="form-check-input" id="{{ $form->id }}" name="form_id" value="{{ $form->id }}" required>
                                                                    <label class="custom-control-label" for="{{ $form->id }}">{{ $form->$title }}</label>
                                                                </div>
                                                            @endforeach

                                                            <br><br>

                                                            <label><b>{{ __('lang.additional_services') }}</b></label>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="defaultUnchecked" name="dopolnitelno">
                                                                <label class="custom-control-label grey-text" for="defaultUnchecked">{{ __('lang.translation') }}</label>
                                                            </div>
                                                            <br>
                                                        @endif
                                                        <label><b>{{ __('lang.NBook') }}</b></label><br>
                                                        @for ($i = 0; $i < 4; $i++)
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" class="form-check-input" id="N_book{{ $i }}" name="Nbook" value="{{ $i }}" @if($i == 0)checked @endif>
                                                                <label class="custom-control-label" for="N_book{{ $i }}">{{ $i }}</label>
                                                            </div>
                                                        @endfor
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">{{ __('lang.send') }}</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endguest
                            @else
                                <button type="button" class="btn btn-primary btn-block btn-lg text-uppercase">
                                    <b style="font-size: 24px">Реєстрацію завершено</b><br>
                                    участь більше не можлива
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
                <br>
                <div class="container-fluid">
                    <!--Accordion wrapper-->
                    <div class="accordion md-accordion accordion-blocks" id="accordionEx" role="tablist" aria-multiselectable="true">

                        <!-- Общая информация -->
                        <div class="card">
                            <!-- Card header -->
                            <div class="card-header grey lighten-4" role="tab" id="headingOne1">
                                <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
                                   aria-controls="collapseOne1">
                                    <h5 class="mb-0">
                                        {{ __('lang.generalInfo') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>
                            <!-- Card body -->
                            <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
                                <div class="card-body">
                                    <div class="container-fluid {{ $style }}">
                                        <p><b>{{ __('lang.description') }}:</b>&nbsp;&nbsp;{{ $event->$description }}</p>
                                        <p><b>{{ __('lang.target') }}:</b>&nbsp;&nbsp;{{ $event->$target }}</p>
                                        <p><b>{{ __('lang.lang') }}:</b>&nbsp;&nbsp;<img src="/img/flag-ua.png" alt="" width="30 px"> <img src="/img/flag-ru.png" alt="" width="30 px"> <img src="/img/flag-en.png" alt="" width="30 px"></p>
                                        <p><b>{{ __('lang.other') }}:</b>&nbsp;&nbsp;{{ $event->$other }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./Общая информация -->

                        <!-- Секции -->
                        <div class="card">
                            <!-- Card header -->
                            <div class="card-header grey lighten-4" role="tab" id="headingTwo2">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                                   aria-expanded="false" aria-controls="collapseTwo2">
                                    <h5 class="mb-0">
                                        {{ __('lang.sections') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>
                            <!-- Card body -->
                            <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionEx">
                                <div class="card-body">
                                    <div class="container-fluid {{ $style }}">
                                        <ol>
                                            @foreach($sections as $section)
                                                <li>{{ $section->$section_title }}</li>
                                            @endforeach
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./Секции -->

                        <!-- Правила оформления -->
                        <div class="card">
                            <!-- Card header -->
                            <div class="card-header grey lighten-4" role="tab" id="headingThree3">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
                                   aria-expanded="false" aria-controls="collapseThree3">
                                    <h5 class="mb-0">
                                        {{ __('lang.designRules') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>
                            <!-- Card body -->
                            <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3" data-parent="#accordionEx">
                                <div class="card-body">
                                    <div class="container-fluid {{ $style }}">
                                        <div>{!! $event->$design_rules !!}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./Правила оформления -->

                        <!-- Стоимость и оплата -->
                        <div class="card">
                            <!-- Card header -->
                            <div class="card-header grey lighten-4" role="tab" id="headingThree4">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree4"
                                   aria-expanded="false" aria-controls="collapseThree4">
                                    <h5 class="mb-0">
                                        {{ __('lang.payment') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>
                            <!-- Card body -->
                            <div id="collapseThree4" class="collapse" role="tabpanel" aria-labelledby="headingThree4" data-parent="#accordionEx">
                                <div class="card-body">
                                    <div class="container-fluid {{ $style }}">
                                        <div>{!! $event->$pay !!}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./Стоимость и оплата -->

                        <!-- Оргкоммитет -->
                        <div class="card">
                            <!-- Card header -->
                            <div class="card-header grey lighten-4" role="tab" id="headingThree5">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree5"
                                   aria-expanded="false" aria-controls="collapseThree5">
                                    <h5 class="mb-0">
                                        {{ __('lang.orgcom') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>
                            <!-- Card body -->
                            <div id="collapseThree5" class="collapse" role="tabpanel" aria-labelledby="headingThree5" data-parent="#accordionEx">
                                <div class="card-body">
                                    <div class="container-fluid {{ $style }}">
                                        <div>{!! $event->$orgcom !!}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./Оргкоммитет -->

                        <!-- Документация -->
                        <div class="card">
                            <!-- Card header -->
                            <div class="card-header grey lighten-4" role="tab" id="headingThree6">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree6"
                                   aria-expanded="false" aria-controls="collapseThree6">
                                    <h5 class="mb-0">
                                        {{ __('lang.docs') }} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </a>
                            </div>
                            <!-- Card body -->
                            <div id="collapseThree6" class="collapse" role="tabpanel" aria-labelledby="headingThree6" data-parent="#accordionEx">
                                <div class="card-body">
                                    <div class="container-fluid {{ $style }}">
                                        <div>{!! $event->$docs !!}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./Документация -->

                    </div>
                    <!-- Accordion wrapper -->
                </div>
            </section>
        </div>
    </div>

    <script>
        const second = 1000,
            minute = second * 60,
            hour = minute * 60,
            day = hour * 24;

        let countDown = new Date('Apr 16, 2019 00:00:00').getTime(),
            x = setInterval(function() {

                let now = new Date().getTime(),
                    distance = countDown - now;

                document.getElementById('days').innerText = Math.floor(distance / (day)),
                document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
                document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
                document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);
            }, second)
    </script>
@endsection
