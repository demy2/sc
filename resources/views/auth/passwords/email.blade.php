@extends('index')

@section('content')
    <br><br><br>

    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card border-primary">
                <div class="card-header"><b>{{ __('Reset Password') }}</b></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <dl class="dl-horizontal">
                            <div class="form-group">
                                <dt>{{ __('E-Mail Address') }}</dt>
                                <dd>
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </dd>
                            </div>
                        </dl>

                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Send Password Reset Link') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection



