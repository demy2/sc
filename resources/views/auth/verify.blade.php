@extends('index')

@section('content')
<br><br><br>
<div class="row justify-content-center">
    <div class="col-md-4">
        <div class="card border-primary">
            <div class="card-header"><b>{{ __('lang.Verify Your Email Address') }}</b></div>

            <div class="card-body text-justify">
                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('lang.A fresh verification link has been sent to your email address.') }}
                    </div>
                @endif

                <p>{{ __('lang.Before proceeding, please check your email for a verification link.') }}</p><br>
                <p>{{ __('lang.If you did not receive the email') }}<a href="{{ route('verification.resend') }}" style="color: #4a148c">{{ __('lang.click here to request another') }}</a>.</p>
            </div>
        </div>
    </div>
</div>
@endsection
