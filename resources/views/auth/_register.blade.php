<div class="row">
    <div class="col-md-12">
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="form-group">
                <!-- добавляем реферальный айди-->
                @if((Cookie::get('ref_id')) != null)
                    <input type="" name="ref_id" value="{{ Cookie::get('ref_id') }}">
                @else
                    <input type="hidden" name="ref_id" value="0">
                @endif
                <label for="email" class="col-md-12 offset-md-12 col-form-label">{{ __('lang.E-Mail Address') }}</label>

                <div class="col-md-12 offset-md-12">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="col-md-12 offset-md-12 col-form-label">{{ __('lang.Password') }}</label>

                <div class="col-md-12 offset-md-12">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-12 offset-md-12 col-form-label">{{ __('lang.Confirm Password') }}</label>

                <div class="col-md-12 offset-md-12">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
            </div>

            <div class="form-group">
                <div class="container">
                    <div class="row justify-content-end">
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-block aqua-gradient">
                                {{ __('lang.Register') }}
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>

