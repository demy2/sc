<div class="row">
    <div class="col-md-12">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group">
                <label for="email" class="col-sm-12 offset-md-12 col-form-label">{{ __('lang.E-Mail Address') }}</label>

                <div class="col-md-12 offset-md-12">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="col-md-12 offset-md-12 col-form-label">{{ __('lang.Password') }}</label>

                <div class="col-md-12 offset-md-12">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12 offset-md-12">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('lang.Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="container">
                    <div class="row justify-content-end">
                        <div class="col-md-6 text-left"><br><a href="{{ route('password.request') }}">{{ __('lang.ResetPas') }}</a></div>
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-block aqua-gradient">
                                {{ __('lang.Login') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
