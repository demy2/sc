@extends('index')

@section('content')
    <br><br><br>

    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card border-primary deep-purple-text">
                <div class="card-header clearfix">
                    <div class="text-uppercase float-left"><b>{{ __('lang.Login') }}</b></div>
                    <div class="float-right"><a href="{{ route('register') }}">{{ __('lang.Register') }}</a></div>
                </div>
                <div class="card-body">
                    @include('auth._login')
                </div>
            </div>
        </div>
    </div>
@endsection
