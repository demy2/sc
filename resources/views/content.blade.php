@extends('index')

@section('content')
<br><br>
<div class="alert alert-primary alert-dismissible fade show deep-purple-text h4 text-center" role="alert">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                @auth
                    <img src="/img/eve.png" alt="" height="80px"><br>
                    {{ __('lang.selEvent') }}<br>
                @else
                    <img src="/img/login.png" alt="" height="80px"><br>
                    {{ __('lang.start') }}<br>
                @endauth
            </div>
        </div>
    </div>

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="card border-primary deep-purple-text">
    <div class="card-header clearfix">
        <h1 class="text-uppercase float-left">{{ __('lang.ActiveEvents') }}</h1>
        <h5 class="float-right">
            <a href="/archive" title="{{ __('lang.archiveEvents') }}">
                {{ __('lang.archiveEvents') }}
            </a>
        </h5>
    </div>

    <div class="card-body text-primary">
        <div class="container-fluid">
            <section class="content">
                @include('_events')
            </section>
        </div>
    </div>
</div>
@endsection

