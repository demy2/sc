<!-- перечень событий -->

<section id="Event" class="content">

    <!-- Выбор научных интересов -->
    <div class="row">
        @foreach($categories as $category)
            <div class="col-lg-2 col-md-4 col-sm-6 text-center text-truncate" >
                <a href="#" class="eventSelect" id="{{$category->title_en}}">
                    <i class="btn-floating btn-lg aqua-gradient-rgba">{!! $category->image !!}</i>
                    <h6>{{ $category->countCategory($category->id) }}</h6>
                    <h3 class="card-title text-uppercase">{{ $category->$title }}</h3>
                </a>
            </div>
        @endforeach
    </div>
    <br>

    <div class="row justify-content-md-center" id="event">



        <div class="col-lg-6 col-md-12 Chemistry Physics Biology Mathematics Economics Humanities">
            <div class="card card-cascade narrower mb-4" style="min-height: 320px">
                <div class="view view-cascade gradient-card-header purple-gradient mr-2 ml-2" style="min-height: 150px">
                    <h2 class="card-header-title text-uppercase"><b>перелік всіх наукових подій України</b><br></h2>
                </div>

                <div class="card-body card-body-cascade text-center">
                    <br>
                    <div class="row">
                        <div class="col-lg-2 col-md-4 col-sm-6 text-center text-truncate" >
                            <a href="/chemistry" class="eventSelect">
                                <i class="fas fa-flask fa-2x"></i>
                                <h3 class="card-title">{{$countChemistry}}</h3>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 text-center text-truncate" >
                            <a href="/physics" class="eventSelect">
                                <b><i class="fab fa-react fa-2x"></i></b>
                                <h3 class="card-title">{{$countPhysics}}</h3>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 text-center text-truncate" >
                            <a href="/biology" class="eventSelect">
                                <i class="fas fa-dna fa-2x"></i>
                                <h3 class="card-title">{{$countBiology}}</h3>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 text-center text-truncate" >
                            <a href="/maths" class="eventSelect">
                                <i class="fas fa-square-root-alt fa-2x"></i>
                                <h3 class="card-title">{{$countMathematics}}</h3>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 text-center text-truncate" >
                            <a href="/economics" class="eventSelect">
                                <i class="fas fa-dollar-sign fa-2x"></i>
                                <h3 class="card-title">{{$countEconomics}}</h3>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 text-center text-truncate" >
                            <a href="/humanities" class="eventSelect">
                                <i class="fas fa-street-view fa-2x"></i>
                                <h3 class="card-title">{{$countHumanities}}</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(isset($events))
            @foreach($events as $event)
                <div class="col-lg-6 col-md-12 {{ $event->getCategoryTitle() }}">
                    <a href="{{ route('eventPart', $event->id) }}">
                        <div class="card card-cascade narrower mb-4" style="min-height: 320px">
                            <div class="view view-cascade gradient-card-header blue-gradient mr-2 ml-2" style="min-height: 150px">
                                <h2 class="card-header-title text-uppercase"><b>{{ str_limit($event->$title, 35) }}</b></h2>

                            </div>

                            <div class="card-body card-body-cascade text-center">
                                <h5 class="mb-0">
                                    <i class="far fa-calendar-alt"></i> {{ $event->dateOnOff($event) }}<br>
                                    <i class="fas fa-map-marked-alt"></i> {{ $event->univer->$title }}, {{ $event->univer->city->$title }}, {{ $event->univer->city->country->$title }}
                                </h5>
                                <hr>
                                <h4 class="text-muted">{{ str_limit($event->$description, 100) }}</h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        @endif

    </div>


</section>
