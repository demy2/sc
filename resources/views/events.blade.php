@extends('index')

@section('content')
    <br><br><br>
    <div class="card border-primary deep-purple-text">
        <div class="card-header"><b>{{$category}} - Перелік наукових подій</b></div>
        <div class="card-body text-primary">
            <div class="container-fluid">
                <section class="content">

                    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" style="color: #4c00be">
                        <thead>
                        <tr class="text-center">
                            <th>№</th>
                            <th>Дата проведення</th>
                            <th>Назва конференції</th>
                            <th>Заклад вищої освіти (установа)</th>
                            <th>Контакти</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($events as $event)
                            <tr class="text-center">
                                <td class="align-middle">{{ $loop->iteration }}</td>
                                <td class="align-middle">{{ $event->dateOnOff($event) }}</td>
                                <td class="align-middle text-left">{{ $event->title_ua }}</td>
                                <td class="align-middle text-left">{{ $event->univer }}</td>
                                <td class="align-middle">
                                    @isset($event->FIO){{ $event->FIO }}<br>@endisset
                                    @isset($event->phone){{ $event->phone }}<br>@endisset
                                    @isset($event->email){{ $event->email }}@endisset
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="text-center">
                            <th>№</th>
                            <th>Дата проведення</th>
                            <th>Назва конференції</th>
                            <th>Заклад вищої освіти (установа)</th>
                            <th>Контакти</th>
                        </tr>
                        </tfoot>
                    </table>
                </section>
            </div>
        </div>
    </div>
@endsection
