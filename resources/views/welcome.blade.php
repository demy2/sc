<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sciendar</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="/css/bootstrap.css" rel="stylesheet">
        <!-- MDBootstrap -->
        <link href="/css/mdb.css" rel="stylesheet">
        <!-- MDBootstrap animations-extended -->
        <link href="/css/modules/animations-extended.css" rel="stylesheet" />

        <!-- Styles -->
        <style>
            /* Required for full background image */

            html,
            body,
            header,
            #intro-section {
                height: 100%;
                font-family: 'Poppins', sans-serif;
            }

            @media (max-width: 740px) {
                html,
                body,
                header,
                #intro-section {
                    height: 100vh;
                }
            }

            .rgba-gradient {
                background: rgba(35, 7, 77, 0.6);
                /* fallback for old browsers */
                background: -webkit-linear-gradient(to right, rgba(82, 20, 2, 0.6), rgba(35, 7, 77, 0.6));
                /* Chrome 10-25, Safari 5.1-6 */
                background: linear-gradient(to right, rgba(70, 15, 0, 0.6), rgba(35, 7, 77, 0.6));
                /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            }
        </style>
    </head>
    <body>
    <!-- Main navigation -->
    <header>
        <!-- Full Page Intro -->
        <div id="intro-section" class="view">

            <video class="video-intro" poster="https://mdbootstrap.com/img/Photos/Others/background.jpg" playsinline
                   autoplay muted loop>
                <source src="https://giant.gfycat.com/MiserlyFlawlessLeafhopper.webm" type="video/mp4">
            </video>
            <!-- Mask & flexbox options-->
            <div class="mask rgba-gradient d-flex justify-content-center align-items-center">
                <!-- Content -->
                <div class="container px-md-3 px-sm-0">
                    <!--Grid row-->
                    <div class="row wow fadeIn">
                        <!--Grid column-->
                        <div class="col-md-12 mb-4 white-text text-center wow fadeIn">
                            <h1 class="display-3 font-weight-bold white-text mb-0 pt-md-5 pt-5">ВІДКРИТТЯ</h1>
                            <hr class="hr-light my-4 w-75">
                            <h1 class="subtext-header font-weight-bold mt-2 mb-4">01 . 03 . 2019</h1>
                        </div>
                        <!--Grid column-->
                    </div>
                    <!--Grid row-->
                </div>
                <!-- Content -->
            </div>
            <!-- Mask & flexbox options-->
        </div>
        <!-- Full Page Intro -->
    </header>


    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="/js/popper.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="/js/mdb.js"></script>

    <script>
        // MDB Lightbox Init

        $(function () {
            $("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");
        });


        //Something is wrong with this script but only in snippet project
    </script>

    </body>
</html>
