<div class="modal fade" id="Modal_Logout" tabindex="-1" role="dialog" aria-labelledby="Modal_Logout" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <!-- Навигация -->
                <div class="form-group">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-block aqua-gradient">
                                    {{ __('lang.Logout') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
