@extends('index')

@section('content')
    <br><br>


    <div class="card border-primary deep-purple-text">
        <div class="card-header clearfix">
            <h1 class="text-uppercase float-left">{{ __('lang.archiveEvents') }}</h1>
            <h5 class="float-right">
                <a href="/" title="{{ __('lang.ActiveEvents') }}">
                    {{ __('lang.ActiveEvents') }}
                </a>
            </h5>
        </div>

        <div class="card-body text-primary">
            <div class="container-fluid">
                <section class="content">
                    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" style="color: #4c00be">
                        <thead>
                        <tr class="text-center">
                            <th>№</th>
                            <th>Дата проведення</th>
                            <th>Місце проведення</th>
                            <th>Назва події</th>
                            <th>Наукові напрямки</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($events as $event)
                                <tr class="text-center">
                                    <td class="align-middle">{{ $loop->iteration }}</td>
                                    <td class="align-middle text-left">{{ $event->dateOnOff($event) }}</td>
                                    <td class="align-middle text-left">{{ $event->univer->$title }}, {{ $event->city->$title }}, {{ $event->country->$title }}</td>
                                    <td class="align-middle text-left">
                                        <a href="{{ route('eventPart', $event->id) }}" class="text-primary">
                                            <span class="text-uppercase">{{ $event->$title }}</span><br>
                                            {{ $event->$description }}
                                        </a>
                                    </td>
                                    <td class="align-middle">
                                        @foreach($event->categories as $category)
                                            <a href="#" title="{{ $category->title_ua }}" style="color: rgb(76, 0, 190)">{!! $category->image !!}</a>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </div>
@endsection
