<?php

Route::group(['prefix' => App\Http\Middleware\LocaleMiddleware::getLocale()], function(){

    Route::get('/', 'IndexController@index')->name('index');
    //Route::get('/index', 'IndexController@index');
    Route::get('/info', 'IndexController@info');
    Route::get('/infoconf', 'IndexController@infoconf');

    // Pay
    Route::post( '/pay', ['uses'=>'PayController@index','as'=>'pay']);
    Route::post( '/payok', ['uses'=>'PayController@payOk','as'=>'payOk']);

    // works
    Route::get('/works', 'IndexController@works');

    Route::fallback(function () { return redirect('404error');});
    Route::view('/404error', '404error');


    Auth::routes(['verify' => true]);


    Route::get('/home', 'HomeController@index')->name('home');


    Route::get('/event', ['uses'=>'EventController@index','as'=>'event']);
    Route::get('/archive', ['uses'=>'EventController@archive','as'=>'archive']);
    Route::get('/chemistry', ['uses'=>'EventController@chemistry','as'=>'chemistry']);
    Route::get('/physics', ['uses'=>'EventController@physics','as'=>'physics']);
    Route::get('/maths', ['uses'=>'EventController@maths','as'=>'maths']);
    Route::get('/biology', ['uses'=>'EventController@biology','as'=>'biology']);
    Route::get('/economics', ['uses'=>'EventController@economics','as'=>'economics']);
    Route::get('/humanities', ['uses'=>'EventController@humanities','as'=>'humanities']);

    Route::match(['get','post'],'/event/{event}', ['uses'=>'EventController@show','as'=>'eventPart']);
    Route::get('/event/{event}/{ref_id}', 'RefController@refs');
    Route::get('/json-cities', 'User\UserInfoController@cities');
    Route::get('/json-university', 'User\UserInfoController@university');
    //Route::match(['get','post'], '/events1', ['uses' => 'Admin\EventsController@store1', 'as' => 'store1']);
    Route::get('/files/chem-2019/{file}', 'EventController@download')->name('downloadArc');


    Route::group(['middleware' => ['auth'], 'prefix'  =>  'user', 'as' => 'user.', 'namespace'    =>  'User'],
        function (){
            Route::get('/', 'UserController@index');
            Route::get('/events', ['uses' => 'UserController@events', 'as' => 'userEvents']);
            Route::get('/myref', ['uses' => 'UserController@myref', 'as' => 'myref']);
            Route::resource('/addwork', 'AddWorkController');
            Route::post('/events/store', 'UserController@store')->name('events.store');
            Route::resource('/info', 'UserInfoController');
            Route::resource('/info/univer', 'UniverInfoController');
            //Route::match(['get','post'],'/event/{events}/section/{section}', 'SectionController@index');
        });


    Route::group(['middleware' => ['auth', 'verified', 'admin'], 'prefix'  =>  'admin', 'as' => 'admin.', 'namespace'    =>  'Admin'],
        function (){
            Route::get('/', 'DashboardController@index');
            Route::resource('/users', 'UsersController');
            Route::resource('/events', 'EventsController');
            Route::resource('/price', 'PriceController');
            Route::post('/events/napochtu', 'EventsController@napochtu')->name('napochtu');
            Route::post('/events/statusset', 'EventsController@statusset')->name('statusset');
            Route::get('/events/download/{id}', 'EventsController@download')->name('download');
            Route::match(['get','post'],'/events/create/{id}', 'EventsController@step');
            Route::match(['get','post'],'/events/{events}/section/{section}', 'SectionController@index')->name('section');
        });


    Route::group(['middleware' => ['auth', 'verified', 'admin', 'general'], 'prefix'  =>  'general', 'as' => 'general.', 'namespace'    =>  'General'],
        function (){
            Route::get('/', 'DashboardController@index');
            Route::get('/info', ['uses' => 'InfoController@index', 'as' => 'info']);
            Route::get('/mbusers', ['uses' => 'MBUsersController@index', 'as' => 'mbusers']);
            Route::resource('/categories', 'CategoriesController');
            Route::resource('/degrees', 'DegreeController');
            Route::resource('/positions', 'PositionController');
            Route::resource('/events', 'EventsController');
            Route::resource('/country', 'CountryController');
            Route::resource('/country/{country}/city', 'CityController');
            Route::resource('/country/{country}/city/{city}/univers', 'UniversController');
        });

});





//Переключение языков
Route::get('setlocale/{lang}', function ($lang) {

    $referer = Redirect::back()->getTargetUrl(); //URL предыдущей страницы
    $parse_url = parse_url($referer, PHP_URL_PATH); //URI предыдущей страницы

    //разбиваем на массив по разделителю
    $segments = explode('/', $parse_url);

    //Если URL (где нажали на переключение языка) содержал корректную метку языка
    if (in_array($segments[1], App\Http\Middleware\LocaleMiddleware::$languages)) {

        unset($segments[1]); //удаляем метку
    }

    //Добавляем метку языка в URL (если выбран не язык по-умолчанию)
    if ($lang != App\Http\Middleware\LocaleMiddleware::$mainLanguage){
        array_splice($segments, 1, 0, $lang);
    }

    //формируем полный URL
    $url = Request::root().implode("/", $segments);

    //если были еще GET-параметры - добавляем их
    if(parse_url($referer, PHP_URL_QUERY)){
        $url = $url.'?'. parse_url($referer, PHP_URL_QUERY);
    }
    return redirect($url); //Перенаправляем назад на ту же страницу

})->name('setlocale');
