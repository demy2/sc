<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventAllsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_alls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->unsigned();
            $table->foreign('admin_id')->references('id')->on('users');
            $table->boolean('status')->default(0);
            $table->date('date_open'); // Дата начала конференции
            $table->date('date_close'); // Дата окончания конференции
            $table->date('date_start'); // Дата запуска регистрации
            $table->date('date_stop'); // Дата завершения регистрации
            $table->string('image');

            $table->string('title_ua');
            $table->string('short_title_ua');
            $table->string('slug_ua');
            $table->string('description_ua');
            $table->string('target_ua');
            $table->text('other_ua');
            $table->text('design_rules_ua');
            $table->text('orgcom_ua');

            $table->string('title_ru');
            $table->string('short_title_ru');
            $table->string('slug_ru');
            $table->string('description_ru');
            $table->string('target_ru');
            $table->text('other_ru');
            $table->text('design_rules_ru');
            $table->text('orgcom_ru');

            $table->string('title_en');
            $table->string('short_title_en');
            $table->string('slug_en');
            $table->string('description_en');
            $table->string('target_en');
            $table->text('other_en');
            $table->text('design_rules_en');
            $table->text('orgcom_en');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_alls');
    }
}
