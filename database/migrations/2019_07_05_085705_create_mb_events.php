<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMbEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mb_events', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date_open'); // Дата начала конференции
            $table->date('date_close'); // Дата окончания конференции

            $table->string('title_ua');
            $table->string('description_ua');
            $table->string('city');
            $table->string('univer');
            $table->string('FIO')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->boolean('isChemistry')->nullable(0);
            $table->boolean('isPhysics')->nullable(0);
            $table->boolean('isBiology')->nullable(0);
            $table->boolean('isMathematics')->nullable(0);
            $table->boolean('isEconomics')->nullable(0);
            $table->boolean('isHumanities')->nullable(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mb_events');
    }
}
