<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->unique();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->integer('univer_id')->unsigned();
            $table->foreign('univer_id')->references('id')->on('universities');

            $table->string('surname');
            $table->string('name');
            $table->string('middle_name');
            $table->string('phone');
            $table->boolean('gender');
            $table->string('avatar')->nullable($value = true);
            $table->date('date_of_birth');
            $table->integer('degree_id')->unsigned();
            $table->foreign('degree_id')->references('id')->on('degrees');
            $table->integer('position_id')->unsigned();
            $table->foreign('position_id')->references('id')->on('positions');

            $table->string('surnameNP')->nullable();//new
            $table->string('nameNP')->nullable();//new
            $table->string('middle_nameNP')->nullable();//new
            $table->integer('city_idNP')->nullable();//new
            $table->string('phoneNP')->nullable();//new
            $table->integer('oNP')->nullable();//new
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_infos');
    }
}
