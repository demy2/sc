<?php

use Illuminate\Database\Seeder;
use App\Position;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Position::create([
            'title_ua'  =>  'Студент',
            'title_ru'  =>  'Студент',
            'title_en'  =>  'Student'
        ]);

        Position::create([
            'title_ua'  =>  'Аспірант',
            'title_ru'  =>  'Аспирант',
            'title_en'  =>  'Graduate student'
        ]);

        Position::create([
            'title_ua'  =>  'Асистент',
            'title_ru'  =>  'Ассистент',
            'title_en'  =>  'Assistant'
        ]);

        Position::create([
            'title_ua'  =>  'Доцент',
            'title_ru'  =>  'Доцент',
            'title_en'  =>  'Assistant professor'
        ]);

        Position::create([
            'title_ua'  =>  'Професор',
            'title_ru'  =>  'Профессор',
            'title_en'  =>  'Professor'
        ]);
    }
}
