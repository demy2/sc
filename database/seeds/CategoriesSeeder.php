<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
                'image'  =>  '<i class="fas fa-flask fa-3x"></i>',
                'title_ua'  =>  'Хімія',
                'title_ru'  =>  'Химия',
                'title_en'  =>  'Chemistry'
        ]);

        Category::create([
                'image'  =>  '<i class="fab fa-react fa-3x"></i>',
                'title_ua'  =>  'Фізика',
                'title_ru'  =>  'Физика',
                'title_en'  =>  'Physics'
        ]);

        Category::create([
                'image'  =>  '<i class="fas fa-dna fa-3x"></i>',
                'title_ua'  =>  'Біологія',
                'title_ru'  =>  'Биология',
                'title_en'  =>  'Biology'
        ]);

        Category::create([
                'image'  =>  '<i class="fas fa-square-root-alt fa-3x"></i>',
                'title_ua'  =>  'Математика',
                'title_ru'  =>  'Математика',
                'title_en'  =>  'Mathematics'
        ]);

        Category::create([
                'image'  =>  '<i class="fas fa-dollar-sign fa-3x"></i>',
                'title_ua'  =>  'Економіка',
                'title_ru'  =>  'Экономика',
                'title_en'  =>  'Economics'
        ]);

        Category::create([
                'image'  =>  '<i class="fas fa-street-view fa-3x"></i>',
                'title_ua'  =>  'Гуманітарні науки',
                'title_ru'  =>  'Гуманитарные науки',
                'title_en'  =>  'Humanities'
        ]);
    }
}
