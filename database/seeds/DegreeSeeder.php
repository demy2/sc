<?php

use Illuminate\Database\Seeder;
use App\Degree;

class DegreeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Degree::create([
            'title_ua'  =>  'Бакалавр',
            'title_ru'  =>  'Бакалавр',
            'title_en'  =>  'Bachelor'
        ]);

        Degree::create([
            'title_ua'  =>  'Спеціаліст',
            'title_ru'  =>  'Специалист',
            'title_en'  =>  'Specialist'
        ]);

        Degree::create([
            'title_ua'  =>  'Магістр',
            'title_ru'  =>  'Магистр',
            'title_en'  =>  'Master'
        ]);

        Degree::create([
            'title_ua'  =>  'Кандидат наук',
            'title_ru'  =>  'Кандидат наук',
            'title_en'  =>  'PhD'
        ]);

        Degree::create([
            'title_ua'  =>  'Доктор наук',
            'title_ru'  =>  'Доктор наук',
            'title_en'  =>  'Ph.D'
        ]);
    }
}
