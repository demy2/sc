// рефссылка - копирование
function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

// рефссылка - инфо при наведении
$('[data-toggle="popover-hover"]').popover({
    html: true,
    trigger: 'hover',
    placement: 'top',
});

///////////////////////////////////////
// datepicker



//////////////////////////////////
<!-- Select2 from -->
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });

///////////////////////////////////////////
// ckeditor
$(document).ready(function(){
    CKEDITOR.config.toolbar = [ // Настройки тулбара редактора

        { name: 'clipboard', items: [

                'Paste', '-', //'Copy',
                'Undo', 'Redo', '-',
                'Bold', 'Italic', '-',
                'Subscript', 'Superscript', '-',//????
                'JustifyLeft', 'JustifyCenter','JustifyRight','JustifyBlock','-',//???
                'NumberedList', 'BulletedList', 'Outdent', 'Indent','-',
                'Link', 'Unlink', '-',
                //'TextColor','BGColor',//????
                'Image', 'Table', 'HorizontalRule', 'SpecialChar', '-',
                'Maximize'
            ] },

        // '/',

        //{ name: 'my_styles', items: [
        //        'Bold', 'Italic', '-',
        //    ] }
    ];
    var editor = CKEDITOR.replaceAll();
    CKFinder.setupCKEditor( editor );
})

/////////////////////////////////////////
// dtBasicExample
$(document).ready(function() {
    $("#dtBasicExample").DataTable({
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        buttons: [
            'copy', 'excel', 'pdf', 'print', 'colvis', 'pageLength' //'csv',
        ]
    });

    $(".dataTables_length").addClass("bs-select");
});
