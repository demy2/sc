<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{


    protected $fillable = [
        'event_id',
        'section_ua',
        'section_ru',
        'section_en'];

    public function event()
    {
        return $this->hasMany(Event::class); // обращение к событию
    }

    public function admin()
    {
        return $this->hasOne(UserInfo::class, 'user_id',  'admin_id');
    }

    public function FIOsectionAdmin($id)
    {
        $section = Section::where('id',$id)->pluck('admin_id');
        $admin = UserInfo::where('user_id', $section)->first();
        $surname = $admin['surname'];
        $name = $admin['name'];
        $middle_name = $admin['middle_name'];
        //dd($name);
        $fio = "$surname".' '."$name".' '."$middle_name";
        return $fio;
    }

    public function userCount($id)
    {
        $count = Participant::where('section_id', $id)->count();

        return $count;
    }
}
