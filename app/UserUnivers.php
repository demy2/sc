<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserUnivers extends Model
{
    protected $fillable = [
        'user_id',
        'country',
        "city",
        "univer",
        "website",
    ];

    public static function add($fields) // Добавление информации
    {
        $userUniver = new static;
        $userUniver->fill($fields);
        $userUniver->save();

        return $userUniver;
    }
}
