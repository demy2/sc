<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Translate extends Model
{
    public $title;

    public function __construct($title)
    {
        $this->title = $title;
    }

    public function lang()
    {
        if(App::isLocale('ua')){$title = "$this->title".'_ua';}
        if(App::isLocale('ru')){$title = "$this->title".'_ru';}
        if(App::isLocale('en')){$title = "$this->title".'_en';}

        return $title;
    }
}
