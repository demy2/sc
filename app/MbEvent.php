<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MbEvent extends Model
{
    protected $fillable = [
        'date_open', 'date_close',

        'title_ua', 'univer',
        'FIO', 'phone', 'email',
        'isChemistry', 'isPhysics', 'isBiology', 'isMathematics', 'isEconomics', 'isHumanities',
    ];

    public static function add($fields) // Добавление события
    {
        $event = new static;
        $event->fill($fields);
        $event->save();

        return $event;
    }

    public function edit($fields) // Изменение события
    {
        $this->fill($fields);
        $this->save();
    }

    public function dateOnOff($event)
    {
        $d1 = explode("-", $event->date_open);
        $d2 = explode("-", $event->date_close);
        if($d1[0] == 3000)
        {
            $data = "nonstop";
        }
        else
        {
            $data = $d1[2].'-'.$d2[2].'.'.$d1[1].'.'.$d1[0];
        }
        return $data;
    }

}
