<?php

namespace App;

use Carbon\Carbon;
use App\Category;
use App\Section;
//use App\Participant;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Storage;
use App\Translate;

class Event extends Model
{
    use Sluggable;

    protected $fillable = [
        'date_open', 'date_close', 'date_start', 'date_stop',

        'image',

        'title_ua', 'short_title_ua', 'description_ua', 'target_ua', 'other_ua', 'pay_ua', 'design_rules_ua', 'orgcom_ua', 'docs_ua',

        'title_ru', 'short_title_ru', 'description_ru', 'target_ru', 'other_ru', 'pay_ru', 'design_rules_ru', 'orgcom_ru', 'docs_ru',

        'title_en', 'short_title_en', 'description_en', 'target_en', 'other_en', 'design_rules_en', 'orgcom_en', 'docs_en', 'position1', 'position2', 'position3', 'position4','position5',

        'country', 'city', 'univer',
        'date_pay', 'position', 'part_form',
        'price', 'currency', 'need_book', 'price_book', 'currency_book',
        'price_en', 'currency_en', 'need_book_en', 'price_book_en', 'currency_book_en',
        'price_translate', 'currency_translate', 'price_certificate', 'currency_certificate',

        'responsible', 'phone', 'e-mail',
    ];


    public function eventInfoUa()
    {
        return $this->hasOne(Event_ua::class);
    }

    public function eventInfoRu()
    {
        return $this->hasOne(Event_ru::class);
    }

    public function eventInfoEn()
    {
        return $this->hasOne(Event_en::class);
    }



    public function univer()
    {
        return $this->belongsTo(University::class, 'universities_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'cities_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'countries_id', 'id');
    }



    public function section()
    {
        return $this->hasMany(Section::class); // обращение ко многим секциям
    }

    public function sections($id)
    {
        $sections = Section::where('event_id',$id)->get();
        return $sections;
    }



    public function author()
    {
        return $this->hasOne(User::class); // обращение к автору события
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }



    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug_ua' => [
                'source' => 'short_title_ua'
            ],
            'slug_ru' => [
                'source' => 'short_title_ru'
            ],
            'slug_en' => [
                'source' => 'short_title_en'
            ]
        ];
    }

    public function dateOnOff($event)
    {
        $d1 = explode("-", $event->date_open);
        $d2 = explode("-", $event->date_close);
        if($d1[0] == 3000)
        {
            $data = "nonstop";
        }
        else
        {
            $data = $d1[2].'-'.$d2[2].'.'.$d1[1].'.'.$d1[0];
        }
        return $data;
    }

    public static function add($fields) // Добавление события
    {
        $event = new static;
        $event->fill($fields);
        $id = Auth::user()->id;
        $info = UserInfo::where('user_id', $id)->first();
        $event->admin_id = $id;
        $event->universities_id = $info->univer_id;
        $event->countries_id = $info->country_id;
        $event->save();

        return $event;
    }

    public function edit($fields) // Изменение события
    {
        $this->fill($fields);
        $this->save();
    }

    public function removeImage()
    {
        if($this->image != null)
        {
            Storage::delete('uploads/' . $this->image);
        }
    }

    public function uploadImage($image)
    {
        if($image == null) { return; }

        $this->removeImage();
        $filename = str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads', $filename);
        $this->image = $filename;
        $this->save();
    }

    public function getImage() // вывод картинки
    {
        if($this->image == null)
        {
            return '/img/no-image.png';
        }

        return '/uploads/' . $this->image;
    }

    public function setSection($id)
    {
        if($id == null) { return; }

        //dd($id);
        $this->section_id = $id;
        $this->save();
    }

    public function setCategory($ids)
    {
        if($ids == null) { return; }

        $this->categories()->sync($ids);
    }

    public function getCategory() //?????????????
    {
        $title = (new Translate('title'))->lang();

        return implode(", ", $this->categories->pluck($title)->all());
    }

    public function getCategoryImage()
    {
        return implode(" ", $this->categories->pluck('image')->all());
    }

    public function getCategoryTitle()
    {
        return implode(" ", $this->categories->pluck('title_en')->all());
    }

    public function countUserEvent($event)
    {
        return Participant::where('event_id', $event->id)->count();
    }












    public function setDraft()
    {
        $this->status = 0;
        $this->save();
    }

    public function setPublic()
    {
        $this->status = 1;
        $this->save();
    }

    public function toggleStatus($value)
    {
        if($value == null)
        {
            return $this->setDraft();
        }

        return $this->setPublic();
    }

    public function setDateAttribute($value) // Не работает формат даты :(
    {
        //$date = Carbon::createFromFormat('d-m-y', $value)->format('Y-m-d');

        //$this->attributes['date_open'] = $date;
    }



}
