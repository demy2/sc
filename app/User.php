<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *добавляем реф
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'ref_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    ////////////////////////////////////////////

    public static function add($fields) // ???? добавление пользователя
    {
        $user = new static;
        $user->fill($fields);
        $user->password = bcrypt($fields['password']);
        $user->save();

        return $user;
    }

    public function edit($fields) // ???? изменение пользователя
    {
        $this->fill($fields);
        $this->password = bcrypt($fields['password']);
        $this->save();
    }



    public function info()
    {
        return $this->hasOne(UserInfo::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'categories_users', 'user_id', 'category_id');
    }


    public function setCategory($ids)
    {
        if($ids == null) { return; }

        $this->categories()->sync($ids);
    }


}
