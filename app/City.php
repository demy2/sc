<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['country_id', 'title_ua','title_ru','title_en'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function university()
    {
        return $this->hasMany(University::class);
    }

    public function add($fields) // Добавление города
    {
        $city = new static;
        $city->fill($fields);
        $city->save();

        return $city;
    }

    public function edit($fields) // Изменение города
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove() // Удаление города
    {
        $this->delete();
    }
}
