<?php

namespace App;

use Carbon\Carbon;
use App\Category;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Storage;

class EventAll extends Model
{
    use Sluggable;

    protected $fillable = [
        'date_open',
        'date_close',
        'date_start',
        'date_stop',

        'image',

        'title_ua',
        'short_title_ua',
        'description_ua',
        'target_ua',
        'other_ua',
        'design_rules_ua',
        'orgcom_ua',

        'title_ru',
        'short_title_ru',
        'description_ru',
        'target_ru',
        'other_ru',
        'design_rules_ru',
        'orgcom_ru',

        'title_en',
        'short_title_en',
        'description_en',
        'target_en',
        'other_en',
        'design_rules_en',
        'orgcom_en'];



    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug_ua' => [
                'source' => 'short_title_ua'
            ],
            'slug_ru' => [
                'source' => 'short_title_ru'
            ],
            'slug_en' => [
                'source' => 'short_title_en'
            ]
        ];
    }

    public static function add($fields) // Добавление события
    {
        $event = new static;
        $event->fill($fields);
        $event->admin_id = 1; /////!!!!!!!!
        $event->save();

        return $event;
    }

    public function edit($fields) // Изменение события
    {
        $this->fill($fields);
        $this->save();
    }

    public function removeImage()
    {
        if($this->image != null)
        {
            Storage::delete('uploads/' . $this->image);
        }
    }

    public function uploadImage($image)
    {
        if($image == null) { return; }

        $this->removeImage();
        $filename = str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads', $filename);
        $this->image = $filename;
        $this->save();
    }

    public function getImage() // вывод картинки
    {
        if($this->image == null)
        {
            return '/img/no-image.png';
        }

        return '/uploads/' . $this->image;
    }

    public function setSection($id)
    {
        if($id == null) { return; }

        $this->section_id = $id;
        $this->save();
    }

    public function setCategory($ids)
    {
        if($ids == null) { return; }

        $this->categories()->sync($ids);
    }

    public function getCategoryTitle()
    {
        return implode(", ", $this->categories->pluck('title_ua')->all());
    }

    public function setDraft()
    {
        $this->status = 0;
        $this->save();
    }

    public function setPublic()
    {
        $this->status = 1;
        $this->save();
    }

    public function toggleStatus($value)
    {
        if($value == null)
        {
            return $this->setDraft();
        }

        return $this->setPublic();
    }

    public function setDateAttribute($value) // Не работает формат даты :(
    {
        //$date = Carbon::createFromFormat('d-m-y', $value)->format('Y-m-d');

        //$this->attributes['date_open'] = $date;
    }



}
