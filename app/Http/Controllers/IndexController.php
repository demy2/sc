<?php

namespace App\Http\Controllers;

use App\Category;
use App\Event;
use App\MbEvent;
use App\UserInfo;
use App\Country;
use App\City;
use App\University;
use App\Translate;

use App\Participant;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = (new Translate('title'))->lang();
        $description = (new Translate('description'))->lang();
        $lng = (new Translate(''))->lang();

        $events = Event::where('status', 1)->where('date_start','!=', null)->get();
        $categories = Category::all();

        $countChemistry = MbEvent::where('isChemistry',1)->count();
        $countPhysics = MbEvent::where('isPhysics',1)->count();
        $countBiology = MbEvent::where('isBiology',1)->count();
        $countMathematics = MbEvent::where('isMathematics',1)->count();
        $countEconomics = MbEvent::where('isEconomics',1)->count();
        $countHumanities = MbEvent::where('isHumanities',1)->count();


        return view('content', compact('events', 'categories', 'title', 'description', 'countChemistry', 'countPhysics', 'countBiology', 'countMathematics', 'countEconomics', 'countHumanities'));
    }

    public function info()
    {
        return view('info');
    }

    public function infoconf()
    {
        $title = (new Translate('title'))->lang();
        $status = (new Translate('status'))->lang();

        $eventTitle = Event::find(1)->$title;
        //dd($events);
        //$categories = Category::pluck('title_ua', 'id')->all();
        $participants = Participant::where('event_id', 1)->get();
        $N_user = $participants->unique('user_id')->count('user_id');
        $N_work = $participants->count('name');
        $N_book = $participants->sum('Nbook');


        return view('infoconf', compact('eventTitle', 'title', 'status','participants', 'N_user', 'N_work', 'N_book'));
    }
}
