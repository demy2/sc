<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserUnivers;

class UniverInfoController extends Controller
{
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'country' =>  'required|max:255',
            'city' => 'required|max:255',
            'univer' => 'required|max:255',
            'website' => 'required|url|max:255',

        ]);

        UserUnivers::add($request->all());

        return redirect()->route('user.info.index');
    }
}
