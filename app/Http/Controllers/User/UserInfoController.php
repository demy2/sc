<?php

namespace App\Http\Controllers\User;

use App\Translate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country;
use App\Category;
use App\City;
use App\University;
use App\User;
use App\UserInfo;
use App\UserUnivers;
use App\Degree;
use App\Position;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class UserInfoController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = (new Translate('title'))->lang();

        if(UserInfo::where('user_id',Auth::user()->id)->first() == null) // Если данных о юзере нет --> направляем заполнять
        {
            return redirect()->route('user.info.create');
        }

        $id = Auth::user()->id;
        $user = User::find($id);
        $userInfo = UserInfo::where('user_id', $id)->first();

        if( $userInfo->country_id == 999999 or $userInfo->city_id == 999999 or $userInfo->univer_id == 999999 )
        {
            if(UserUnivers::where('user_id', $id)->first() != null)
            {
                return redirect()->route('user.userEvents');
            }

            return view('/user/info/univer');
        }


        $categories = Category::pluck($title, 'id')->all();
        $countries = Country::pluck($title, 'id')->all();
        $degrees = Degree::pluck($title, 'id')->all();
        $positions = Position::pluck($title, 'id')->all();
        //$userCat = $userInfo->categories->pluck('id')->all();

        $userCat = $user->categories($id)->get();
        //dd($userCat);
        $category = University::find($userInfo->univer_id); /// ???????????????????????????????
        $univerUser = University::find($userInfo->univer_id);
        $cityUser = City::find($userInfo->city_id)->$title;
        $countryUser = Country::find($userInfo->country_id);
        $degree = Degree::find($userInfo->degree_id);
        $position = Position::find($userInfo->position_id);
        //new
        $cities = City::pluck($title, 'id')->all();
        if ($userInfo->city_idNP !== NULL) {
            $npCity = $userInfo->city_idNP;

        }else{
            $npCity = $cityUser;
        }



        return view('/user/info/index', compact('title','user', 'userInfo', 'categories', 'countries', 'degrees', 'positions', 'userCat', 'univerUser', 'cityUser', 'countryUser', 'degree', 'position','cities','npCity'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = (new Translate('title'))->lang();

        $id = Auth::user()->id;
        $user = User::find($id);

        $categories = Category::pluck($title, 'id')->all();
        $countries = Country::pluck($title, 'id')->all();
        $degrees = Degree::pluck($title, 'id')->all();
        $positions = Position::pluck($title, 'id')->all();

        return view('/user/info/create', compact('title', 'user', 'categories', 'countries', 'degrees', 'positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'avatar' =>  'nullable|image',
            'gender' => 'required',
            'surname' => 'required',
            'name' => 'required',
            'middle_name' => 'required',
            'date_of_birth' => 'required|date',
            'phone' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'univer_id' => 'required',
            'degree_id' => 'required',
            'position_id' => 'required',
        ]);

        $userInfo = UserInfo::add($request->all());
        $user = User::find(Auth::user()->id);
        $userInfo->uploadAvatar($request->file('avatar'));
        $user->setCategory($request->get('categories'));

        return redirect()->route('user.userEvents')->with('message','hii there');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('user.info.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($request->has('subNP')){

            $userInfo = UserInfo::where('user_id', $id)->first();
            $userInfo->edit($request->all());
            return back()->with('message','зміни внесено');
        }elseif($request->has('subUI')){

            $this->validate($request, [
                'avatar' =>  'nullable|image',
                'gender' => 'required',
                'surname' => 'required',
                'name' => 'required',
                'middle_name' => 'required',
                'date_of_birth' => 'required',
                'phone' => 'required',
                //'univer_id' => 'required',
                //'degree_id' => 'required',
                //'position_id' => 'required',
            ]);

            //$countries = Country::pluck('title_ua', 'id')->all();
            $userInfo = UserInfo::where('user_id', $id)->first();
            $userInfo->edit($request->all());
            $userInfo->uploadAvatar($request->file('avatar'));
            //$userInfo->user_id = $request->user()->id;
            $userInfo->user->setCategory($request->get('categories'));

            return redirect()->route('user.userEvents')->with('message','hii there');
        }

    }

    public function cities(){
        $title = (new Translate('title'))->lang();

        $country_id = Input::get('country_id');
        $cities = City::where('country_id','=', $country_id)->orderBy($title)->get();
        return response()->json($cities);
    }

    public function university(){
        $title = (new Translate('title'))->lang();

        $cities_id = Input::get('cities_id');
        $universities = University::where('cities_id','=', $cities_id)->orderBy($title)->get();
        return response()->json($universities);
    }
}
