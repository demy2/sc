<?php

namespace App\Http\Controllers\User;

use App\Category;
use App\Event;
use App\Participant;
use App\Position;
use App\Price;
use App\UserInfo;
use App\UserUnivers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\User;
use Config;
use App\Translate;

class UserController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
        $userInfo = UserInfo::where('user_id', $id)->first();

        if(UserInfo::where('user_id', $id)->first() == null) // Если данных о юзере нет --> направляем заполнять
        {
            return redirect()->route('user.info.create');
        }

        if( $userInfo->country_id == 999999 or $userInfo->city_id == 999999 or $userInfo->univer_id == 999999 )
        {
            if(UserUnivers::where('user_id', $id)->first() != null)
            {
                return redirect()->route('user.userEvents');
            }

            return view('/user/info/univer');
        }

        return redirect()->route('user.userEvents'); // user/info
    }

    public function myref()
    {
        $refs = User::where('ref_id', Auth::user()->id)->get();
        //$count = $refs->count('id');

        return view('user/myref', compact('refs'));
    }

    public function events()
    {

        $title = (new Translate('title'))->lang();
        $section = (new Translate('section'))->lang();
        $description = (new Translate('description'))->lang();

        if(isset(Auth::user()->info->id))
        {
            if (count(Participant::where('user_id', Auth::user()->id)->get())>0)
            {
                $eventDate = Event::where('status','!=',0)
                    ->where('date_close','>',now())
                    ->pluck('id');
                $participants = Participant::where('user_id', Auth::user()->id)
                    ->whereIn('event_id', $eventDate)
                    ->get();

                $oldEventDate = Event::where('status','!=',0)
                    ->where('date_close','<',now())
                    ->pluck('id');
                $oldParticipants = Participant::where('user_id', Auth::user()->id)
                    ->whereIn('event_id', $oldEventDate)
                    ->get();
            }
            else
            {
                $participants = [];
                $oldParticipants = [];
            }

            $categories = Auth::user()->categories()->pluck('category_id');
            foreach ($categories as $category)
            {
                $categories = Category::find($category);
                $actualeEvent = $categories->events;
                $actualeEvents[] = $actualeEvent;
            }

            $actualeEvents = collect($actualeEvents)->collapse()->unique()->where('status','!=',0)->where('date_stop','>',now())->all();

            $actualeEventsCount = count($actualeEvents);


            return view('user.events', compact('participants', 'actualeEventsCount', 'oldParticipants', 'actualeEvents', 'title', 'section', 'description'));
        }

        return redirect('user/info');
    }

    public function store(Request $request)
    {
        $this->validate($request,
        [
            'work' => 'mimes:doc,docx',
        ]);

        //$status = Config::get('statusy.status2');

        $id = $request->id;
        $path = $request->file('work')->store('works');
        $participant = Participant::find($id);
        $participant->status = 2;
        $participant->file = $path;
        $participant->save();


        return redirect('/user/events')->with('file','ok');
    }
}
