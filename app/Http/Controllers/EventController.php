<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Event;
use App\Category;
use App\MbEvent;
use App\ParticipantForm;
use App\Section;
use App\University;
use App\User;
use App\UserInfo;
use Illuminate\Http\Request;
use Validator;
use App\Participant;
use Cookie;
use Redirect;
use Illuminate\Support\Facades\App;
use App\Translate;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = (new Translate('title'))->lang();

        $events = Event::where('status', 1)->get();
        $categories = Category::all();

        return view('content', compact('events','categories', 'title'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event, Request $request)
    {
        $title = (new Translate('title'))->lang();
        $description = (new Translate('description'))->lang();
        $target = (new Translate('target'))->lang();
        $other = (new Translate('other'))->lang();
        $design_rules = (new Translate('design_rules'))->lang();
        $pay = (new Translate('pay'))->lang();
        $orgcom = (new Translate('orgcom'))->lang();
        $docs = (new Translate('docs'))->lang();
        $short_title = (new Translate('short_title'))->lang();
        $section_title = (new Translate('section'))->lang();

        $style = 'deep-purple-text text-justify h6-responsive';


        if ($request->isMethod('post')) {
            $data = $request->except('_token');

            $validator = Validator::make($data,
                [
                    'name' => 'required|max:255'
                ]
            );
            if ($validator->fails()) {
                return redirect()->route('eventPart')->withErrors($validator)->withInput();
            }
            $participant = new Participant();
            $participant->fill($data);

            if($participant->save()){

                return redirect()->route('user.userEvents');
            }
        }

        $user = User::where('id', $event->admin_id)->first();
        $userInfo = UserInfo::where('user_id', $user->id)->first();
        $univer = University::where('id', $userInfo->univer_id)->first();
        $city = City::where('id', $userInfo->city_id)->first();
        $country = Country::where('id', $userInfo->country_id)->first();

        $categories = Category::all();
        $forms = ParticipantForm::all();
        $sections = Section::where('event_id', $event->id)->get();

        if(strtotime($event->date_close) < strtotime(date('Y-m-d')))
        {
            return view('report', compact('event', 'title', 'univer', 'short_title', 'city', 'country'));
        }
        return view('event', compact('event','categories', 'forms', 'sections', 'title', 'description',
            'target', 'other', 'design_rules', 'pay', 'orgcom', 'docs', 'style', 'user', 'userInfo', 'univer', 'short_title', 'section_title', 'city', 'country'));
    }

    public function chemistry()
    {
        $events = MbEvent::where('isChemistry', 1)->get();
        $category = 'ХІМІЯ';

        return view('events', compact('events', 'category'));
    }

    public function physics()
    {
        $events = MbEvent::where('isPhysics', 1)->get();
        $category = 'ФІЗИКА';

        return view('events', compact('events', 'category'));
    }

    public function maths()
    {
        $events = MbEvent::where('isMathematics', 1)->get();
        $category = 'МАТЕМАТИКА';

        return view('events', compact('events', 'category'));
    }

    public function biology()
    {
        $events = MbEvent::where('isBiology', 1)->get();
        $category = 'БІОЛОГІЯ';

        return view('events', compact('events', 'category'));
    }

    public function economics()
    {
        $events = MbEvent::where('isEconomics', 1)->get();
        $category = 'ЕКОНОМІКА';

        return view('events', compact('events', 'category'));
    }

    public function humanities()
    {
        $events = MbEvent::where('isHumanities', 1)->get();
        $category = 'ГУМАНІТАРНІ НАУКИ';

        return view('events', compact('events', 'category'));
    }

    public function archive()
    {
        $title = (new Translate('title'))->lang();
        $description = (new Translate('description'))->lang();

        $events = Event::where('status', 2)->where('date_close','<',date('Y-m-d'))->select('id','universities_id','cities_id', 'countries_id','title_ua','description_ua','title_ru','description_ru','title_en','description_en','date_open','date_close')->get();

        return view('archive', compact('events','title','description'));
    }

}
