<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City;
use App\Country;
use App\University;
use Illuminate\Validation\Rule;

class UniversController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($country, $city)
    {
        $countries = Country::find($country);
        $cities = City::find($city);
        $universities = University::where('cities_id', $city)->get();
        $i = 0;
        return view('general.country.indexUnivers', [
            'countries'  =>  $countries,
            'cities'  =>  $cities,
            'universities'  =>  $universities,
            'i'   =>  $i
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($country, $city)
    {
        $countries = Country::find($country);
        $cities = City::find($city);
        return view('general.country.createUnivers', ['countries'  =>  $countries, 'cities'    =>  $cities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $country, $city)
    {

        $this->validate($request, [
            'title_ua' => 'required|unique:universities',
            'title_ru' => 'required|unique:universities',
            'title_en' => 'required|unique:universities',
            'short_title_ua' => 'required|unique:universities',
            'short_title_ru' => 'required|unique:universities',
            'short_title_en' => 'required|unique:universities',
            'url' => 'required|unique:universities',
            'email' => 'required|unique:universities',
        ]);

        University::create($request->all());
        return redirect()->route('general.univers.index', [$country, $city]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($country, $city, $id)
    {
        $countries = Country::find($country);
        $cities = City::find($city);
        $universities = University::find($id);

        return view('general.country.editUnivers', ['countries'=>$countries, 'cities'=>$cities, 'universities'=>$universities]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $country, $city, $id)
    {
        $universities = University::find($id);

        $this->validate($request, [
            'title_ua' => ['required',Rule::unique('universities')->ignore($universities->id)],
            'title_ru' => ['required',Rule::unique('universities')->ignore($universities->id)],
            'title_en' => ['required',Rule::unique('universities')->ignore($universities->id)],
            'short_title_ua' => ['required',Rule::unique('universities')->ignore($universities->id)],
            'short_title_ru' => ['required',Rule::unique('universities')->ignore($universities->id)],
            'short_title_en' => ['required',Rule::unique('universities')->ignore($universities->id)],
            'url' => ['required',Rule::unique('universities')->ignore($universities->id)],
            'email' => ['required',Rule::unique('universities')->ignore($universities->id)],
        ]);

        $universities->update($request->all());

        return redirect()->route('general.univers.index', [$country, $city]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($country, $city, $id)
    {
        University::find($id)->delete();
        return redirect()->route('general.univers.index', [$country, $city]);
    }
}
