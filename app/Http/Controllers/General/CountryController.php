<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country;
use Illuminate\Validation\Rule;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();
        $i = 0;
        return view('general.country.indexCountry', [
            'countries'  =>  $countries,
            'i'   =>  $i
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('general.country.createCountry');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title_ua' => 'required|unique:countries',
            'title_ru' => 'required|unique:countries',
            'title_en' => 'required|unique:countries'
        ]);

        Country::create($request->all());
        return redirect()->route('general.country.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);

        return view('general.country.editCountry', ['country'=>$country]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $country = Country::find($id);

        $this->validate($request, [
            'title_ua' => ['required',Rule::unique('countries')->ignore($country->id)],
            'title_ru' => ['required',Rule::unique('countries')->ignore($country->id)],
            'title_en' => ['required',Rule::unique('countries')->ignore($country->id)],
        ]);

        $country->update($request->all());
        return redirect()->route('general.country.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Country::find($id)->delete();
        return redirect()->route('general.country.index');
    }
}
