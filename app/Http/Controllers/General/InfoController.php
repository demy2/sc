<?php

namespace App\Http\Controllers\General;

use App\Category;
use App\Degree;
use App\Position;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InfoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $degrees = Degree::all();
        $positions = Position::all();
//dd($categories, $degrees, $positions);
        return view('general.info.index', compact('categories', 'degrees', 'positions'));
    }
}
