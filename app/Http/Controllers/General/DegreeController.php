<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Degree;

class DegreeController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('general.degrees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title_ua' => 'required|unique:degrees',
            'title_ru' => 'required|unique:degrees',
            'title_en' => 'required|unique:degrees'
        ]);

        Degree::create($request->all());
        return redirect()->route('general.info');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $degrees = Degree::find($id);

        return view('general.degrees.edit', ['degrees'=>$degrees]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title_ua' => 'required',
            'title_ru' => 'required',
            'title_en' => 'required'
        ]);

        $degrees = Degree::find($id);
        $degrees->update($request->all());
        return redirect()->route('general.info');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Degree::find($id)->delete();
        return redirect()->route('general.info');
    }
}
