<?php

namespace App\Http\Controllers\General;

use App\MbEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\Category;
use App\Country;
use App\City;
use App\University;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = MbEvent::all();

        return view('general.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('general.events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'date_open' => 'required',
            'date_close' => 'required',

            'title_ua' => 'required',

            'univer' => 'required',
        ]);

        $event = MbEvent::add($request->all());

        return redirect()->route('general.events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = MbEvent::find($id);

        return view('general.events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date_open' => 'required',
            'date_close' => 'required',

            'title_ua' => 'required',

            'univer' => 'required',
        ]);

        $event = MbEvent::find($id);
        $event->update($request->all());
        return redirect()->route('general.events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MbEvent::find($id)->delete();

        return redirect()->route('general.events.index');
    }
}
