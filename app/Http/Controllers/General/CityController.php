<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City;
use App\Country;
use Illuminate\Validation\Rule;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($country)
    {
        $countries = Country::find($country);
        $cities = City::where('country_id', $country)->get();
        $i = 0;
        return view('general.country.indexCity', [
            'countries'  =>  $countries,
            'cities'  =>  $cities,
            'i'   =>  $i
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($country)
    {
        $countries = Country::find($country);
        return view('general.country.createCity', ['countries'  =>  $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $country)
    {

        $this->validate($request, [
            'title_ua' => 'required|unique:cities',
            'title_ru' => 'required|unique:cities',
            'title_en' => 'required|unique:cities'
        ]);

        City::create($request->all());
        return redirect()->route('general.city.index', [$country]);
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($country, $id)
    {
        $countries = Country::find($country);
        $cities = City::find($id);

        return view('general.country.editCity', ['countries'=>$countries, 'cities'=>$cities]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $country, $id)
    {
        $city = City::find($id);

        $this->validate($request, [
            'title_ua' => ['required',Rule::unique('countries')->ignore($city->id)],
            'title_ru' => ['required',Rule::unique('countries')->ignore($city->id)],
            'title_en' => ['required',Rule::unique('countries')->ignore($city->id)],
        ]);

        $city->update($request->all());

        return redirect()->route('general.city.index', [$country]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($country, $id)
    {
        City::find($id)->delete();
        return redirect()->route('general.city.index', [$country]);
    }
}
