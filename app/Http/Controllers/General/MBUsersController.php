<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MBUsersController extends Controller
{
    public function index()
    {

        $users = DB::table('mbusers')->get();

        return view('general.users', compact('users'));
    }
}
