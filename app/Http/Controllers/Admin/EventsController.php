<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Event;
use App\Position;
use App\Section;
use App\Participant;
use App\Translate;
use App\UserInfo;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = (new Translate('title'))->lang();

        $events = Event::where('admin_id', Auth::user()->id)->get();
        //$participants = Participant::all();

        return view('admin.events.index', compact('title', 'events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = (new Translate('title'))->lang();

        //$id = Auth::user()->id;
        $categories = Category::pluck($title, 'id')->all();
        // $categories = Category::all();
        $positions = Position::all();

        //$eventId = Event::where('admin_id', $id)->max('id');


        return view('admin.events.create', compact('categories','positions'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'date_open' => 'required',
            'date_close' => 'required',
            'date_start' => 'required',
            'date_stop' => 'required',

            'image' =>  'nullable|image',

            'title_ua' => 'required',
            'short_title_ua' => 'required',
            'description_ua' => 'required',
            'target_ua' => 'required',
            'other_ua' => 'required',
            'design_rules_ua' => 'required',
            'orgcom_ua' => 'required',
            'docs_ua' => 'required',
            'section_ua' => 'required',

            'title_ru' => 'required',
            'short_title_ru' => 'required',
            'description_ru' => 'required',
            'target_ru' => 'required',
            'other_ru' => 'required',
            'design_rules_ru' => 'required',
            'orgcom_ru' => 'required',
            'docs_ru' => 'required',
            'section_ru' => 'required',

            'title_en' => 'required',
            'short_title_en' => 'required',
            'description_en' => 'required',
            'target_en' => 'required',
            'other_en' => 'required',
            'design_rules_en' => 'required',
            'orgcom_en' => 'required',
            'docs_en' => 'required',
            'section_en' => 'required',
        ]);

        $event = Event::add($request->all());
        $event->uploadImage($request->file('image'));
        $event->setCategory($request->get('categories'));
        if($request->has('section_ua'))
        {
            for ($i=0; $i < count($request->section_ua); $i++)
            {
                $section = new Section();
                $section->event_id = /*$value->event_id*/  $event->id;
                $section->section_ua = $request->section_ua[$i];
                $section->section_ru = $request->section_ru[$i];
                $section->section_en = $request->section_en[$i];
                $section->save();
            }
        }
        $id = $event->id;

        return redirect()->route('admin.price.show',$id);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = (new Translate('title'))->lang();
        $status = (new Translate('status'))->lang();
        $shortTitle = (new Translate('short_title'))->lang();

        if(Event::find($id)->admin_id == Auth::user()->id)
        {
            $eventTitle = Event::find($id)->$title;
            //dd($events);
            //$categories = Category::pluck('title_ua', 'id')->all();
            $participants = Participant::where('event_id', $id)->get();
            $N_user = $participants->unique('user_id')->count('user_id');
            $N_work = $participants->count('name');
            $N_book = $participants->sum('Nbook');

//dd($participants);
            return view('admin.events.show', compact('eventTitle', 'title', 'status', 'shortTitle', 'participants', 'N_user', 'N_work', 'N_book'));
        }
        return back()->withInput();


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Event::find($id)->admin_id == Auth::user()->id)
        {
            $events = Event::find($id);
            $categories = Category::pluck('title_ua', 'id')->all();
            $sections = Section::where('event_id', $id)->get();

            return view('admin.events.edit', compact('events', 'categories', 'sections'));
        }
        return back()->withInput();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date_open' => 'required',
            'date_close' => 'required',
            'date_start' => 'required',
            'date_stop' => 'required',

            'image' =>  'nullable|image',

            'title_ua' => 'required',
            'short_title_ua' => 'required',
            'description_ua' => 'required',
            'target_ua' => 'required',
            'other_ua' => 'required',
            'pay_ua' => 'required',
            'design_rules_ua' => 'required',
            'orgcom_ua' => 'required',
            'docs_ua' => 'required',
            'section_ua' => 'required',

            'title_ru' => 'required',
            'short_title_ru' => 'required',
            'description_ru' => 'required',
            'target_ru' => 'required',
            'other_ru' => 'required',
            'pay_ru' => 'required',
            'design_rules_ru' => 'required',
            'orgcom_ru' => 'required',
            'docs_ru' => 'required',
            'section_ru' => 'required',

            'title_en' => 'required',
            'short_title_en' => 'required',
            'description_en' => 'required',
            'target_en' => 'required',
            'other_en' => 'required',
            'pay_en' => 'required',
            'design_rules_en' => 'required',
            'orgcom_en' => 'required',
            'docs_en' => 'required',
            'section_en' => 'required',
        ]);

        $event = Event::find($id);
        $event->update($request->all());
        $event->uploadImage($request->file('image'));
        $event->setCategory($request->get('categories'));
        return redirect()->route('admin.events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Event::find($id)->delete();

        return redirect()->route('admin.events.index');
    }

    public function section(Request $request, $id)
    {
        $event = Event::find($id);

        return view('admin.events.section', compact('event'));
    }

    public function napochtu(Request $request)
    {
        $partids = $request->input('email');
        $event = $request->input('event');
        $text = $request->input('text');

        foreach ($partids as $partid) {
            $obj = new \stdClass(); //Переменная $user, содержит имя почту и любую другую информацию
            $part = Participant::find($partid);
            $usid = $part->user_id;
            $email = User::find($usid)->email;
            $info = UserInfo::where('user_id', $usid)->first();
            $obj->name = $info->name;
            $obj->surname = $info->middle_name;
            $obj->event = $event;
            $obj->text = $text;
            $obj->work = $part->name;
            $obj->avtor = $part->soavtor;
            \Illuminate\Support\Facades\Mail::to($email)->send(new \App\Mail\User($obj));
        }
        echo 'Письма отправлены';
    }

    public function download($id)
    {
        $file = Participant::find($id);

        return Storage::download($file->file) ;
    }

    public function statusset(Request $request)
    {
        $pid = $request->input('part');
        $stat = $request->input('status');
        $part = Participant::find($pid);
        $part->status = $stat;
        $part->save();
        $message = "changed";

        return back();
    }
}
