<?php

namespace App\Http\Controllers\admin;

use App\Section;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Translate;
use App\Event;
use App\Participant;
use Illuminate\Support\Facades\Auth;

class SectionController extends Controller
{
    public function index($eventId, $sectionId)
    {
        $title = (new Translate('title'))->lang();
        $shortTitle = (new Translate('short_title'))->lang();
        $status = (new Translate('status'))->lang();

        if(Event::find($eventId)->admin_id == Auth::user()->id)
        {
            $eventTitle = Event::find($eventId)->$title;
            //dd($events);
            //$categories = Category::pluck('title_ua', 'id')->all();
            $section = Section::where('event_id', $eventId)->where('local_id', $sectionId)->first();
            $participants = Participant::where('event_id', $eventId)->where('section_id', $section->id)->get();
            $N_user = $participants->unique('user_id')->count('user_id');
            $N_work = $participants->count('name');
            $N_book = $participants->sum('Nbook');

//dd($section);
            return view('admin.events.section', compact('eventTitle', 'section', 'title', 'shortTitle', 'status','participants', 'N_user', 'N_work', 'N_book'));
        }
        return back()->withInput();


    }
}
