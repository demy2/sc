<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;
use Redirect;

class RefController extends Controller
{
    public function refs($event, $ref_id)
    {
        return Redirect::route('eventPart', $event)->withCookie('ref_id', $ref_id, 6*30*60*24);
    }
}
