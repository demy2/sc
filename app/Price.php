<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{

    /*
    A - Any
    O - Ochno
    Z - Zaochno
    V - VUZ
    C - Current country
    N - Not Current country

    */
    protected $fillable = [
        'event_id','country','myVuz','form','price_book_en','currency_book_en','price','currency','priceAO','priceAZ','priceVA','priceVO','priceVZ','priceCA','priceCO','priceCZ','priceNA','priceNO','priceNZ','price_en','currency_en','price_translate','currency_translate','price_certificate','currency_certificate','created_at','updated_at'
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
