<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['title_ua','title_ru','title_en'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function city()
    {
        return $this->hasMany(City::class);
    }

    public function add($fields) // Добавление страны
    {
        $country = new static;
        $country->fill($fields);
        $country->save();

        return $country;
    }

    public function edit($fields) // Изменение страны
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove() // Удаление страны
    {
        $this->delete();
    }
}
