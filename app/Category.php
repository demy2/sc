<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['image', 'title_ua', 'title_ru', 'title_en'];

    public function events()
    {
        return $this->belongsToMany('App\Event');
    }

    public function countCategory()
    {
        return $this->events->where('status', 1)->count();
    }
}
