<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $fillable = ['cities_id', 'title_ua','short_title_ua', 'url', 'email','title_ru', 'short_title_ru', 'title_en', 'short_title_en',];

     /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'cities_id', 'id');
    }

    public function add($fields) // Добавление университета
    {
        $university = new static;
        $university->fill($fields);
        $university->save();

        return $university;
    }

    public function edit($fields) // Изменение университета
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove() // Удаление университета
    {
        $this->delete();
    }
}
