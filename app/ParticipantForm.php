<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticipantForm extends Model
{
    public function participant()
    {
        return $this->belongsTo('App\Participant', 'participant_form_id', 'id');
    }
}
