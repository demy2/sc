<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pay extends Model
{
    protected $fillable = [
        'token',
        'user_id',
        'event_id',
        'part_id',
        'status',
        'paytype',
        'amount','currency',
        'sender_first_name','sender_last_name','sender_phone',
        'create_date','completion_date','end_date',
        'action',
        'err_code','err_description',
        'payment_id','order_id','liqpay_order_id','transaction_id',
        'sender_card_bank','sender_card_country','sender_card_mask2',
    ];

    public function participant()
    {
        return $this->hasOne('App\Participant', 'part_id', 'id'); // ?????
    }

}
