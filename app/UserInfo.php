<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Storage;

class UserInfo extends Model
{
    protected $fillable = [
        'user_id',
        'ref_id',
        "gender",
        "surname",
        "name",
        "middle_name",
        "date_of_birth",
        "phone",
        "country_id",
        "city_id",
        "univer_id",
        "position_id",
        "degree_id",
        'avatar',
        "surnameNP",//new
        "nameNP",//new
        "middle_nameNP",//new
        "city_idNP",//new
        "phoneNP",//new
        "oNP",//new
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function university()
    {
        return $this->belongsTo(University::class, 'univer_id', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public static function add($fields) // Добавление информации
    {
        $userInfo = new static;
        $userInfo->fill($fields);
        $userInfo->save();

        return $userInfo;
    }

    public function edit($fields) // Изменение информации
    {
        $this->fill($fields);
        $this->save();
    }

    public function removeAvatar()
    {
        if($this->avatar != null)
        {
            Storage::delete('uploads/' . $this->avatar);
        }
    }

    public function uploadAvatar($avatar)
    {
        if($avatar == null) { return; }

        $this->removeAvatar();
        $filename = str_random(10) . '.' . $avatar->extension();
        $avatar->storeAs('uploads', $filename);
        $this->avatar = $filename;
        $this->save();
    }

    public function getAvatar() // вывод картинки
    {
        if($this->avatar == null)
        {
            return '/img/not-user.png';
        }

        return '/uploads/' . $this->avatar;
    }

    public function setCategory($ids)
    {
        if($ids == null) { return; }

        $this->categories()->sync($ids);
    }

    public function getCategoryTitle()
    {
        return implode(", ", $this->categories->pluck('title')->all());
    }
}
