<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_ua extends Model
{
    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
