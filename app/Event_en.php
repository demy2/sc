<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_en extends Model
{
    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
