<?php

namespace App;

use Carbon\Laravel\ServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use LiqPay;

class Participant extends Model
{
    protected $fillable =[
        'user_id',
        'event_id',
        'section_id',
        'name',
        'soavtor',
        'form_id',
        'dopolnitelno',
        'Nbook',

        'user_id',
        'event_id',
        'part_id',
        'status',
        'paytype',
        'amount','currency',
        'sender_first_name','sender_last_name','sender_phone',
        'create_date','completion_date','end_date',
        'action',
        'err_code','err_description',
        'payment_id','order_id','liqpay_order_id','transaction_id',
        'sender_card_bank','sender_card_country','sender_card_mask2',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function form()
    {
        return $this->hasOne(ParticipantForm::class, 'id', 'form_id');
    }

    public function section()
    {
        return $this->hasOne(Section::class, 'id', 'section_id');
    }

    public function stat()
    {
        return $this->hasOne(Status::class, 'id', 'status');
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function pay($part_id)
    {
        $pay = Pay::where('part_id',$part_id)->select('amount')->get();
//dd($pay);

        return $pay;
    }

    public function infoPay($participant)
    {
        if(App::isLocale('ua')){$lang = 'uk'; $title = 'title_ua';}
        if(App::isLocale('ru')){$lang = 'ru'; $title = 'title_ru';}
        if(App::isLocale('en')){$lang = 'en'; $title = 'title_en';}
        $uchast = 0;
        $dop = 0;
        $book = 0;
        $event_id = $participant->event->id;
        $price = Price::where('event_id',  $event_id)->first();
        //проверяем сначала есть ли разделение по стране затем по форме участия
        if ($price->country == 'country') {

            if ($price->form == 'formAny') {

                $uchast = $price->price;
            }else{
                if($participant->form_id == 1){$uchast = $price->priceAO;}
                elseif($participant->form_id == 2){$uchast = $price->priceAZ;}
                elseif($participant->form_id == 3){$uchast = $price->priceAZ;}
            }
//если есть разделение по стране...
        }elseif ($price->country == 'two'){
            //проверяем форму участия т.к. там больше условий
            if ($price->form == 'formAny'){
                if ($participant->user->info->country_id == $participant->event->countries_id){$uchast = $price->priceCA;}
                else{$uchast = $price->priceNA;}
            }else{
                if ($participant->user->info->country_id == $participant->event->countries_id){

                    if ($participant->form_id == 1) {$uchast = $price->priceCO;}
                    elseif ($participant->form_id == 2) {$uchast = $price->priceCZ;}
                    elseif ($participant->form_id == 3) {$uchast = $price->priceCZ;}

                }else{
                    if ($participant->form_id == 1) {$uchast = $price->priceNO;}
                    elseif ($participant->form_id == 2) {$uchast = $price->priceNZ;}
                    elseif ($participant->form_id == 3) {$uchast = $price->priceNZ;}
                }
            }
        }elseif($price->myVuz = 'on'){
            if($participant->user->info->univer_id == $participant->event->universities_id){
                if ($price->form == 'formAny'){$uchast = $price->priceVA;}
                else{
                    if ($participant->form_id == 1) {$uchast = $price->priceVO;}
                    elseif ($participant->form_id == 2) {$uchast = $price->priceVZ;}
                    elseif ($participant->form_id == 3) {$uchast = $price->priceVZ;}
                }
            }

        }
        $val = ' ₴';
        $currency = 'UAH';
        $bookTxt = "$participant->Nbook"."*$price->price_book"."$val";
        $dopTxt = "---";
        /*
              if($participant->dopolnitelno == 'on')
              {
                  $dop = 100;
                  $dopTxt = "$dop"."$val";
              }


              $book = $participant->Nbook*50;
              $bookTxt = "$participant->Nbook"."*50"."$val";
              $currency = 'UAH';

          */

        $uchastTxt = $uchast;
        $sum = $uchast+$dop+$book;
        $sumTxt = "$sum"."$val";

        //$req = $request->all();
        $public_key = "i14419032504";
        $private_key = "9IifZcA22IxvtDdaI13nLjh0F3DitBX4Ox2LBosC";
        $description = __('lang.LiqPayDescription').' '.$participant->event->$title;
        $participant_id = $participant->id;
        $user_id = $participant->user->id;

        $rnd = str_random(10);
        $liqpay = new LiqPay($public_key, $private_key);
        $html = $liqpay->cnb_form(array(
            'action'         => 'pay',
            'amount'         => "$sum",
            'currency'       => "$currency",
            'language'       => "$lang",
            'description'    => "$description",
            'order_id'       => "$rnd",
            'version'        => '3',
            'info'           => "$participant_id",
            'customer'       => "$user_id",
            'product_name'   => "$event_id"
        ));

//

        return compact('uchastTxt', 'book', 'bookTxt', 'dop', 'dopTxt', 'sum', 'sumTxt', 'html','price');
    }

    public function statPart($participant)
    {
        if($participant->status != 2 and $participant->status != 3 or $participant->pay == 1)
        {
            $border = 'danger';
        }
        else
        {
            $border = 'primary';
        }

        if($participant->status == 1)
        {
            $t = 'Работа не прислана'; $color = 'lightgray'; $s = 'fa-spin';
        }
        elseif($participant->status == 2)
        {
            $t = 'Работа прислана'; $color = 'blue'; $s = '';
        }
        elseif($participant->status == 3)
        {
            $t = 'Работа одобрена'; $color = 'limegreen'; $s = '';
        }
        elseif($participant->status == 4)
        {
            $t = 'Работа содержит замечания'; $color = 'orange'; $s = 'fa-spin';
        }
        else
        {
            $t = 'Работа не одобрена'; $color = 'red'; $s = '';
        }

        if($participant->pay == 1)
        {
            $p = 'Работа не оплачена'; $colorPay = 'red'; $sPay = 'fa-spin';
        }
        else
        {
            $p = 'Работа оплачена'; $colorPay = 'limegreen'; $sPay = '';
        }


        return compact('border', 't', 'color', 's', 'p', 'colorPay', 'sPay');
    }

}
